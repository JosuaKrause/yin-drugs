#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import csv
import sys
import json
import math
import argparse

import numpy as np
from sklearn.metrics import roc_auc_score
from sklearn.ensemble import RandomForestClassifier

from sklearn.datasets import load_digits

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train Digits')
    parser.add_argument('--ratio', type=float, default=0.1, help="test train ratio between 0..1")
    parser.add_argument('--target', type=str, default="7", help="training target")
    parser.add_argument('output', type=str, help="output file")
    args = parser.parse_args()

    digits = load_digits()
    data = np.asmatrix(digits['data'] / 4, dtype=np.int8)

    num = data.shape[0]
    train = int(math.floor(num * args.ratio))

    np.random.seed(0)
    all_ixs = np.arange(num)
    np.random.shuffle(all_ixs)

    train_ixs = all_ixs[:train]
    test_ixs = all_ixs[train:]

    target = np.array([ 1 if str(digits['target_names'][t]) == args.target else 0 for t in digits['target'].tolist() ], dtype=np.int8)
    if train > 0:
        model = RandomForestClassifier(n_jobs=-1, random_state=0)
        model.fit(data[train_ixs, :], target[train_ixs])

        pred = model.predict_proba(data)
        pred = pred[:, list(model.classes_).index(1)]
        print("prediction done", file=sys.stdout)
        print("AUC: {0}".format(roc_auc_score(target[test_ixs], pred[test_ixs])), file=sys.stdout)
    else:
        pred = None

    feature_list = [ str(p % 8) + "x" + str(p // 8) for p in xrange(data.shape[1]) ]
    feature_list.append("label")
    feature_list.append("meta")
    if pred is not None:
        feature_list.append("pred")

    def process_field(ix, k):
        if k == "label":
            return target[ix]
        if k == "pred":
            return pred[ix]
        if k == "meta":
            return digits['target_names'][digits['target'][ix]]
        [ x, y ] = k.split('x')
        pos = int(x) + int(y) * 8
        return data[ix, pos]

    with open(args.output, 'w') as f_out:
        out = csv.writer(f_out)
        out.writerow(feature_list)
        ixs = test_ixs.tolist()
        ixs.sort(key=lambda row_ix: -pred[row_ix])
        for row_ix in ixs:
            out.writerow([ process_field(row_ix, k) for k in feature_list ])
