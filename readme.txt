After cloning run:

git submodule update --init --recursive

To start the server run:

./server.py -p 13579 -- data.csv

where "data.csv" is your data file.
And browse to:

http://localhost:13579/class_signatures/

Or replace "class_signatures" with whatever the folder is called.
Set the initial thresholds.
Then restart the server by typing "restart" (and press enter) in the console.
Afterwards reload the page. This applies to whenever many changes (eg., setting
number of clusters) happen at once and slow the server down (restarting the
server stops all ongoing calculations). This does not retain the state if
clusters were manually edited, though.

Left click on features or class signatures creates a selection and allows to
inspect the corresponding items in more detail. Right clicking splits clusters.

Troubleshooting:

If the server won't start because of wrong python versions run the following command.

pip install --upgrade -r requirements.txt

If this does not work

pip install --user --upgrade -r requirements.txt

=====

to forward the server from a different machine use this:

echo "" | ./server.py -p 13579 -- data.csv > log.txt 2>&1 &

on the server side and

ssh -N -L localhost:13579:localhost:13579 name@server.com

on the client side.
