/**
 * Created by krause on 2016-02-02.
 */
function Clusters(selP, fmt, onUpdate, onHover, onOut, colors) {
  var that = this;

  var textW = 100;
  var chartW = 50;
  var chartH = 20;
  var scatterSize = 75;
  var dotSize = 1;

  var images = false;
  this.images = function(_) {
    if(!arguments.length) return images;
    images = _;
  };

  this.convertFeature = function(f) {
    var values = {};
    f["values"].forEach(function(v) {
      values[v["value"]] = v["stats"].map(function(n) {
        return +n;
      });
    });
    var res = {
      "name": f["name"],
      "values": values,
    };
    Object.keys(f).forEach(function(k) {
      if(k === "name" || k === "values") return;
      res[k] = +f[k];
    });
    return res;
  };

  var threshold = 10;
  this.threshold = function(_) {
    if(!arguments.length) return threshold;
    threshold = _;
  };
  var clusters = [];
  this.clusters = function(_) {
    if(!arguments.length) return clusters;
    clusters = _.map(function(c) {
      var stats = {};
      var sums = {};
      fillStats(c["features"], stats, sums, {});
      return {
        "features": c["features"].map(that.convertFeature),
        "stats": stats,
        "sums": sums,
        "cat": c["name"].slice(0, 3),
        "ixs": c["ixs"],
        "label_pos": +c["label_pos"],
        "total": +c["total"],
        "points": c["points"],
      };
    });
    selectedCs = {};
    onInternalSelectedChange();
  };

  var favorValue = null;
  this.favorValue = function(_) {
    if(!arguments.length) return favorValue;
    favorValue = _;
  };

  var favorValues = [];
  this.getFavorValues = function() {
    return favorValues;
  };

  function fillStats(features, stats, sums, vals) {
    features.forEach(function(f) {
      var stat = {};
      var sum = 0;
      f["values"].forEach(function(v, vix) {
        vals[v["value"]] = vix;
        var num = v["stats"].reduce(function(p, n) {
          return p + +n;
        }, 0);
        stat[v["value"]] = num;
        sum += num;
      });
      stats[f["name"]] = stat;
      sums[f["name"]] = sum;
    });
  }

  var fStats = {};
  var fSums = {};
  this.fStats = function(_) {
    if(!arguments.length) return fStats;
    fStats = {};
    fSums = {};
    var vals = {};
    fillStats(_, fStats, fSums, vals);
    favorValues = Object.keys(vals);
    favorValues.sort(function(va, vb) {
      return d3.ascending(vals[va], vals[vb]);
    });
    if(favorValues.length && favorValue === null) {
      favorValue = favorValues[favorValues.length - 1];
    }
  };

  function getFavorRatio(name, stats, sums) {
    var g = stats[name];
    var s = sums[name];
    var n = favorValue in g ? g[favorValue] : 0.0;
    return s > 0 ? n / s : 0.0;
  }

  function sortFavor(aname, bname, stats, sums) {
    if(!(aname in stats) || !(bname in stats)) {
      return 0;
    }
    var ra = getFavorRatio(aname, stats, sums);
    var rb = getFavorRatio(bname, stats, sums);
    return d3.descending(ra, rb);
  }

  this.getClusterIxs = function() {
    return clusters.map(function(c) {
      return getSingleClusterIxs(c);
    });
  };

  function getSingleClusterIxs(c) {
    return c["ixs"].map(function(ix) {
      return +ix;
    });
  }

  this.setWeights = function(field, cfs) {
    clusters.forEach(function(c, cix) {
      var fs = cfs[cix];
      c["features"].forEach(function(f, fix) {
        var nf = fs[fix];
        if(nf["name"] !== f["name"]) {
          console.warn("order mismatch", nf, f);
        }
        f[field] = +nf[field];
      });
    });
  };

  var selected = null;
  this.selected = function(_) {
    if(!arguments.length) return selected;
    selected = _;
  };

  var selectedCs = {};
  var selectedFs = {};
  function toggleC(cix, c) {
    if(!selectedCs[cix]) {
      selectedCs[cix] = c;
    } else {
      selectedCs[cix] = null;
    }
    onInternalSelectedChange();
  }

  function toggleF(fix) {
    if(!selectedFs[fix]) {
      selectedFs[fix] = fix;
    } else {
      selectedFs[fix] = null;
    }
    onInternalSelectedChange();
  }

  function onInternalSelectedChange() {
    var ixs = {};
    Object.keys(selectedCs).forEach(function(cix) {
      var c = selectedCs[cix];
      if(!c) return;
      getSingleClusterIxs(c).forEach(function(ix) {
        ixs[ix] = true;
      });
    });
    var ixsArr = Object.keys(ixs);
    ixsArr.sort(function(a, b) {
      return d3.ascending(a, b);
    });
    var fs = Object.keys(selectedFs).filter(function(fix) {
      return selectedFs[fix];
    });
    fs.sort(function(a, b) {
      return d3.ascending(a, b);
    });
    onSelectedChange(ixsArr, fs, []);
  }

  this.selectIxs = function(ixs, breadcrumbs) {
    var dirty = Object.keys(selectedCs).length || Object.keys(selectedFs).length;
    selectedCs = {};
    selectedFs = {};
    onSelectedChange(ixs, [], breadcrumbs);
    if(dirty) {
      that.update();
    }
  };

  var onSelectedChange = function(ixs, fs) {
    // nop
  };
  this.onSelectedChange = function(_) {
    if(!arguments.length) return onSelectedChange;
    onSelectedChange = _;
  };

  function isC(cix) {
    return !!selectedCs[cix];
  }

  function isF(fix) {
    return !!selectedFs[fix];
  }

  var onSplitCluster = function(cix, fix) {
    // nop
  };
  this.onSplitCluster = function(_) {
    if(!arguments.length) return onSplitCluster;
    onSplitCluster = _;
  };

  var onRecluster = function(cix) {
    // nop
  };
  this.onRecluster = function(_) {
    if(!arguments.length) return onRecluster;
    onRecluster = _;
  };

  var featureUnion = false;
  this.featureUnion = function(_) {
    if(!arguments.length) return featureUnion;
    featureUnion = _;
  };

  var ignoreZeros = false;
  this.ignoreZeros = function(_) {
    if(!arguments.length) return ignoreZeros;
    ignoreZeros = _;
  };

  var pct = false;
  this.asPercentage = function(_) {
    if(!arguments.length) return pct;
    pct = _;
  };

  var ORDER_IMPORTANCE = "importance";
  var ORDER_DISTRIBUTION_ALL = "distribution (align)";
  var ORDER_DISTRIBUTION_ONE = "distribution (individual)";
  var ORDERS = [ ORDER_IMPORTANCE, ORDER_DISTRIBUTION_ALL, ORDER_DISTRIBUTION_ONE ];
  this.orderValues = function() {
    return ORDERS;
  };

  var order = ORDER_DISTRIBUTION_ONE;
  this.order = function(_) {
    if(!arguments.length) return order;
    order = _;
  };

  var min_t = 1;
  var max_t = 10;
  this.range = function() {
    return [ min_t, max_t ];
  };

  var FIELD_PATHS = "paths";
  var FIELD_IMPORTANCE = "importance";
  var FIELD_GINI = "gini";
  var FIELDS = [ FIELD_PATHS, FIELD_IMPORTANCE, FIELD_GINI ];
  this.fieldValues = function() {
    return FIELDS;
  };

  var fieldSort = FIELD_GINI;
  this.field = function(_) {
    if(!arguments.length) return fieldSort;
    fieldSort = _;
  };

  this.sortByDistribution = function(arr, getF, features) {
    var stats = {};
    var sums = {};
    fillStats(features, stats, sums, {});
    arr.sort(function(aid, bid) {
      var a = getF(aid);
      var b = getF(bid);
      return sortFavor(a["name"], b["name"], stats, sums);
    });
  };

  var allFeatures = [];
  this.getAllFeatures = function() {
    return allFeatures;
  };

  this.getSummary = function(points) {
    var lines = {};
    points.forEach(function(p) {
      var truth = p["label"] ? 1 : 0;
      var score = +p["score"];
      var meta = p["meta"];
      if(!(meta in lines)) {
        lines[meta] = {
          "num": 0,
          "score": 0,
          "truth": 0,
          "ixs": [],
        };
      }
      lines[meta]["num"] += 1;
      lines[meta]["score"] += score;
      lines[meta]["truth"] += truth;
      lines[meta]["ixs"].push(p["ix"]);
    });
    var lineKeys = Object.keys(lines);
    if(lineKeys.length < 2) {
      var res = points.map(function(p) {
        var truth = p["label"] ? 1 : 0;
        var score = +p["score"];
        var meta = p["meta"];
        return {
          "text": fmt(score) + " -> " + fmt(truth) + ": " + meta,
          "ixs": [ p["ix"] ],
          "score": score,
        };
      });
      res.sort(function(pa, pb) {
        return d3.descending(pa["score"], pb["score"]);
      });
      return res;
    }
    lineKeys.sort(function(ka, kb) {
      var a = lines[ka];
      var b = lines[kb];
      var s = d3.descending(a["num"], b["num"]);
      if(s !== 0) {
        return s;
      }
      return d3.descending(a["score"] / a["num"], b["score"] / b["num"]);
    });
    return lineKeys.map(function(meta) {
      var line = lines[meta];
      var total = line["num"];
      var pos = line["truth"];
      var nonPos = total - pos;
      return {
        "text": total + " (" + pos + "+" + nonPos + "): " + meta,
        "ixs": line["ixs"],
      };
    });
  };

  var sps = [];
  var sp_points = [];
  this.setPoints = function(points) {
    sps.forEach(function(sp) {
      if(!sp) return;
      sp.points(points);
      sp.update();
    });
    sp_points = points;
  };

  var clusterLookup = {};
  this.clusterLookup = function(_) {
    if(!arguments.length) return clusterLookup;
    clusterLookup = _;
    sps.forEach(function(sp) {
      if(!sp) return;
      sp.update();
    });
  };

  this.createColumn = function(sel, header, data, getF, getC, max_imp, max_val, noNames, isSelected, textW) {
    var colorImportance = d3.scale.linear().domain([ 0, max_imp ]).range([ "white", "black" ]);
    if(!images) {
      var vixs = favorValues.map(function(_, vix) {
        return vix;
      });
      vixs.sort(function(aix, bix) {
        // charts are drawn in reverse order
        return d3.descending(favorValues[aix], favorValues[bix]);
      });

      header.style({
        "width": "100%",
        "height": "1.5em",
      });

      var cns = header.selectAll("span.v_chart_names").data(vixs, function(vix) {
        return vix;
      });
      cns.exit().remove();
      cns.enter().append("span").classed("v_chart_names", true);

      cns.style({
        "float": "right",
        "display": "inline-block",
        "width": chartW + "px",
        "height": "1.5em",
        "text-align": "left",
      }).text(function(vix) {
        return favorValues[vix];
      });

      var fsel = sel.selectAll("div.feature").data(data, function(fid) {
        return fid[0] + "_" + fid[1];
      });
      fsel.exit().remove();
      var fselE = fsel.enter().append("div").classed("feature", true);
      fselE.append("span").classed("f_name", true);
      fselE.append("span").classed("f_charts", true);

      function getColor(fid) {
        var f = getF(fid);
        return isF(fid[1]) ? colors[4] : f["name"] === isSelected() ? colors[3] : fieldSort in f ? colorImportance(+f[fieldSort]) : null;
      }

      function updateBackground() {
        fsel.style({
          "color": function(fid) {
            return jkjs.util.getFontColor(getColor(fid) || "white");
          },
          "background": getColor,
        });
      };

      updateBackground();
      fsel.on("contextmenu", function(fid) {
        if(realContextmenu()) {
          return;
        }
        onSplitCluster(fid[0], fid[1]);
        d3.event.preventDefault();
      }).on("click", function(fid) {
        toggleF(fid[1]);
        that.update();
      }).style({
        "display": "block",
        "padding": "1px",
        "cursor": "pointer",
      }).attr({
        "title": function(fid) {
          var f = getF(fid);

          function sum(p, c) {
            return p + c;
          }

          var favor = 0.0;
          var allSum = 0.0;
          var vals = favorValues.map(function(v) {
            var num = f["values"][v].reduce(sum, 0);
            if(v === favorValue) {
              favor = num;
            }
            allSum += num;
            return fmt(num);
          });
          if(allSum === 0.0) {
            allSum = 1.0;
          }
          return f["name"] + ": [ " + vals.join(" | ") + " ] " + fmt(favor / allSum * 100.0) + "%";
        },
      }).on("mouseover", function(fid) {
        that.selected(getF(fid)["name"]);
        updateBackground();
      }).order();
      fsel.selectAll(".f_name").text(function(fid) {
        return getF(fid)["name"];
      }).style({
        "margin-right": "5px",
        "overflow": "hidden",
        "text-overflow": "ellipsis",
        "width": textW + "px",
        "float": "left",
        "display": function(fid) {
          return (fid[0] && noNames) ? "none" : "inline-block";
        },
        "white-space": "nowrap",
      });

      var chsel = fsel.selectAll(".f_charts").style({
        "display": "inline-block",
      }).selectAll("svg.v_charts").data(function(fid) {
        return vixs.map(function(vix) {
          return [ fid[0], fid[1], vix ];
        });
      }, function(chid) {
        return chid[0] + "_" + chid[1] + "_" + chid[2];
      });
      chsel.exit().remove();
      var chselE = chsel.enter().append("svg").classed("v_charts", true);
      chselE.append("rect").classed("pos", true);
      chselE.append("rect").classed("neg", true);

      chsel.attr({
        "width": chartW,
        "height": chartH,
      }).style({
        "float": "right",
        "display": "inline-block",
        "width": chartW + "px",
        "height": "1.5em",
        "border": "1px solid lightgray",
      });

      function get(fid, isPos) {
        var f = getF(fid);
        var total = pct ? +(getC(fid[0])["total"]) : max_val;
        if(total === 0) {
          return 0;
        }
        var vs = f["values"][fid[2]];
        var num;
        if(isPos) {
          num = vs[0] + vs[3] + vs[4];
        } else {
          num = vs[1] + vs[2] + vs[5];
        }
        return (chartW - 2.0) * num / total;
      }

      function off(fid, isPos) {
        return isPos ? 0.25 : get(fid, !isPos);
      }

      var barPosSel = chsel.selectAll("rect.pos").attr({
        "x": function(fid) {
          return off(fid, true);
        },
        "width": function(fid) {
          return get(fid, true);
        },
        "y": 0.25 * chartH,
        "height": 0.5 * chartH,
        "fill": colors[0],
      });
      var barNegSel = chsel.selectAll("rect.neg").attr({
        "x": function(fid) {
          return off(fid, false);
        },
        "width": function(fid) {
          return get(fid, false);
        },
        "y": 0.25 * chartH,
        "height": 0.5 * chartH,
        "fill": colors[2],
      });
    } else {
      // sel, header, data, getF, getC, max_imp, max_val, noNames, isSelected, textW
      var imgs = sel.selectAll("svg.image_glyphs").data(function(cix) {
        return [ "mean", "stddev", "weights" ].map(function(ptype) {
          return [ cix, ptype ];
        });
      }, function(v) {
        return v[0] + "_" + v[1];
      });
      imgs.exit().remove();
      imgs.enter().append("svg").classed("image_glyphs", true);

      function valueToColor(v) {
        var val = 255 - Math.min(v * 4 * 16, 255);
        return d3.rgb(val, val, val);
      }

      var pixelSize = 10;
      var imgSize = pixelSize * 8;
      imgs.attr({
        "width": imgSize,
        "height": imgSize,
      }).style({
        "border": "solid black 1px",
        "width": imgSize + "px",
        "height": imgSize + "px",
        "margin": "5px",
      });

      var pixels = imgs.selectAll("rect.pixel").data(function(tuple) {
        return data(tuple[0]).map(function(fid) {
          return [ fid[0], fid[1], tuple[1] ];
        });
      }, function(v) {
        return v[0] + "_" + v[1] + "_" + v[2];
      });
      pixels.exit().remove();
      pixels.enter().append("rect").classed("pixel", true);

      function sum(arr) {
        return arr.reduce(function(p, v) {
          return p + v;
        }, 0);
      }

      function mean(obj, id) {
        var total = 0;
        var s = Object.keys(obj).reduce(function(p, k) {
          var v = id(k);
          var c = sum(obj[k]);
          total += c;
          return p + v*c;
        }, 0);
        return s / total;
      }

      pixels.attr({
        "x": function(fid) {
          return (+getF(fid)["name"].split("x")[0]) * pixelSize;
        },
        "y": function(fid) {
          return (+getF(fid)["name"].split("x")[1]) * pixelSize;
        },
        "width": pixelSize,
        "height": pixelSize,
        "stroke": "none",
        "stroke-width": 0,
        "fill": function(fid) {
          var ptype = fid[2];
          var f = getF(fid);
          if(ptype === "mean") {
            var v = mean(f["values"], function(k) {
              return +k;
            });
            return valueToColor(v);
          } else if(ptype === "stddev") {
            var v = mean(f["values"], function(k) {
              return +k;
            });
            var vsq = mean(f["values"], function(k) {
              return +k * +k;
            });
            var std = Math.sqrt(vsq - v*v);
            return valueToColor(Math.min(std, 4));
          } else if(ptype === "weights") {
            return colorImportance(+f[fieldSort]);
          } else {
            console.warn("unknown ptype", ptype);
            return "pink";
          }
        },
      });
    }
  }; // createColumn

  var lastContextPos = [ Number.NaN, Number.NaN ];
  function realContextmenu() {
    var oldPos = lastContextPos;
    lastContextPos = d3.mouse(selP.node());
    return oldPos[0] === lastContextPos[0] && oldPos[1] === lastContextPos[1];
  }

  this.update = function() {
    var align = order === ORDER_DISTRIBUTION_ALL;
    var individual = order === ORDER_DISTRIBUTION_ONE;
    var noNames = featureUnion && align;
    max_t = 1;
    var max_val = 0;
    var clusters = that.clusters();
    var max_imp = 0;
    clusters.forEach(function(c) {
      max_val = Math.max(max_val, +c["total"]);
      max_t = Math.max(max_t, c["features"].length);
      max_imp = c["features"].reduce(function(p, c) {
        return Math.max(p, +c[fieldSort]);
      }, max_imp);
    });

    if(max_t > 1) {
      if(threshold > max_t) {
        threshold = max_t;
      }
    } else {
      max_t = threshold;
    }

    var cixs = clusters.map(function(_, ix) { return ix; });
    cixs.sort(function(aix, bix) {
      if(noNames) {
        return d3.ascending(aix, bix);
      }
      var a = getC(aix)["cat"];
      var b = getC(bix)["cat"];
      if(a === b) {
        return d3.ascending(aix, bix);
      }
      return d3.descending(a, b);
    });
    var splits = {};
    var curCat = null;
    cixs.forEach(function(cix) {
      if(noNames) return;
      var c = getC(cix)["cat"];
      if(c !== curCat) {
        splits[cix] = true;
        curCat = c;
      }
    });

    function getC(ix) {
      return clusters[ix];
    }

    var sel = selP.selectAll("div.cluster").data(cixs, function(cix) {
      return cix;
    });
    sel.exit().each(function(cix) {
      sps[cix] = null;
    }).remove();
    var selE = sel.enter().append("div").classed("cluster", true);
    var cf = selE.append("div").classed("c_flex", true).style({
      "border": "solid 1px silver",
      "display": "flex",
      "justify-content": "center",
      "align-items": "center",
      "padding": "5px",
    });
    var cc = cf.append("div").classed("c_content", true).style({
      "display": "inline-block"
    });
    cc.append("div").style({
      "margin": "0 auto",
      "width": scatterSize + "px",
    }).classed("c_scatter", true).each(function(cix) {
      var scatter = d3.select(this);
      var sp = new Scatterplot(scatter, scatterSize, dotSize);
      sps[cix] = sp;
      sp.colorCB(function(ix) {
        return clusterLookup[ix] === cix ? "black" : "lightgray";
      });
      sp.points(sp_points);
      sp.update();
    });
    var c_head = cc.append("span").style({
      "display": "block",
      "text-align": "center",
    }).classed("c_head", true);
    c_head.append("span").classed("c_size", true);
    c_head.append("div").classed("c_color", true);
    c_head.append("span").classed("c_header_name", true);
    cc.append("div").classed("c_feature_head", true);
    cc.append("div").classed("c_features", true);
    sel.style({
      "float": "left",
      "clear": function(cix) {
        return cix in splits ? "left" : null;
      },
      "width": function(cix) {
        var w = textW + (chartW * favorValues.length) + 20 + (noNames && cix ? -6 : 0);
        if(cix && noNames) {
          w -= textW;
        }
        return w + "px"
      },
      "margin": noNames ? "10px 0" : "10px 5px",
    }).on("mouseover", function(cix) {
      var c = getC(cix);
      onHover && onHover(c, cix);
    }).on("mouseout", function(cix) {
      var c = getC(cix);
      onOut && onOut(c, cix);
    }).order();

    function getBGColor(cix) {
      return isC(cix) ? colors[4] : null;
    }

    sel.selectAll(".c_head").on("contextmenu", function(cix) {
      if(realContextmenu()) {
        return;
      }
      onRecluster(cix);
      d3.event.preventDefault();
    }).on("click", function(cix) {
      var c = getC(cix);
      toggleC(cix, c);
      that.update();
    }).style({
      "cursor": "pointer",
      "color": function(cix) {
        return jkjs.util.getFontColor(getBGColor(cix) || "white");
      },
      "background": function(cix) {
        return getBGColor(cix);
      },
    });
    sel.selectAll(".c_size").style({
      "display": "inline-block"
    }).text(function(cix) {
      var c = getC(cix);
      var total = +c["total"];
      var pos = +c["label_pos"];
      var nonPos = total - pos;
      return total + " (" + pos + "+" + nonPos + ")";
    }).attr({
      "title": function(cix) {
        var c = getC(cix);
        return fmt(+c["label_pos"] / +c["total"] * 100.0) + "%";
      },
    });
    var colorMap = {
      "pos": colors[0],
      "mid": colors[1],
      "neg": colors[2],
    };
    sel.selectAll(".c_color").style({
      "display": "inline-block",
      "border": "1px solid black",
      "background": function(cix) {
        var cat = getC(cix)["cat"];
        return cat in colorMap ? colorMap[cat] : "black";
      },
      "height": "1em",
      "width": "1em",
      "margin": "0 0 0 5px",
      "padding": 0,
      "vertical-align": "middle",
    });
    sel.selectAll(".c_header_name").style({
      "display": function(cix) {
        var c = getC(cix);
        return "header_name" in c ? "block" : "none";
      },
      "text-overflow": "ellipsis",
      "white-space": "nowrap",
      "width": (textW + (chartW * favorValues.length)) + "px",
      "overflow": "hidden",
      "margin": "0 auto",
    }).attr({
      "title": function(cix) {
        var c = getC(cix);
        return "header_name" in c ? c["header_name"] : "";
      },
    }).text(function(cix) {
      var c = getC(cix);
      return "header_name" in c ? c["header_name"] : "";
    });

    function getF(fid) {
      return getC(fid[0])["features"][fid[1]];
    }

    function featureFilter(fid, ix) {
      if(ignoreZeros) {
        var f = getF(fid);
        if(individual) {
          var c = getC(fid[0]);
          if(getFavorRatio(f["name"], c["stats"], c["sums"]) === 0) {
            return false;
          }
        } else {
          if(+f[fieldSort] === 0) {
            return false;
          }
        }
      }
      return ix < threshold || images;
    }

    function sortFeatures(fs, general) {
      fs.sort(function(aid, bid) {
        var a = getF(aid);
        var b = getF(bid);
        var res;
        if(individual) {
          var c = getC(aid[0]);
          res = sortFavor(a["name"], b["name"], c["stats"], c["sums"]);
        } else if(!general) {
          res = d3.descending(+a[fieldSort], +b[fieldSort]);
        } else {
          res = sortFavor(a["name"], b["name"], fStats, fSums);
        }
        if(res === 0) {
          return d3.ascending(a["name"], b["name"]);
        }
        return res;
      });
      return fs;
    }

    function computeAllFeatures() {
      var fm = {};
      sel.each(function(cix) {
        return sortFeatures(getC(cix)["features"].map(function(_, fix) {
          return [ cix, fix ];
        }), false).filter(featureFilter).forEach(function(fid) {
          fm[fid[1]] = true;
        });
      });
      return Object.keys(fm);
    }

    allFeatures = computeAllFeatures();

    function getFeatures(cix) {
      if(!featureUnion) {
        var res = sortFeatures(getC(cix)["features"].map(function(_, fix) {
          return [ cix, fix ];
        }), false).filter(featureFilter);
        return sortFeatures(res, align);
      }
      return sortFeatures(allFeatures.map(function(fix) {
        return [ cix, fix ];
      }), align);
    }

    that.createColumn(sel.selectAll(".c_features"), sel.selectAll(".c_feature_head"), function(cix) { // data
      return getFeatures(cix);
    }, getF, getC, max_imp, max_val, noNames, function() { // isSelected
      return selected;
    }, textW);
    onUpdate();
  }; // update
} // Clusters
