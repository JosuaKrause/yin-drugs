/**
 * Created by krause on 2016-11-10.
 */
function Burst(sel, width, height, scale) {
  var that = this;
  var svg = sel.append("svg").style({
    "width": width,
    "height": height,
  });

  var zoom = d3.behavior.zoom().scaleExtent([ 0.01, 100 ]);
  var g = svg.append("g");
  zoom.on("zoom", function() {
    g.attr("transform", "translate(" + d3.event.translate + ") scale(" + d3.event.scale + ")");
  });
  svg.call(zoom);

  var towardsPositive = false;
  this.towardsPositive = function(_) {
    if(!arguments.length) return towardsPositive;
    towardsPositive = !!_;
  };

  var scoreRatios = {};
  var points = [];
  this.points = function(ps) {
    var sr = {};
    points = ps.map(function(p, ix) {
      if(+p["ix"] !== ix) {
        throw {
          "err": "index mismatch",
          "p": p,
          "ix": ix,
        };
      }
      var score = +p["score"];
      if(!(score in sr)) {
        sr[score] = [ 0, 0 ];
      }
      var label = p["label"];
      sr[score][label ? 1 : 0] += 1;
      return {
        "id": ix,
        "score": score,
        "label": label,
        "meta": p["meta"],
      };
    });
    scoreRatios = sr;
  };

  var links = [];
  var linkInfo = {};
  this.neighbors = function(ns) {
    li = {};
    links = ns.map(function(n, ix) {
      li[+n["from"]] = ix;
      return {
        "source": +n["from"],
        "min_ix": +n["min_ix"],
        "max_ix": +n["max_ix"],
        "size": +n["size"],
        "chg_min": n["chg_min"],
        "chg_max": n["chg_max"],
      };
    });
    linkInfo = li;
  };

  var vecs = [];
  this.vecs = function(_) {
    if(!arguments.length) return vecs;
    vecs = _;
  };

  function getVec(ix) {
    return vecs[ix].join("\n");
  } // getVec

  var sep = "\n";
  function getTitle(chg) {
    var cs = [];
    if("0" in chg) {
      cs.push(chg["0"]["1"].map(function(c) {
        return "+" + c;
      }).join(sep));
    }
    if("1" in chg) {
      cs.push(chg["1"]["0"].map(function(c) {
        return "-" + c;
      }).join(sep));
    }
    return cs.join(sep);
  } // getTitle

  function getExplanationLength(chg) {
    return Object.keys(chg).reduce(function(p, v) {
      return Object.keys(chg[v]).reduce(function(pp, vv) {
        return pp + chg[v][vv].length;
      }, p);
    }, 0);
  } // getExplanationLength

  function getRows() {
    var ins = {};
    var outs = {};
    var roots = {};
    links.forEach(function(l) {
      var source = l["source"];
      var target = towardsPositive ? l["max_ix"] : l["min_ix"];
      var size = +l["size"];
      var chg = towardsPositive ? l["chg_max"] : l["chg_min"];
      var expl = getExplanationLength(chg);
      if(!(source in outs)) {
        var sp = points[source];
        outs[source] = {
          "id": sp["id"],
          "score": sp["score"],
          "label": sp["label"],
          "meta": sp["meta"],
        };
        if(source !== target) {
          roots[source] = false;
        }
      }
      if(!(target in ins)) {
        var tp = points[target];
        ins[target] = {
          "point": {
            "id": tp["id"],
            "score": tp["score"],
            "label": tp["label"],
            "meta": tp["meta"],
          },
          "links": {},
        };
      }
      ins[target]["links"][source] = {
        "size": size,
        "chg": chg,
        "expl": expl,
      };
      if(!(target in roots)) {
        roots[target] = true;
      }
    });
    return [ ins, outs, Object.keys(roots).filter(function(e) {
      return roots[e];
    }).map(function(e) {
      return e;
    }) ];
  } // getRows

  function getColor(score) {
    var r = scoreRatios[score];
    if(!r) return "black";
    return scale(r[1] / (r[0] + r[1]));
  }

  this.update = function() {
    var io = getRows();
    var ins = io[0];
    var outs = io[1];
    var roots = io[2];

    function getInDegree(r) {
      return Object.keys(r["links"]).length;
    } // getInDegree

    roots.sort(function(ar, br) {
      var a = ins[ar];
      var b = ins[br];
      var cmp = d3.descending(getInDegree(a), getInDegree(b));
      if(cmp !== 0) return cmp;
      return d3.descending(a["point"]["score"], b["point"]["score"]) * (towardsPositive ? 1 : -1);
    });

    function getChildren(r) {
      if(!(r in ins)) {
        return [];
      }
      var res = Object.keys(ins[r]["links"]).filter(function(ix) {
        return ix !== r;
      });
      res.sort(function(ar, br) {
        var a = outs[ar];
        var b = outs[br];
        return d3.descending(a["score"], b["score"]) * (towardsPositive ? 1 : -1);
      });
      return res;
    } // getChildren

    function getLeafCount(r) {
      if(!(r in ins)) return 1;
      var res = Object.keys(ins[r]["links"]).filter(function(ix) {
        return ix !== r;
      });
      return res.reduce(function(p, c) {
        return p + getLeafCount(c);
      }, 0);
    } // getLeafCount

    var diff = 10;
    function processRoots(sel, parent, elem, rad, parRad, from, to) {
      if(!elem.length) return rad;
      var s = sel.selectAll("g.level" + rad).data(elem, function(ix) {
        return ix;
      });
      s.exit().remove();
      var sE = s.enter().append("g").classed("level" + rad, true);
      sE.append("g").classed("glevel" + rad, true);
      sE.append("line").classed("level" + rad, true);
      var pE = sE.append("path").classed("level" + rad, true);
      pE.append("title").classed("level" + rad, true);
      var sizes = {};
      var fts = {};
      s.order();
      var cc = {};
      var totalCC = 0;
      s.each(function(ix) {
        cc[ix] = getLeafCount(ix);
        totalCC += cc[ix];
      });
      var pos = 0;
      var adds = {};
      s.selectAll("g.glevel" + rad).each(function(ix) {
        var el = d3.select(this);
        var f = from + pos / totalCC * (to - from);
        pos += cc[ix];
        var t = from + pos / totalCC * (to - from);
        if(rad === 0) {
          f = 0;
          t = 360;
        }
        var add = 0;
        if(parent in ins) {
          var l = ins[parent]["links"][ix];
          add = l["expl"];
        }
        adds[ix] = add;
        fts[ix] = [ f, t ];
        sizes[ix] = processRoots(el, ix, getChildren(ix), rad + diff + add, rad + add, f, t) + add;
      });
      if(rad === 0) {
        s.sort(function(aix, bix) {
          return d3.descending(sizes[aix], sizes[bix]);
        });
        var trans = {};
        var curX = 0;
        var curY = 0;
        var rowMax = 0;
        var curRow = [];
        s.each(function(ix) {
          if(sizes[ix] > rowMax) {
            rowMax = sizes[ix];
          }
          curX += sizes[ix];
          trans[ix] = [ curX, curY ];
          curRow.push(trans[ix]);
          curX += sizes[ix];
          curX += diff;
          if(curX > 1000) {
            curRow.forEach(function(t) {
              t[1] += rowMax + diff;
            });
            curY += 2 * rowMax + diff;
            curX = 0;
            curRow = [];
            rowMax = 0;
          }
        });
        curRow.forEach(function(t) {
          t[1] += rowMax + diff;
        });
        s.attr({
          "transform": function(ix) {
            return "translate(" + trans[ix] + ")";
          },
        });
      }

      s.selectAll("line.level" + rad).attr({
        "x1": function(ix) {
          var ft = fts[ix];
          var f = ft[0];
          var t = ft[1];
          var mid = (f + t) * 0.5;
          return Math.cos(mid * Math.PI / 180.0) * parRad;
        },
        "y1": function(ix) {
          var ft = fts[ix];
          var f = ft[0];
          var t = ft[1];
          var mid = (f + t) * 0.5;
          return Math.sin(mid * Math.PI / 180.0) * parRad;
        },
        "x2": function(ix) {
          var ft = fts[ix];
          var f = ft[0];
          var t = ft[1];
          var mid = (f + t) * 0.5;
          return Math.cos(mid * Math.PI / 180.0) * (rad + adds[ix]);
        },
        "y2": function(ix) {
          var ft = fts[ix];
          var f = ft[0];
          var t = ft[1];
          var mid = (f + t) * 0.5;
          return Math.sin(mid * Math.PI / 180.0) * (rad + adds[ix]);
        },
        "stroke": "black",
        "stroke-width": function(ix) {
          return Math.min(cc[ix] * 0.01, diff);
        },
      });

      s.selectAll("path.level" + rad).attr({
        "d": function(ix) {
          var p = new jkjs.Path();
          var ft = fts[ix];
          var f = ft[0];
          var t = ft[1];
          p.arcSegment(0, 0, rad + adds[ix], rad + adds[ix] + diff, f, t);
          return p;
        },
        "fill": function(ix) {
          var el = outs[ix];
          return getColor(el["score"]);
        },
        "stroke": "black",
        "stroke-width": 0.01,
      });

      s.selectAll("title.level" + rad).text(function(ix) {
        var el = outs[ix];
        return [
          el["id"],
          (el["label"] ? "T" : "F") + " (" + el["score"] + ")",
          el["meta"],
          getVec(ix),
          ((parent in ins) ? getTitle(ins[parent]["links"][ix]["chg"]) : ""),
        ].join("\n");
      });

      return Object.keys(sizes).reduce(function(p, v) {
        return Math.max(p, sizes[v]);
      }, rad);
    } // processRoots

    g.selectAll("*").remove();
    processRoots(g, null, roots, 0, 0, 0, 360);
  }; // update
} // AdjacencyMatrix
