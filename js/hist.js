/**
 * Created by krause on 2016-03-16.
 */
function Hist(sel, settings, sizeW, sizeH, border, onROCThreshold, scoreTmp, fmt, colors) {
  var that = this;
  var COLOR_LEFT = colors[0];
  var COLOR_RIGHT = colors[1];
  var COLOR_TMP = colors[2];

  var points = [];
  this.points = function(_) {
    if(!arguments.length) return points;
    points = _;
  };

  var tLeft = 1.0;
  var tRight = 0.0;
  this.roct = function(left) {
    return left ? tLeft : tRight;
  };

  this.setROCThreshold = function(score, left) {
    var val = findScore(score);
    if(Number.isNaN(val)) return;
    if(left) {
      tLeft = val;
    } else {
      tRight = val;
    }
    onROCThreshold(val, left);
  };

  function findScore(score) {
    if(Number.isNaN(score)) return Number.NaN;
    var p = Number.NaN;
    points.forEach(function(cp) {
      var cur = +cp["score"];
      if(Number.isNaN(p) || Math.abs(cur - score) < Math.abs(p - score)) {
        p = cur;
      }
    });
    return p;
  }

  function scoreFromPos(pos) {
    var x = pos[0];
    if(x < border || x > border * sizeW) return Number.NaN;
    return findScore(1.0 - (x - border) / sizeW);
  }

  this.setROCTemp = function(score) {
    setTmp(findScore(score));
  };

  var onROCTemp = function(score) {};
  this.onROCTemp = function(_) {
    if(!arguments.length) return _;
    onROCTemp = _;
  };

  var oldTemp = Number.NaN;
  function setTmp(score) {
    if(oldTemp === score || (Number.isNaN(oldTemp) && Number.isNaN(score))) return;
    oldTemp = score;
    if(Number.isNaN(score)) {
      tmpLine.attr({
        "opacity": 0,
      });
    } else {
      tmpLine.attr({
        "x1": xScale(score),
        "x2": xScale(score),
        "opacity": 1,
      });
    }
    onROCTemp(score);
  }

  var lastRightPoint = [ Number.NaN, Number.NaN ];
  var svg = sel.append("svg").attr({
    "width": sizeW + 2*border,
    "height": sizeH + 2*border,
  }).style({
    "cursor": "crosshair"
  }).on("mousemove", function() {
    setTmp(scoreFromPos(mousePos()));
  }).on("mouseout", function() {
    setTmp(Number.NaN);
  }).on("click", function() {
    that.setROCThreshold(scoreFromPos(mousePos()), true);
  }).on("contextmenu", function() {
    var p = mousePos();
    if(!p || (p[0] === lastRightPoint[0] && p[1] === lastRightPoint[1])) {
      return;
    }
    that.setROCThreshold(scoreFromPos(mousePos()), false);
    lastRightPoint = p;
    d3.event.preventDefault();
  });

  function mousePos() {
    return d3.mouse(svg.node());
  }

  var xScale = d3.scale.linear().domain([ 1.0, 0.0 ]).range([ border, border + sizeW ]);
  var yScale = d3.scale.linear().domain(settings["yscale"]).range([ border, border + sizeH ]);

  var xAxis = d3.svg.axis().scale(xScale).ticks(4).tickSize(5).tickSubdivide(false).orient("top");
  var yAxis = d3.svg.axis().scale(yScale).ticks(4).tickSize(5).tickSubdivide(false).orient("right");

  svg.append("g").classed({
    "x": true,
    "axis": true
  }).attr({
    "transform": "translate(" + [ 0, border ] + ")"
  }).call(xAxis);
  svg.append("g").classed({
    "y": true,
    "axis": true
  }).attr({
    "transform": "translate(" + [ sizeW + border, 0 ] + ")"
  }).call(yAxis);

  svg.append("text").attr({
    "x": border + sizeW * 0.5,
    "y": sizeH + border * 1.5,
    "text-anchor": "middle",
    "alignment-baseline": "middle"
  }).text("Threshold");
  svg.append("text").attr({
    "x": 0,
    "y": 0,
    "text-anchor": "middle",
    "alignment-baseline": "middle",
    "transform": "translate(" + [ border * 0.5, border + sizeH * 0.5 ] + ") rotate(-90)"
  }).text(settings["name"]);

  svg.append("g").classed("curves", true);
  var tmpLine = svg.append("line").classed("t_tmp", true).attr({
    "stroke": COLOR_TMP,
    "y1": yScale(0.0),
    "y2": yScale(1.0),
    "opacity": 0,
  });
  svg.append("line").classed("t_left", true).attr({
    "stroke": COLOR_LEFT,
    "x1": xScale(tLeft),
    "x2": xScale(tLeft),
    "y1": yScale(0.0),
    "y2": yScale(1.0),
  });
  svg.append("line").classed("t_right", true).attr({
    "stroke": COLOR_RIGHT,
    "x1": xScale(tRight),
    "x2": xScale(tRight),
    "y1": yScale(0.0),
    "y2": yScale(1.0),
  });

  svg.append("rect").attr({
    "x": border,
    "y": border,
    "width": sizeW,
    "height": sizeH,
    "stroke": "black",
    "fill": "none",
  });

  if(settings["pattern"]) {
    for(var patt_ix = 0;patt_ix < colors.length;patt_ix += 1) {
      svg.append("pattern").attr({
        "id": "pattern_stripes_" + patt_ix,
        "patternUnits": "userSpaceOnUse",
        "width": 10,
        "height": 10,
      }).append("path").attr({
        "d": "M0 5 L5 0 L0 0 Z M0 10 L5 10 L10 5 L10 0 Z",
        "fill": colors[patt_ix],
        "stroke": "none",
      });
    }
  }

  this.update = function() {

    function updateLine(curve, line, prevYs) {
      var vals = points.map(function(p) {
        return [ +p["score"], line["y"](p) ];
      });
      vals.sort(function(a, b) {
        return d3.ascending(a[0], b[0]);
      });

      var path = new jkjs.Path();
      if(!prevYs) {
        vals.forEach(function(p) {
          var x = xScale(p[0]);
          var y = yScale(p[1]);
          if(path.isEmpty()) {
            path.move(x, y);
          } else {
            path.line(x, y);
          }
        });
      } else {
        var htap = new jkjs.Path();
        vals.forEach(function(p, ix) {
          var x = xScale(p[0]);
          var y = p[1];
          htap.line(x, yScale(prevYs[ix]));
          prevYs[ix] += y;
          if(path.isEmpty()) {
            path.move(x, yScale(prevYs[ix]));
          } else {
            path.line(x, yScale(prevYs[ix]));
          }
        });
        path.add(htap.reverse());
        path.close();
      }
      curve.attr({
        "d": path,
        "opacity": prevYs ? 0.5 : null,
        "fill": !("fill" in line) ? "none" : Number.isNaN(+line["fill"]) ? line["fill"] : colors[+line["fill"]],
        "stroke": !("color" in line) ? "none" : Number.isNaN(+line["color"]) ? line["color"] : colors[+line["color"]],
        "stroke-dasharray": line["dash"] || null,
      });
    }

    var curves = svg.selectAll("g.curves");
    curves.selectAll("*").remove();
    var prevYs = settings["area"] ? points.map(function(p) {
      return 0.0;
    }) : null;
    settings["lines"].forEach(function(line) {
      var curve = curves.append("path").classed(line["id"], true);
      updateLine(curve, line, prevYs);
    });

    svg.selectAll("line.t_left").attr({
      "x1": xScale(tLeft),
      "x2": xScale(tLeft),
    });
    svg.selectAll("line.t_right").attr({
      "x1": xScale(tRight),
      "x2": xScale(tRight),
    });
  };
} // Hist
Hist.ACCURACY = {
  "name": "Accuracy",
  "yscale": [ 1.0, 0.0 ],
  "lines": [
    {
      "id": "acc",
      "color": "black",
      "y": function(p) {
        return (+p["tp"] + +p["tn"]) / (+p["tp"] + +p["fn"] + +p["tn"] + +p["fp"]);
      },
    },
  ],
};
Hist.CONFUSION = {
  "name": "Confusion",
  "yscale": [ 0.0, 1.0 ],
  "lines": [
    {
      "id": "tp",
      "color": 0,
      "y": function(p) {
        return 1.0 - +p["tp"] / (+p["tp"] + +p["fn"]);
      },
    },
    {
      "id": "fn",
      "color": 0,
      "dash": "2, 2",
      "y": function(p) {
        return 1.0 - +p["fn"] / (+p["tp"] + +p["fn"]);
      },
    },
    {
      "id": "tn",
      "color": 1,
      "y": function(p) {
        return 1.0 - +p["tn"] / (+p["tn"] + +p["fp"]);
      },
    },
    {
      "id": "fp",
      "color": 1,
      "dash": "2, 2",
      "y": function(p) {
        return 1.0 - +p["fp"] / (+p["tn"] + +p["fp"]);
      },
    },
  ],
};
Hist.MATRIX = {
  "name": "Matrix",
  "yscale": [ 1.0, 0.0 ],
  "lines": [
    {
      "id": "tp",
      "color": 0,
      "y": function(p) {
        return +p["tp"] / (+p["tp"] + +p["fn"] + +p["tn"] + +p["fp"]);
      },
    },
    {
      "id": "fn",
      "color": 0,
      "dash": "2, 2",
      "y": function(p) {
        return +p["fn"] / (+p["tp"] + +p["fn"] + +p["tn"] + +p["fp"]);
      },
    },
    {
      "id": "tn",
      "color": 1,
      "y": function(p) {
        return +p["tn"] / (+p["tp"] + +p["fn"] + +p["tn"] + +p["fp"]);
      },
    },
    {
      "id": "fp",
      "color": 1,
      "dash": "2, 2",
      "y": function(p) {
        return +p["fp"] / (+p["tp"] + +p["fn"] + +p["tn"] + +p["fp"]);
      },
    },
  ],
};
Hist.MATRIX_AREA = {
  "name": "Population",
  "yscale": [ 1.0, 0.0 ],
  "area": "true",
  "pattern": "true",
  "lines": [
    {
      "id": "tp",
      "fill": 0,
      "color": "black",
      "y": function(p) {
        return +p["tp"] / (+p["tp"] + +p["fn"] + +p["tn"] + +p["fp"]);
      },
    },
    {
      "id": "fn",
      "fill": "url(#pattern_stripes_0)",
      "color": "black",
      "y": function(p) {
        return +p["fn"] / (+p["tp"] + +p["fn"] + +p["tn"] + +p["fp"]);
      },
    },
    {
      "id": "fp",
      "fill": "url(#pattern_stripes_1)",
      "color": "black",
      "y": function(p) {
        return +p["fp"] / (+p["tp"] + +p["fn"] + +p["tn"] + +p["fp"]);
      },
    },
    {
      "id": "tn",
      "fill": 1,
      "color": "black",
      "y": function(p) {
        return +p["tn"] / (+p["tp"] + +p["fn"] + +p["tn"] + +p["fp"]);
      },
    },
  ],
};
