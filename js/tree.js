/**
 * Created by krause on 2017-01-09.
 */

function Tree(sel, size, fmt, nonSpherical, colors, colorMap, ignoreCount) {
  var that = this;
  var svg = sel.append("svg").style({
    "width": "100%",
    "height": "100%",
    "pointer-events": "all",
  });

  var bottomOrder = [];
  this.bottomOrder = function(_) {
    if(!arguments.length) return bottomOrder;
    bottomOrder = _;
  }

  var cm = colorMap || {};
  var showCount = !ignoreCount;
  var baseColors = colors || [ "white", "white" ];
  function addPatterns(root) {
    var defs = root.append("defs");
    var s = 10;
    [
      [ "hedge_pattern_left", baseColors[0] ],
      [ "hedge_pattern_right", baseColors[1] ],
    ].forEach((arr) => {
      var id = arr[0];
      var color = arr[1];
      var hedge = defs.append("pattern").attr({
        "id": id,
        "x": 0,
        "y": 0,
        "width": s,
        "height": s,
        "patternUnits": "userSpaceOnUse",
      });
      hedge.append("rect").attr({
        "x": 0,
        "y": 0,
        "width": s,
        "height": s,
        "fill": color,
        "stroke": "none",
      });
      hedge.append("path").attr({
        "fill": "none",
        "stroke": "black",
        "stroke-width": 0.5,
        "stroke-linecap": "square",
        "d": new jkjs.Path().move(0, s * 0.25).line(s * 0.25, 0)
                            .move(0, s * 0.75).line(s * 0.75, 0)
                            .move(s * 0.25, s).line(s, s * 0.25)
                            .move(s * 0.75, s).line(s, s * 0.75),
      });
    });
  } // addPatterns
  addPatterns(svg);
  var textShadow = "0 0 10px white, 0 0 10px white";

  var isSpherical = !nonSpherical;
  var metricText = svg.append("text");
  var selRoot = svg.append("g");
  var linkRoot = selRoot.append("g");
  var nodeRoot = selRoot.append("g");
  var zoom = d3.behavior.zoom();
  var scale = 1.0;
  zoom.scaleExtent([ 0.1, 10 ]).on("zoom", function() {
    scale = d3.event.scale;
    selRoot.attr({
      "transform": "translate(" + d3.event.translate + ") scale(" + scale + ")",
    });
    move();
  });
  svg.call(zoom);

  var COLOR_RED = "crimson";
  var COLOR_BLACK = "black";

  var change = false;
  var tree = {
    "children": [],
    "features": [],
    "count_exact": 0,
    "count_all": 0,
  };
  this.tree = function(_) {
    if(!arguments.length) return tree;
    tree = _;
    change = true;
  };

  var expl = [];
  this.expl = function(_) {
    if(!arguments.length) return expl;
    expl = _;
    change = true;
  };

  function getTextSize(line) {
    metricText.text(line);
    var own = metricText.node().getBBox();
    return [ own.width, own.height ];
  } // getTextW

  function prepareTree(node, expl, maxs) {
    var res = {
      "lines": node["features"].map(function(f) {
        return [ f, expl.indexOf(f) >= 0 ? COLOR_RED : COLOR_BLACK ];
      }),
    };
    if(showCount) {
      res["lines"].push([
        fmt(node["count_exact"]) + " / " + fmt(node["count_all"]),
        COLOR_BLACK,
      ]);
    }
    var minW = 0;
    var hAdd = 0;
    if("stats" in node) {
      res["bottom"] = jkjs.util.flatMap(bottomOrder, (k) => {
        if(!(k in node["stats"]) || node["stats"][k] === 0) return [];
        var v = "" + node["stats"][k];
        var vs = getTextSize(v);
        minW += vs[0] + size;
        hAdd = Math.max(hAdd, vs[1]);
        return [ [ v, cm[k] ] ];
      });
      res["bottom_w"] = minW / res["bottom"].length;
    }
    var textSize = res["lines"].reduce(function(p, l) {
      var ts = getTextSize(l[0]);
      return [ Math.max(p[0], ts[0]), p[1] + ts[1] ];
    }, [ 0, 0 ]);
    if(res["lines"].length === 0) {
      textSize = getTextSize("0");
    }
    res["max_w"] = Math.max(textSize[0], minW);
    maxs["w"] = Math.max(maxs["w"], res["max_w"]);
    res["max_h"] = textSize[1] + hAdd;
    maxs["h"] = Math.max(maxs["h"], res["max_h"]);
    res["line_h"] = textSize[1] / Math.max(res["lines"].length, 1);
    res["children"] = node["children"].map(function(child) {
      return prepareTree(child, expl, maxs);
    });
    return res;
  } // prepareTree

  function depth(node) {
    return node["children"].reduce(function(p, child) {
      return Math.max(p, depth(child) + 1);
    }, 1);
  } // depth

  var links = [];
  var nodes = [];
  var force = d3.layout.force().nodes([]).links([]);
  var move = function() {};
  this.update = function() {
    var w = jkjs.util.svgWidth(svg);
    var h = jkjs.util.svgHeight(svg);
    force.stop();
    if(change) {
      var maxs = {
        "w": 0,
        "h": 0,
      };
      var root = prepareTree(tree, expl, maxs);
      metricText.text("");
      var layout = d3.layout.tree();
      if(isSpherical) {
        layout.size([ 360, 200 * depth(root) ]);
        nodes = layout.nodes(root).map(function(node) {
          var x = w * 0.5 + node["y"] * Math.cos(node["x"] / 180.0 * Math.PI);
          var y = h * 0.5 + node["y"] * Math.sin(node["x"] / 180.0 * Math.PI);
          node["x"] = x;
          node["y"] = y;
          return node;
        });
      } else {
        layout.nodeSize([ maxs["w"] * 0.5, maxs["h"] * 2.0 ]);
        nodes = layout.nodes(root).map((node) => {
          node["x"] = w * 0.5 + node["x"];
          node["y"] = node["y"] + maxs["h"];
          return node;
        });
      }
      links = layout.links(nodes);
      force.size([ w, h ]).nodes(nodes).links(links);
      change = false;
    }
    force.gravity(0.01).linkDistance(function(l) {
      var dx = x(l["source"]) - x(l["target"]);
      var dy = y(l["source"]) - y(l["target"]);
      return Math.sqrt(dx * dx + dy * dy);
    }).linkStrength(0.1).charge(function(node) {
      return -Math.max(node["max_w"], node["max_h"]) * 5 - 100;
    });

    function x(node) {
      return node["x"];
    } // x

    function y(node) {
      return node["y"];
    } // y

    var linkSel = linkRoot.selectAll("line.links").data(links.map(function(_, ix) {
      return ix;
    }), function(ix) {
      return ix;
    });
    linkSel.exit().remove();
    linkSel.enter().append("line").classed("links", true);
    linkSel.attr({
      "stroke": "black",
    });

    var nodeSel = nodeRoot.selectAll("g.nodes").data(nodes.map(function(_, ix) {
      return ix;
    }), function(ix) {
      return ix;
    });
    nodeSel.exit().remove();
    var nodeSelE = nodeSel.enter().append("g").classed("nodes", true);
    nodeSelE.append("rect").classed("boundary", true);
    if(!showCount) {
      nodeSelE.append("g").classed("bottom", true);
    }
    nodeSelE.append("text").classed("lines", true);

    move = function() {
      linkSel.attr({
        "x1": function(ix) {
          return x(links[ix]["source"]);
        },
        "y1": function(ix) {
          return y(links[ix]["source"]);
        },
        "x2": function(ix) {
          return x(links[ix]["target"]);
        },
        "y2": function(ix) {
          return y(links[ix]["target"]);
        },
      });
      nodeSel.attr({
        "transform": function(ix) {
          return "translate(" + [
            x(nodes[ix]),
            y(nodes[ix]),
          ] + ") scale(" + Math.pow(scale, -0.4) + ") translate(" + [
            -nodes[ix]["max_w"] * 0.5,
            -nodes[ix]["max_h"] * 0.5,
          ] + ")";
        },
      });
    }; // move

    nodeSel.selectAll("rect.boundary").attr({
      "x": -2,
      "y": -2,
      "width": function(ix) {
        return nodes[ix]["max_w"] + 4;
      },
      "height": function(ix) {
        return nodes[ix]["max_h"] + 4;
      },
      "fill": "white",
      "stroke": "black",
    });

    var lineSel = nodeSel.selectAll("text.lines").selectAll("tspan").data(function(nix) {
      return nodes[nix]["lines"].map(function(_, lix) {
        return [ nix, lix ];
      });
    }, function(id) {
      return id[0] + "_" + id[1];
    });
    lineSel.exit().remove();
    lineSel.enter().append("tspan");
    lineSel.attr({
      "fill": function(id) {
        return nodes[id[0]]["lines"][id[1]][1];
      },
      "x": 0,
      "y": function(id, pos) {
        return (pos + 1) * nodes[id[0]]["line_h"] - 3;
      },
    }).text(function(id) {
      return nodes[id[0]]["lines"][id[1]][0];
    });

    var numsSel = nodeSel.selectAll("g.bottom").selectAll("g.nums").data((nix) => {
      if(!("bottom" in nodes[nix])) return [];
      if(nodes[nix]["line_h"] === 0) console.log(nodes[nix]);
      return nodes[nix]["bottom"].map((_, bix) => [ nix, bix ]);
    }, (id) => id[0] + "_" + id[1]);
    numsSel.exit().remove();
    var numsSelE = numsSel.enter().append("g").classed("nums", true);
    numsSelE.append("rect").classed("nums", true);
    numsSelE.append("text").classed("nums", true);

    numsSel.attr({
      "transform": (id) => "translate(" + [
        nodes[id[0]]["max_w"] * 0.5,
        nodes[id[0]]["max_h"] - nodes[id[0]]["line_h"]
      ] + ")",
    });

    function getB(id) {
      return nodes[id[0]]["bottom"][id[1]];
    } // getB

    function getBX(id) {
      return id[1] * getBW(id) - getBW(id) * nodes[id[0]]["bottom"].length * 0.5;
    } // getBX

    function getBW(id) {
      return nodes[id[0]]["bottom_w"];
    } // getBW

    numsSel.selectAll("rect.nums").attr({
      "x": (id) => getBX(id),
      "y": 0,
      "width": (id) => getBW(id),
      "height": (id) => nodes[id[0]]["line_h"],
      "fill": (id) => getB(id)[1],
      "stroke": "black",
      "stroke-width": 0.5,
    });
    numsSel.selectAll("text.nums").attr({
      "x": (id) => getBX(id) + getBW(id) * 0.5,
      "y": (id) => nodes[id[0]]["line_h"] * 0.5,
      "fill": "black",
      "stroke": "none",
      "text-anchor": "middle",
      "alignment-baseline": "central",
    }).style({
      "user-select": "none",
      "text-shadow": textShadow,
    }).text((id) => getB(id)[0]);

    if(isSpherical) {
      force.on("tick", function() {
        move();
      });
      force.start();
      for(var ticks = 0;ticks < 100;ticks += 1) {
        force.tick();
      }
    } else {
      move();
    }
  }; // update
} // Tree
