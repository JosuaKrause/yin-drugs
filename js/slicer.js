/**
 * Created by krause on 2017-01-09.
 */

function Slicer(sel, size, width, height, marginW, marginH, onClick, onDblClick) {
  var that = this;
  var parent = sel.append("div").style({
    "width": width,
    "height": height,
    "position": "relative",
    "overflow": "auto",
  });
  var svg = parent.append("svg");
  var texts = svg.append("g");
  var textTop = texts.append("g");
  var textBottom = texts.append("g");
  var textLeft = texts.append("g");
  var textRight = texts.append("g");
  var regions = svg.append("g");
  var items = svg.append("g");
  var navs = svg.append("g");
  var navXText = navs.append("text");
  var navYText = navs.append("text");

  var ease = "easeInOutCubic";
  var duration = 750;
  var posChange = false;
  function transition(sel) {
    if(!posChange) {
      return sel;
    }
    return sel.transition().duration(duration).ease(ease);
  } // transition

  this.posChange = function(_) {
    if(!arguments.length) return posChange;
    posChange = _;
  };

  var ixs = [];
  this.ixs = function(_) {
    if(!arguments.length) return ixs;
    ixs = _;
    posChange = true;
  };

  function click(ix) {
    var pos = [ d3.event.pageX, d3.event.pageY ];
    onClick(ix, getState(ix), pos);
  } // click

  function dblclick(id) {
    dblclickFull([ id ]);
  } // dblclick

  function dblclickFull(ids) {
    onDblClick(ids);
  } // dblclickFull

  this.noSelect = function() {
    return -1;
  };
  var selectIx = this.noSelect();
  this.selectIx = function(_) {
    if(!arguments.length) return selectIx;
    selectIx = _;
  };

  function getRegionKey(id) {
    return id[0] + "__" + id[1];
  } // getRegionKey

  var selectRegions = {};
  this.selectRegions = function(_) {
    if(!arguments.length) return Object.values(selectRegions);
    var forceValue = !_.every(function(grp) {
      var key = getRegionKey(grp);
      return key in selectRegions;
    });
    _.forEach(function(grp) {
      var key = getRegionKey(grp);
      if(forceValue) {
        selectRegions[key] = grp;
      } else if(key in selectRegions) {
        delete selectRegions[key];
      }
    });
  };

  this.clearSelectRegions = function() {
    selectRegions = {};
  };

  function getSelectedIxs(buckets, ixMap) {
    var res = jkjs.util.flatMap(Object.keys(selectRegions), function(k) {
      var grp = selectRegions[k];
      if(!(grp[0] in buckets)) return [];
      if(!(grp[1] in buckets[grp[0]])) return [];
      return buckets[grp[0]][grp[1]]["arr"].map(function(ix) {
        return ixMap[ix];
      });
    });
    res = jkjs.util.distinct(res);
    res.sort(d3.ascending);
    return res;
  };

  this.getSelectedIxs = function() {
    return getSelectedIxs(lastBuckets, lastIxMap);
  };

  var yGroup = function(ix) {
    return [];
  };
  this.yGroup = function(_) {
    if(!arguments.length) return yGroup;
    yGroup = _;
    posChange = true;
  };

  var yGroups = [];
  this.yGroups = function(_) {
    if(!arguments.length) return yGroups;
    yGroups = _;
    posChange = true;
  };

  var yPage = null;
  this.yPage = function(_) {
    if(!arguments.length) return yPage;
    yPage = _;
  };
  var yPages = 1;
  this.yPages = function(_) {
    if(!arguments.length) return yPages;
    yPages = _;
  };

  var xGroup = function(ix) {
    return [];
  };
  this.xGroup = function(_) {
    if(!arguments.length) return xGroup;
    xGroup = _;
    posChange = true;
  };

  var xGroups = [];
  this.xGroups = function(_) {
    if(!arguments.length) return xGroups;
    xGroups = _;
    posChange = true;
  };

  var xPage = null;
  this.xPage = function(_) {
    if(!arguments.length) return xPage;
    xPage = _;
  };
  var xPages = 1;
  this.xPages = function(_) {
    if(!arguments.length) return xPages;
    xPages = _;
  };

  var state = function(ix) {
    return {
      "label": "F",
      "color": "black",
      "mispred": false,
    };
  };
  this.state = function(_) {
    if(!arguments.length) return state;
    state = _;
    posChange = true;
  };

  var itemSort = function(as, bs, aix, bix) {
    var cmpL = d3.ascending(as["label"], bs["label"]);
    if(cmpL !== 0) return cmpL;
    var cmpX = d3.ascending(as["mispred"], bs["mispred"]);
    if(cmpX !== 0) return cmpX;
    return d3.ascending(aix, bix);
  };
  this.itemSort = function(_) {
    if(!arguments.length) return itemSort;
    itemSort = _;
    posChange = true;
  };

  var states = {};
  function getState(ix) {
    return states[ix];
  } // getState

  function fillStates(ixs, buckets, ixMap) {
    var s = {};
    ixs.forEach(function(ix) {
      var rix = ixMap[ix];
      if(!(rix in s)) {
        s[rix] = state(rix)
      }
      s[ix] = s[rix];
    });
    states = s;
    Object.keys(buckets).forEach(function(xGrp) {
      var bucket = buckets[xGrp];
      Object.keys(bucket).forEach(function(yGrp) {
        var b = bucket[yGrp];
        b["arr"].sort(function(aix, bix) {
          return itemSort(getState(aix), getState(bix), aix, bix);
        });
        var lookup = {};
        b["arr"].forEach(function(ix, pos) {
          lookup[ix] = pos;
        });
        b["lookup"] = lookup;
      });
    });
  } // fillStates

  function getBuckets(ixs, xGroups, yGroups) {
    var buckets = {};
    var bucketLookup = {};
    xGroups.forEach(function(xGrp) {
      var bucket = {};
      yGroups.forEach(function(yGrp) {
        bucket[yGrp] = {
          "arr": [],
          "lookup": {},
        };
      });
      buckets[xGrp] = bucket;
    });
    var rix = -1;
    var rIxs = [];
    var ixMap = {};
    ixs.forEach(function(ix) {
      var xGrps = xGroup(ix);
      var yGrps = yGroup(ix);
      var curIx = ix;
      var one = false;
      xGrps.forEach(function(xGrp) {
        if(!(xGrp in buckets)) {
          throw {
            "err": "unknown group",
            "axis": "x",
            "group": xGrp,
            "groups": xGroups,
          };
        }
        var bucket = buckets[xGrp];
        yGrps.forEach(function(yGrp) {
          if(!(yGrp in bucket)) {
            throw {
              "err": "unknown group",
              "axis": "y",
              "group": yGrp,
              "groups": yGroups,
            };
          }
          var b = bucket[yGrp];
          b["lookup"][curIx] = b["arr"].length;
          b["arr"].push(curIx);
          bucketLookup[curIx] = [ xGrp, yGrp ];
          if(curIx in ixMap) {
            throw {
              "err": "duplicate index!",
              "curIx": curIx,
              "ix": ix,
              "prevIx": ixMap[curIx],
            };
          }
          ixMap[curIx] = ix;
          rIxs.push(curIx);
          curIx = rix;
          rix -= 1;
          one = true;
        });
      });
      if(one) {
        rix += 1;
      }
    });
    return [ buckets, bucketLookup, rIxs, ixMap ];
  } // getBuckets

  function flattenBuckets(buckets) {
    return Object.keys(buckets).reduce(function(p, xGrp) {
      return Object.keys(buckets[xGrp]).reduce(function(pp, yGrp) {
        pp.push([ xGrp, yGrp ]);
        return pp;
      }, p);
    }, []);
  } // flattenBuckets

  function getNumXs(buckets, xGroups, numYs, w) {
    var numXs = xGroups.map(function(xGrp) {
      return Object.keys(buckets[xGrp]).reduce(function(m, yGrp, ix) {
        var arr = buckets[xGrp][yGrp]["arr"];
        var numX = Math.floor(arr.length / numYs[ix]);
        return Math.max(m, numX);
      }, minX);
    });
    var budgetPer = Math.max(minX, Math.floor(Math.floor(w / size) / Math.max(numXs.length, 1)));
    var good = 0;
    var newXs = numXs.map(function(numX) {
      if(numX > budgetPer) {
        return budgetPer;
      }
      good += budgetPer - numX;
      return numX;
    });
    var nixs = numXs.map(function(_, nix) {
      return nix;
    });
    nixs.sort(function(aix, bix) {
      return d3.descending(numXs[aix], numXs[bix]);
    });
    while(good > 0 && nixs.some(function(nix) {
      return newXs[nix] < numXs[nix];
    })) {
      nixs.forEach(function(nix) {
        if(newXs[nix] >= numXs[nix] || good <= 0) {
          return;
        }
        newXs[nix] += 1;
        good -= 1;
      });
    }
    while(good > 0) {
      nixs.forEach(function(nix) {
        if(good <= 0) {
          return;
        }
        newXs[nix] += 1;
        good -= 1;
      });
    }
    return newXs;
  } // getNumXs

  function getBucket(buckets, bucketLookup, ix) {
    var grp = bucketLookup[ix];
    return getRegion(buckets, grp);
  } // getBucket

  function getRegion(buckets, id) {
    return buckets[id[0]][id[1]];
  } // getRegion

  function getPosInBucket(buckets, bucketLookup, ix) {
    var b = getBucket(buckets, bucketLookup, ix);
    if(!(ix in b["lookup"])) {
      throw {
        "err": "item in wrong bucket",
        "bucket": b,
        "ix": ix,
      };
    }
    return b["lookup"][ix];
  } // getPosInBucket

  function getPos(buckets, bucketLookup, numX, numY, ix) {
    var pos = getPosInBucket(buckets, bucketLookup, ix);
    var adjPos = Math.floor(pos % (numX * numY));
    var x = Math.floor(adjPos / numY) * size;
    var y = Math.floor(adjPos % numY) * size;
    return [ x, y ];
  } // getPos

  var fontSize = size * 0.6;
  function drawTexts(xGroups, yGroups, numXs, numYs, leftXs, topYs, mw, mh, w, h) {

    function createLabels(tG, names, ds, vs, isX, isBefore, mLen, len) {

      function dblclickRange(name) {
        var ids;
        if(isX) {
          ids = yGroups.map(function(yGrp) {
            return [ name, yGrp ];
          });
        } else {
          ids = xGroups.map(function(xGrp) {
            return [ xGrp, name ];
          });
        }
        dblclickFull(ids);
      } // dblclickRange

      function ownV(ix) {
        return vs[ix];
      } // own

      function ownS(ix) {
        return ds[ix] * size;
      } // own

      var otherV = isBefore ? 0 : len - mLen;
      var otherS = mLen;
      var nameLookup = {};
      names.forEach(function(name, ix) {
        if(name in nameLookup) {
          console.warn("duplicate name in group", name, names);
        }
        nameLookup[name] = ix;
      });
      var tSel = tG.selectAll("g").data(names, function(name) {
        return name;
      });
      tSel.exit().remove();
      var tSelE = tSel.enter().append("g").attr({
        "opacity": 0,
      });
      tSelE.append("text");

      var tAttr = {
        "transform": function(name) {
          var ix = nameLookup[name];
          return "translate(" + [ isX ? ownV(ix) : otherV, !isX ? ownV(ix) : otherV ] + ")";
        },
      };
      tSelE.attr(tAttr);
      transition(tSel).attr(tAttr).attr({
        "opacity": 1,
      });

      tSel.selectAll("text").attr({
        "font-size": fontSize,
        "font-family": "courier",
        "font-weight": "lighter",
        "stroke": "none",
      }).style({
        "cursor": "pointer",
        "user-select": "none",
      }).on("click", dblclickRange).each(function(name) {
        var ix = nameLookup[name];
        var textSel = d3.select(this);
        jkjs.text.display(textSel, name, {
          "x": 0,
          "y": 0,
          "width": isX ? ownS(ix) : otherS,
          "height": !isX ? ownS(ix) : otherS,
        }, true, jkjs.text.align.middle, jkjs.text.position.center, true, true);
      });
    } // createLabels

    createLabels(textLeft, yGroups, numYs, topYs, false, true, mw, w);
    createLabels(textRight, yGroups, numYs, topYs, false, false, mw, w);
    createLabels(textTop, xGroups, numXs, leftXs, true, true, mh, h);
    createLabels(textBottom, xGroups, numXs, leftXs, true, false, mh, h);

    var px = xPage ? xPage.value : 0;
    var py = yPage ? yPage.value : 0;
    function createNavigation() {
      var navV = [ xPage, xPage, yPage, yPage, ];
      var navP = [ px - 1, px + 1, py - 1, py + 1, ];
      var navPs = [ xPages, xPages, yPages, yPages, ];
      var shown = [ xGroups.length, xGroups.length, yGroups.length, yGroups.length, ];
      var rot = [ 90, -90, 180, 0, ];
      var sm = size * 0.2;
      var sb = size * 0.7;
      var x = [ mw - sm, w - mw + sm, w * 0.5, w * 0.5 ];
      var y = [ h * 0.5, h * 0.5, mh - sm, h - mh + sm, ];
      var order = [ 0, 1, 2, 3, ].filter(function(ix) {
        return navP[ix] >= 0 && navP[ix] <= navPs[ix] - shown[ix];
      });
      var nSel = navs.selectAll("path.nav").data(order, function(ix) {
        return ix;
      });
      nSel.exit().remove();
      nSel.enter().append("path").classed("nav", true).attr({
        "fill": "white",
        "stroke": "black",
        "d": new jkjs.Path().move(-sb, 0).line(0, sb).line(sb, 0).close(),
      }).style({
        "cursor": "pointer",
      });
      nSel.attr({
        "transform": function(ix) {
          return "translate(" + [ x[ix], y[ix] ] + ") rotate(" + rot[ix] + ")";
        },
      }).on("click", function(ix) {
        navV[ix].value = navP[ix];
      });
    } // createNavigation
    createNavigation();
    navXText.attr({
      "font-size": fontSize,
      "font-family": "courier",
      "font-weight": "lighter",
      "stroke": "none",
    }).style({
      "user-select": "none",
    });
    navYText.attr({
      "font-size": fontSize,
      "font-family": "courier",
      "font-weight": "lighter",
      "stroke": "none",
    }).style({
      "user-select": "none",
    });
    jkjs.text.display(navXText, (px + 1) + "-" + (px + xGroups.length) + " / " + xPages, {
      "x": w - mw,
      "y": 0,
      "width": mw,
      "height": mh,
    }, true, jkjs.text.align.middle, jkjs.text.position.center, true, true);
    jkjs.text.display(navYText, (py + 1) + "-" + (py + yGroups.length) + " / " + yPages, {
      "x": 0,
      "y": h - mh,
      "width": mw,
      "height": mh,
    }, true, jkjs.text.align.middle, jkjs.text.position.center, true, true);
  } // drawTexts

  function isOverfull(xGroups, yGroups, numXs, numYs, buckets) {
    return xGroups.some(function(xGrp, xIx) {
      var numX = numXs[xIx];
      var bucket = buckets[xGrp];
      return yGroups.some(function(yGrp, yIx) {
        var numY = numYs[yIx];
        var b = bucket[yGrp];
        return b["arr"].length > numX * numY;
      });
    });
  } // isOverfull

  function drawRegions(rSel, rSelE, xLookup, yLookup, numXs, numYs, leftXs, topYs, buckets, overfull) {
    var pos = {
      "transform": function(id) {
        return "translate(" + [ leftXs[xLookup[id[0]]], topYs[yLookup[id[1]]] ] + ")";
      },
    };
    rSelE.attr(pos);
    transition(rSel).attr(pos);

    var rSelRE = rSelE.append("rect").classed("region", true).attr({
      "stroke": "black",
      "stroke-width": 1,
      "opacity": 0,
    }).style({
      "cursor": "pointer",
    });
    rSel.selectAll("rect.region").attr({
      "fill": function(id) {
        return getRegionKey(id) in selectRegions ? "lightgray" : "white";
      },
    }).on("dblclick", dblclick);

    var rAttr = {
      "x": 0,
      "y": 0,
      "width": function(id) {
        return numXs[xLookup[id[0]]] * size;
      },
      "height": function(id) {
        return numYs[yLookup[id[1]]] * size;
      },
    };
    rSelRE.attr(rAttr);
    transition(rSel.selectAll("rect.region")).attr(rAttr).attr({
      "opacity": 1,
    });

    var gSelE = rSelE.append("g").classed("summary", true);
    var gSel = rSel.selectAll("g.summary").attr({
      "transform": function(id) {
        return "translate(" + [ numXs[xLookup[id[0]]] * size * 0.5, numYs[yLookup[id[1]]] * size * 0.5 ] + ")";
      },
      "opacity": function(id) {
        return overfull ? 1 : 0;
      },
    });
    SUMMARY(gSel, gSelE, buckets);
  } // drawRegions

  function barSummary(gSel, gSelE, buckets) {
    gSelE.append("path").classed("bars", true).attr({
      "stroke": "black",
      "stroke-width": 0.2,
      "d": new jkjs.Path().move(-1.25 * size, 0).line(1.25 * size, 0)
                          .move(0, -1.25 * size).line(0, 1.25 * size),
    });
    gSel.selectAll("path.bars").attr({
      "opacity": function(id) {
        var num = getRegion(buckets, id)["arr"].length;
        return num !== 0 ? 1 : 0;
      },
    });
    var order = [ 0, 1, 2, 3, ];
    var up = [ true, true, false, false, ];
    var left = [ true, false, false, true, ];
    var bars = {};
    var colors = [ null, null, null, null, ];
    var max = 0;
    var bSel = gSel.selectAll("g.bar").data(function(id) {
      var arr = getRegion(buckets, id)["arr"];
      var vals = [ 0, 0, 0, 0, ];
      var total = 0;
      arr.forEach(function(ix) {
        var s = getState(ix);
        var pos = (s["label"] === "T" ? 0 : 2) + (s["mispred"] ? 1 : 0);
        if(colors[pos]) {
          if(colors[pos] !== s["color"]) {
            throw {
              "err": "mismatching colors",
              "colors": colors[pos],
              "state": s["color"],
            };
          }
        } else {
          colors[pos] = s["color"];
        }
        vals[pos] += 1;
        total += 1;
      });
      max = Math.max(max, total);
      bars[getRegionKey(id)] = vals;
      return order.map(function(pos) {
        return [ id[0], id[1], pos ];
      });
    }, function(id) {
      return getRegionKey(id) + "__" + id[2];
    });
    bSel.exit().remove();
    var bSelE = bSel.enter().append("g").classed("bar", true);

    bSelE.append("rect");
    var scale = d3.scale.linear().domain([ 0, max ]).range([ 0, 1.5 * size ]);
    bSel.selectAll("rect").attr({
      "x": function(id) {
        var isLeft = left[id[2]];
        return (isLeft ? -1 : 0) * size;
      },
      "y": function(id) {
        var isUp = up[id[2]];
        return (isUp ? -scale(bars[getRegionKey(id)][id[2]]) : 0);
      },
      "width": size,
      "height": function(id) {
        var num = bars[getRegionKey(id)][id[2]];
        return scale(num);
      },
      "fill": function(id) {
        return colors[id[2]];
      },
      "stroke": "black",
      "stroke-width": 0.5,
    });

    bSelE.append("text").attr({
      "alignment-baseline": "central",
      "font-size": fontSize * 1.25,
      "font-family": "courier",
      "font-weight": "bolder",
      "stroke": "white",
      "stroke-width": 0.6,
      "fill": "black",
    }).style({
      "cursor": "pointer",
      "user-select": "none",
    });
    bSel.selectAll("text").attr({
      "x": function(id) {
        var isLeft = left[id[2]];
        return (isLeft ? -1 : 1) * size * 0.1;
      },
      "y": function(id) {
        var isUp = up[id[2]];
        return (isUp ? -0.65 : 0.6) * size;
      },
      "text-anchor": function(id) {
        var isLeft = left[id[2]];
        return isLeft ? "end" : "start";
      },
    }).text(function(id) {
      var num = bars[getRegionKey(id)][id[2]];
      return num !== 0 ? num : "";
    });

    bSel.order();
  } // barSummary

  function pieSummary(gSel, gSelE, buckets) {
    var cake = {};
    var pSel = gSel.selectAll("path").data(function(id) {
      var arr = getRegion(buckets, id)["arr"];
      var objs = {};
      var counter = {};
      var total = 0;
      arr.forEach(function(ix) {
        var s = getState(ix);
        var key = s["mispred"] + "__" + s["label"];
        if(!(key in counter)) {
          counter[key] = 0;
          objs[key] = {
            "label": s["label"],
            "color": s["color"],
            "mispred": s["mispred"],
          };
        }
        counter[key] += 1;
        total += 1;
      });
      var pies = Object.keys(counter).map(function(c) {
        return {
          "label": objs[c]["label"],
          "color": objs[c]["color"],
          "ratio": counter[c] / total,
          "mispred": objs[c]["mispred"],
          "total": total,
        };
      });
      pies.sort(function(pa, pb) {
        var cmp = d3.ascending(pa["label"], pb["label"]);
        if(cmp !== 0) return cmp;
        var isT = pa["label"] === "T";
        return d3.ascending(pa["mispred"] !== isT, pb["mispred"] !== isT);
      });
      cake[getRegionKey(id)] = pies;
      var pixs = pies.map(function(_, ix) {
        return [ id[0], id[1], ix ];
      });
      return pixs;
    }, function(id) {
      return getRegionKey(id) + "__" + id[2];
    });
    pSel.exit().remove();
    pSel.enter().append("path");
    var angle = {};
    var radScale = d3.scale.log().domain([ 1, that.ixs().length ]).range([ 0.5 * size, 1.5 * size ]);
    pSel.attr({
      "fill": function(id) {
        var slice = cake[getRegionKey(id)][id[2]];
        return slice["color"];
      },
      "stroke": "black",
      "d": function(id) {
        var key = getRegionKey(id);
        var pie = cake[key][id[2]];
        var seg = pie["ratio"] * 360.0;
        if(!(key in angle)) {
          angle[key] = -90;
        }
        var begA = angle[key];
        angle[key] += seg;
        var endA = angle[key];
        var path = new jkjs.Path();
        var rad = radScale(pie["total"]);
        path.arcSegment(0, 0, 0, rad, begA, endA);
        return path;
      },
    }).style({
      "cursor": "pointer",
    }).on("dblclick", dblclick).order();

    gSelE.append("text").attr({
      "alignment-baseline": "central",
      "font-size": fontSize * 1.5,
      "font-family": "courier",
      "font-weight": "bolder",
      "stroke": "white",
      "stroke-width": 0.6,
      "text-anchor": "middle",
      "fill": "black",
    }).style({
      "cursor": "pointer",
      "user-select": "none",
    });

    gSel.selectAll("text").text(function(id) {
      var num = getRegion(buckets, id)["arr"].length;
      return num > 0 ? num : "";
    }).on("dblclick", dblclick);
  } // pieSummary

  var SUMMARY = barSummary;

  function drawItems(iSel, iSelE, xLookup, yLookup, numXs, numYs, leftXs, topYs, buckets, bucketLookup, ixMap) {
    var hs = size * 0.5;
    var rs = size * 0.3;

    function dblclickItem(ix) {
      dblclick(bucketLookup[ix]);
    } // dblclickItem

    function clickItem(ix) {
      click(ixMap[ix]);
    } // clickItem

    iSelE.append("circle").attr({
      "r": rs,
      "cx": hs,
      "cy": hs,
    }).style({
      "cursor": "pointer",
    });

    var iAttr = {
      "transform": function(ix) {
        var grp = bucketLookup[ix];
        var numX = numXs[xLookup[grp[0]]];
        var numY = numYs[yLookup[grp[1]]];
        var pos = getPos(buckets, bucketLookup, numX, numY, ix);
        pos[0] += leftXs[xLookup[grp[0]]];
        pos[1] += topYs[yLookup[grp[1]]];
        return "translate(" + pos + ")";
      },
    };
    iSelE.attr(iAttr).attr({
      "opacity": 0,
    });
    transition(iSel).attr(iAttr).attr({
      "opacity": 1,
    });

    iSel.selectAll("circle").attr({
      "stroke": function(ix) {
        return jkjs.util.getFontColor(getState(ix)["color"]);
      },
      "stroke-width": function(ix) {
        return ixMap[ix] === selectIx ? 1 : 0.25;
      },
      "fill": function(ix) {
        return getState(ix)["color"];
      },
    }).on("click", clickItem).on("dblclick", dblclickItem);
  } // drawItems

  var minX = 3;
  var minY = 3;
  var lastBuckets = {};
  var lastIxMap = {};
  this.update = function() {
    if(xGroups.length === 0 || yGroups.length === 0) return;
    var mh = marginH;
    var mw = marginW;
    var rw = Math.floor(jkjs.util.svgWidth(parent));
    var rh = Math.floor(jkjs.util.svgHeight(parent));
    var availableW = rw - mw * 2;
    var availableH = rh - mh * 2;

    var numYs = yGroups.map(function() {
      return Math.max(minY, Math.floor(Math.floor(availableH / size) / Math.max(yGroups.length, 1)));
    });
    var bArr = getBuckets(ixs, xGroups, yGroups);
    var buckets = bArr[0];
    var rIxs = bArr[2];
    var ixMap = bArr[3];
    fillStates(rIxs, buckets, ixMap);

    var bucketLookup = bArr[1];
    var numXs = getNumXs(buckets, xGroups, numYs, availableW);
    var overfull = isOverfull(xGroups, yGroups, numXs, numYs, buckets);

    var xLookup = {};
    xGroups.forEach(function(xGrp, ix) {
      xLookup[xGrp] = ix;
    });
    var yLookup = {};
    yGroups.forEach(function(yGrp, ix) {
      yLookup[yGrp] = ix;
    });

    var leftXsW = xGroups.reduce(function(cur, _, ix) {
      cur[0].push(cur[1]);
      return [ cur[0], cur[1] + numXs[ix] * size ];
    }, [ [], mw ]);
    var leftXs = leftXsW[0];
    var w = leftXsW[1] + mw;

    var topYsH = yGroups.reduce(function(cur, _, ix) {
      cur[0].push(cur[1]);
      return [ cur[0], cur[1] + numYs[ix] * size ];
    }, [ [], mh ]);
    var topYs = topYsH[0];
    var h = topYsH[1] + mh;

    svg.attr({
      "width": w,
      "height": h,
    });

    drawTexts(xGroups, yGroups, numXs, numYs, leftXs, topYs, mw, mh, w, h);

    var rSel = regions.selectAll("g.region").data(flattenBuckets(buckets), function(id) {
      return getRegionKey(id);
    });
    transition(rSel.exit()).attr({
      "opacity": 0,
    }).remove();
    var rSelE = rSel.enter().append("g").classed("region", true);
    drawRegions(rSel, rSelE, xLookup, yLookup, numXs, numYs, leftXs, topYs, buckets, overfull);

    var iIxs = overfull ? [] : rIxs;
    iIxs.sort(function(aix, bix) {
      var aGrp = bucketLookup[aix];
      var bGrp = bucketLookup[bix];
      var cmpX = d3.descending(xLookup[aGrp[0]], xLookup[bGrp[0]]);
      if(cmpX !== 0) return cmpX;
      var cmpY = d3.descending(yLookup[aGrp[1]], yLookup[bGrp[1]]);
      if(cmpY !== 0) return cmpY;
      return d3.ascending(getPosInBucket(buckets, bucketLookup, aix), getPosInBucket(buckets, bucketLookup, bix));
    });
    var iSel = items.selectAll("g.item").data(iIxs, function(ix) {
      return ix;
    });
    transition(iSel.exit()).attr({
      "opacity": 0,
    }).remove();
    var iSelE = iSel.enter().append("g").classed("item", true);
    iSel.order();
    drawItems(iSel, iSelE, xLookup, yLookup, numXs, numYs, leftXs, topYs, buckets, bucketLookup, ixMap);

    lastBuckets = buckets;
    lastIxMap = ixMap;
    posChange = false;
  };
}; // Slicer
