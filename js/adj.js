/**
 * Created by krause on 2016-11-10.
 */
function AdjacencyMatrix(sel, size, preSize, scale) {
  var that = this;
  var svg = sel.append("svg");

  var towardsPositive = false;
  this.towardsPositive = function(_) {
    if(!arguments.length) return towardsPositive;
    towardsPositive = !!_;
  };

  var scoreRatios = {};
  var points = [];
  this.points = function(ps) {
    var sr = {};
    points = ps.map(function(p, ix) {
      if(+p["ix"] !== ix) {
        throw {
          "err": "index mismatch",
          "p": p,
          "ix": ix,
        };
      }
      var score = +p["score"];
      if(!(score in sr)) {
        sr[score] = [ 0, 0 ];
      }
      var label = p["label"];
      sr[score][label ? 1 : 0] += 1;
      return {
        "id": ix,
        "score": score,
        "label": label,
        "meta": p["meta"],
      };
    });
    scoreRatios = sr;
  };

  var links = [];
  var linkInfo = {};
  this.neighbors = function(ns) {
    li = {};
    links = ns.map(function(n, ix) {
      li[+n["from"]] = ix;
      return {
        "source": +n["from"],
        "min_ix": +n["min_ix"],
        "max_ix": +n["max_ix"],
        "size": +n["size"],
        "chg_min": n["chg_min"],
        "chg_max": n["chg_max"],
      };
    });
    linkInfo = li;
  };

  var vecs = [];
  this.vecs = function(_) {
    if(!arguments.length) return vecs;
    vecs = _;
  };

  function getVec(ix) {
    return vecs[ix].join("\n");
  } // getVec

  var sep = "\n";
  function getTitle(chg) {
    var cs = [];
    if("0" in chg) {
      cs.push(chg["0"]["1"].map(function(c) {
        return "+" + c;
      }).join(sep));
    }
    if("1" in chg) {
      cs.push(chg["1"]["0"].map(function(c) {
        return "-" + c;
      }).join(sep));
    }
    return cs.join(sep);
  } // getTitle

  function getExplanationLength(chg) {
    return Object.keys(chg).reduce(function(p, v) {
      return Object.keys(chg[v]).reduce(function(pp, vv) {
        return pp + chg[v][vv].length;
      }, p);
    }, 0);
  } // getExplanationLength

  function getRows() {
    var ins = {};
    var outs = {};
    links.forEach(function(l) {
      var source = l["source"];
      var target = towardsPositive ? l["max_ix"] : l["min_ix"];
      var size = +l["size"];
      var chg = towardsPositive ? l["chg_max"] : l["chg_min"];
      var expl = getExplanationLength(chg);
      // if(!expl) {
      //   console.log(expl, chg);
      // }
      if(!(source in outs)) {
        outs[source] = points[source];
      }
      if(!(target in ins)) {
        ins[target] = {
          "point": points[target],
          "links": {},
        };
      }
      ins[target]["links"][source] = {
        "size": size,
        "chg": chg,
        "expl": expl,
      };
    });
    return [ ins, outs ];
  } // getRows

  function getColor(score) {
    var r = scoreRatios[score];
    if(!r) return "black";
    return scale(r[1] / (r[0] + r[1]));
  }

  this.update = function() {
    var io = getRows();
    var rows = io[0];
    var rowIxs = Object.keys(rows);
    var cols = io[1];
    var colIxs = Object.keys(cols);

    function getInDegree(r) {
      return Object.keys(r["links"]).length;
    } // getInDegree

    rowIxs.sort(function(ar, br) {
      var a = rows[ar];
      var b = rows[br];
      return d3.descending(getInDegree(a), getInDegree(b));
    });

    var maxInDegree = rowIxs.reduce(function(p, rix) {
      return Math.max(p, getInDegree(rows[rix]));
    }, 0);

    var w = preSize + maxInDegree * size;
    var h = rowIxs.length * size;
    svg.style({
      "width": w + "px",
      "height": h + "px",
    }).attr({
      "width": w,
      "height": h,
    });

    var rowSel = svg.selectAll("g.row").data(rowIxs, function(ix) {
      return ix;
    });
    rowSel.exit().remove();
    var rowSelE = rowSel.enter().append("g").classed("row", true);
    rowSelE.append("rect").classed("pre_rect", true);
    rowSelE.append("text").classed("pre_text", true);
    rowSelE.append("g").classed("cols", true);

    rowSel.order();
    rowSel.attr({
      "transform": function(_, ix) {
        return "translate(" + [ 0, ix * size ] + ")";
      },
    });
    rowSel.selectAll("rect.pre_rect").attr({
      "x": 0,
      "y": 0,
      "width": preSize,
      "height": size,
      "fill": function(rix) {
        var r = rows[rix];
        return getColor(r["point"]["score"]);
      },
    });
    rowSel.selectAll("text.pre_text").attr({
      "x": size + 0.2,
      "y": size * 0.8,
    }).text(function(rix) {
      var r = rows[rix];
      return r["point"]["id"] + " " + (r["point"]["label"] ? "T" : "F") + " (" + r["point"]["score"] + "): " + r["point"]["meta"];
    });

    var colSel = rowSel.selectAll("g.cols").attr({
      "transform": "translate(" + [ preSize, 0 ] + ")",
    }).selectAll("g.col").data(function(rix) {
      var res = colIxs.map(function(cix, ix) {
        return [ rix, cix, ix ];
      }).filter(function(id) {
        return id[1] in rows[id[0]]["links"];
      });
      res.sort(function(ac, bc) {
        var a = getLink(ac);
        var b = getLink(bc);
        return d3.ascending(a["expl"], b["expl"]);
      });
      return res;
    }, function(ix) {
      return ix[0] + " " + ix[1] + " " + ix[2];
    });
    colSel.exit().remove();
    var colSelE = colSel.enter().append("g").classed("col", true);
    colSelE.append("rect").classed("link_rect", true).append("title");
    colSelE.append("text").classed("link_text", true).append("title");

    function getSourcePoint(id) {
      return cols[id[1]];
    } // getTargetPoint

    function getTargetPoint(id) {
      return rows[id[0]]["point"];
    } // getTargetPoint

    function getLink(id) {
      return rows[id[0]]["links"][id[1]];
    } // getLink

    colSel.order();
    colSel.attr({
      "transform": function(_, ix) {
        return "translate(" + [ ix * size, 0 ] + ")";
      },
    });
    colSel.selectAll("title").text(function(id) {
      var n = getSourcePoint(id);
      var l = getLink(id);
      var str = n["id"] + "\n";
      str += (n["label"] ? "T" : "F") + " (" + n["score"] + ")\n";
      str += n["meta"] + "\n";
      str += getVec(n["id"]) + "\n";
      return str + getTitle(l["chg"]);
    });
    colSel.selectAll("rect.link_rect").attr({
      "x": 0,
      "y": 0,
      "width": size,
      "height": size,
      "fill": function(id) {
        var p = getSourcePoint(id);
        return getColor(p["score"]);
      },
    });
    colSel.selectAll("text.link_text").attr({
      "x": size * 0.2,
      "y": size * 0.8,
    }).text(function(id) {
      var link = getLink(id);
      return link["expl"];
    });
  }; // update
} // AdjacencyMatrix
