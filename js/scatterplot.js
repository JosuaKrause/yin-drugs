/**
 * Created by krause on 2016-02-04.
 */
function Scatterplot(sel, size, pad) {
  var that = this;
  var lastRightPoint = [ Number.NaN, Number.NaN ];
  var mouseActive = 0;
  var curMouse = null;
  var superSample = 2;
  var scatterplot = sel.append("canvas").attr({
    "width": size * superSample,
    "height": size * superSample,
  }).style({
    "display": "block",
    "width": size + "px",
    "height": size + "px",
  }).classed({
    "grab": true,
    "grabbing": false
  }).on("mousedown", function() {
    var p = mousePos();
    var left = d3.event.button === 0;
    if(!left && d3.event.button !== 2) {
      return;
    }
    scatterplot.classed({
      "grab": false,
      "grabbing": true
    });
    if(mouseDown(p, left)) {
      mouseActive = 1;
    }
  }).on("contextmenu", function() {
    var p = mousePos();
    if(p[0] === lastRightPoint[0] && p[1] === lastRightPoint[1]) {
      return;
    }
    lastRightPoint = p;
    d3.event.preventDefault();
  });
  d3.select("html").on("mousemove", function() {
    var p = mousePos();
    if(mouseActive > 0) {
      mouseActive = 2;
      mouseMove(p);
    }
    if(p[0] < 0 || p[0] >= size || p[1] < 0 || p[1] >= size) {
      if(curMouse) {
        curMouse = null;
        that.update();
      }
    } else {
      curMouse = p;
      that.update();
    }
  }).on("mouseup", function() {
    if(mouseActive > 0) {
      var p = mousePos();
      mouseUp(p, mouseActive > 1);
      scatterplot.classed({
        "grab": true,
        "grabbing": false
      });
      mouseActive = 0;
      that.update();
    }
  });

  var scale = 1;

  function mousePos() {
    return d3.mouse(scatterplot.node());
  }

  var selectRadius = 15;
  function getCirclePoints(pointSet, pos) {
    if(!posX || !posY) {
      return;
    }
    var x1 = pos[0];
    var y1 = pos[1];
    for(var ix = 0;ix < posX.length;ix += 1) {
      var x2 = posX[ix];
      var y2 = posY[ix];
      var dist = (x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2);
      if(dist < selectRadius*selectRadius) {
        pointSet[points[ix]["ix"]] = points[ix];
      }
    }
  }

  function getClosestPoint(pos) {
    if(!posX || !posY) {
      return null;
    }
    var x1 = pos[0];
    var y1 = pos[1];
    var point = null;
    var curDist = Number.POSITIVE_INFINITY;
    for(var ix = 0;ix < posX.length;ix += 1) {
      var x2 = posX[ix];
      var y2 = posY[ix];
      var dist = (x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2);
      if(dist < curDist) {
        point = points[ix];
        curDist = dist;
      }
    }
    return point;
  }

  var lasso = false;
  this.lasso = function(_) {
    if(!arguments.length) return lasso;
    lasso = !!_;
  };
  var lassoPath = [];
  function lassoSelection() {
    if(!posX || !posY) {
      return;
    }
    var pointSet = {};
    for(var ix = 0;ix < points.length;ix += 1) {
      if(pointInPolygon([ posX[ix], posY[ix] ], lassoPath)) {
        pointSet[points[ix]["ix"]] = points[ix];
      }
    }
    curPointSet = pointSet;
  } // lassoSelection

  function pointInPolygon(pos, arr) {
    var px = pos[0];
    var py = pos[1];

    function crossings(posA, posB) {
      var x0 = posA[0];
      var y0 = posA[1];
      var x1 = posB[0];
      var y1 = posB[1];
      if(py <  y0 && py <  y1) return 0;
      if(py >= y0 && py >= y1) return 0;
      // (y0 != y1) || console.warn("y0 == y1", posA, posB);
      if(px >= x0 && px >= x1) return 0;
      if(px <  x0 && px <  x1) return (y0 < y1) ? 1 : -1;
      var xintercept = x0 + (py - y0) * (x1 - x0) / (y1 - y0);
      if (px >= xintercept) return 0;
      return (y0 < y1) ? 1 : -1;
    }

    var posA = arr[arr.length - 1];
    var numCrossings = 0;
    for(var ix = 0;ix < arr.length;ix += 1) {
      var posB = arr[ix];
      numCrossings += crossings(posA, posB);
      posA = posB;
    }
    return (numCrossings & 1) !== 0;
  } // pointInPolygon

  var curPointSet = {};
  var curColor = null;
  function mouseDown(p, left) {
    if(lasso) {
      if(left) {
        lassoPath = [ p ];
        that.update();
        curColor = "gray";
        return true;
      }
      return false;
    }
    if(left) {
      var point = getClosestPoint(p);
      if(!point) {
        return false;
      }
      curColor = _colorCB(point["ix"]);
      if(curColor === "black") {
        curColor = "gray";
      }
    } else {
      curColor = "black";
    }
    getCirclePoints(curPointSet, p);
    return true;
  }

  function mouseMove(p) {
    if(lasso) {
      lassoPath.push(p);
      lassoSelection();
      that.update();
      return;
    }
    getCirclePoints(curPointSet, p);
  }

  function mouseUp(p, commit) {
    if(lasso) {
      lassoPath.push(p);
      lassoSelection();
      if(commit && curColor) {
        addToCluster(curColor, curPointSet);
      }
      lassoPath = [];
      that.update();
      curColor = null;
      return;
    }
    getCirclePoints(curPointSet, p);
    if(commit && curColor) {
      addToCluster(curColor, curPointSet);
    }
    curPointSet = {};
    curColor = null;
  }

  var addToCluster = function(c, ps) {};
  this.addToCluster = function(_) {
    if(!arguments.length) return addToCluster;
    addToCluster = _;
  };

  var points = [];
  this.points = function(_) {
    if(!arguments.length) return points;
    points = _;
  };

  var alpha = 0.3;
  this.alpha = function(_) {
    if(!arguments.length) return alpha;
    alpha = +_;
  };

  var jitter = 0;
  this.jitter = function(_) {
    if(!arguments.length) return jitter;
    jitter = +_;
  }

  var selected = {};
  this.selectPoints = function(_) {
    if(!arguments.length) return Object.keys(selected);
    selected = {};
    _.forEach(function(ix) {
      selected[ix] = true;
    });
  };

  var colorCB = function(ix) {
    if(curColor && ix in curPointSet) {
      return curColor;
    }
    return _colorCB(ix);
  };
  var _colorCB = function(ix) {
    return "black"
  };
  this.colorCB = function(_) {
    if(!arguments.length) return _colorCB;
    _colorCB = _;
  };

  var drawCB = function(dh, pos, ix, isSelected, x, y, size, color) {
    if(isSelected) {
      dh.cross(x, y, size, color);
    } else {
      dh.point(x, y, size, color);
    }
  };
  this.drawCB = function(_) {
    if(!arguments.length) return drawCB;
    drawCB = _;
  };

  var extent = [];
  function computePositions() {
    var posX = new Float32Array(points.length);
    var posY = new Float32Array(points.length);
    points.forEach(function(p, ix) {
      posX[ix] = +p["x"];
      posY[ix] = +p["y"];
    });

    var minX = Number.POSITIVE_INFINITY;
    var minY = Number.POSITIVE_INFINITY;
    var maxX = Number.NEGATIVE_INFINITY;
    var maxY = Number.NEGATIVE_INFINITY;
    for(var ix = 0;ix < points.length;ix += 1) {
      var x = posX[ix];
      var y = posY[ix];
      if(Number.isFinite(x) && Number.isFinite(y)) {
        minX = Math.min(minX, x);
        maxX = Math.max(maxX, x);
        minY = Math.min(minY, y);
        maxY = Math.max(maxY, y);
      } else {
        posX[ix] = Number.NaN;
        posY[ix] = Number.NaN;
      }
    }

    function jit() {
      if(!jitter) return 0;
      return jkjs.util.randomNorm() * jitter;
    }

    extent = [ minX, maxX, minY, maxY ];

    var computeX = function(x) {
      return pad + (x - minX) / (maxX - minX) * (size - 2*pad) + jit();
    }
    var computeY = function(y) {
      return pad + (1 - (y - minY) / (maxY - minY)) * (size - 2*pad) + jit();
    }

    for(var ix = 0;ix < posX.length;ix += 1) {
      posX[ix] = computeX(posX[ix]);
    }
    for(var ix = 0;ix < posY.length;ix += 1) {
      posY[ix] = computeY(posY[ix]);
    }
    return [ posX, posY, computeX, computeY ];
  } // computePositions

  this.getExtent = function() {
    return extent.slice();
  };

  var posX = null;
  var posY = null;
  function plot(ctx) {
    var spec = computePositions();
    posX = spec[0];
    posY = spec[1];

    ctx.lineWidth = scale;
    ctx.strokeStyle = "black";
    ctx.beginPath();
    ctx.rect(1, 1, size-2, size-2);
    ctx.stroke();

    var dots = new jkjs.util.Randomizer(posX.length);

    function drawDot(pos) {
      var ix = +points[pos]["ix"];
      var color = colorCB(ix);
      if(color === 'none') return;
      var x = posX[pos];
      if(Number.isFinite(x)) { // if one is NaN both are
        var y = posY[pos];
        dots.push([ pos, ix, color, x, y ]);
      }
    }

    for(var ix = 0;ix < posX.length;ix += 1) {
      drawDot(ix);
    }

    function DrawHelper(ctx) {
      this.point = function(x, y, size, color) {
        ctx.lineWidth = 1;
        ctx.fillStyle = color;
        ctx.fillRect(x - size, y - size, size*2, size*2);
      };
      this.cross = function(x, y, size, color) {
        ctx.strokeStyle = color;
        ctx.lineWidth = 2;
        ctx.beginPath();
        ctx.moveTo(x - 2*size, y - 2*size);
        ctx.lineTo(x + 2*size, y + 2*size);
        ctx.moveTo(x - 2*size, y + 2*size);
        ctx.lineTo(x + 2*size, y - 2*size);
        ctx.stroke();
      };
      this.ctx = function() {
        return ctx;
      };
    }; // DrawHelper

    ctx.save();
    ctx.globalAlpha *= alpha;
    var dotSize = 1.5 * scale;
    var dh = new DrawHelper(ctx);
    dots.each(function(dot) {
      var pos = dot[0];
      var ix = dot[1];
      var isSelected = ix in selected;
      var color = dot[2];
      var x = dot[3];
      var y = dot[4];
      drawCB(dh, pos, ix, isSelected, x, y, dotSize, color);
    });
    ctx.restore();
  } // plot

  this.update = function() {
    var spx = scatterplot.node().getContext("2d");
    spx.imageSmoothingEnabled = false;
    spx.globalAlpha = 1;
    spx.save();
    spx.scale(superSample, superSample);
    spx.clearRect(0, 0, size, size);
    if(lassoPath.length) {
      spx.save();
      spx.globalAlpha *= 0.5;
      spx.beginPath();
      lassoPath.forEach(function(p, ix) {
        if(ix) {
          spx.lineTo(p[0], p[1]);
        } else {
          spx.moveTo(p[0], p[1]);
        }
      });
      spx.fillStyle = curColor;
      spx.fill();
      spx.strokeStyle = "black";
      spx.stroke();
      spx.restore();
    }
    spx.save();
    plot(spx);
    spx.restore();
    if(curMouse) {
      var p = getClosestPoint(curMouse);
      if(p) {
        spx.save();
        spx.globalAlpha *= 0.5;
        spx.beginPath();
        spx.arc(curMouse[0], curMouse[1], selectRadius, 0, 2 * Math.PI, false);
        spx.fillStyle = curColor ? curColor : colorCB(+(p["ix"]));
        spx.fill();
        spx.lineWidth = 1;
        spx.strokeStyle = "black";
        spx.stroke();
        spx.restore();
      }
    }
    spx.restore();
  }; // update
} // Scatterplot
