/**
 * Created by krause on 2016-11-09.
 */
function Neighbograms(sel, width, height, w, h, size, fontSize, scale, getVecs, fmt) {
  var that = this;
  var div = sel.style({
    "height": height,
    "overflow-y": "scroll",
    "margin": "5px",
  }).append("div").style({
    "width": width,
  });
  var superSample = 2;

  var scoreRatios = {};
  var points = [];
  this.points = function(ps) {
    var sr = {};
    points = ps.map(function(p, ix) {
      if(+p["ix"] !== ix) {
        throw {
          "err": "index mismatch",
          "p": p,
          "ix": ix,
        };
      }
      var score = +p["score"];
      if(!(score in sr)) {
        sr[score] = [ 0, 0 ];
      }
      var label = p["label"];
      sr[score][label ? 1 : 0] += 1;
      return {
        "id": ix,
        "score": score,
        "label": label,
        "meta": p["meta"],
      };
    });
    scoreRatios = sr;
  };

  function getColor(score) {
    var r = scoreRatios[score];
    if(!r) return "black";
    return scale(r[1] / (r[0] + r[1]));
  }

  var vecs = {};
  this.vecs = function(_) {
    if(!arguments.length) return Object.keys(vecs);
    var v = {};
    _.forEach(function(f) {
      v[f] = true;
    });
    vecs = v;
  };

  function getVec(ix) {
    return vecs[ix].join("\n");
  } // getVec

  var maxD = 1e-6;
  this.maxD = function(_) {
    if(!arguments.length) return maxD;
    maxD = +_;
  };

  var fixD = false;
  this.fixD = function(_) {
    if(!arguments.length) return fixD;
    fixD = !!_;
  };

  var dm = [];
  var DM_IXS = 0;
  var DM_D = 1;
  var DM_MD = 2;
  var DM_CIXS = 3;
  this.distances = function(ds) {
    dm = ds.map(function(d) {
      var dists = Float64Array.from(d["d"], function(e) {
        return +e[DM_D];
      });
      var localMaxD = Math.max(jkjs.stat.max(dists), 1e-6);
      var ixs = Uint32Array.from(d["d"], function(e) {
        return +e[DM_IXS];
      });
      var cixs = Uint32Array.from(d["cixs"], function(ix) {
        return +ix;
      });
      return [ ixs, dists, localMaxD, cixs ];
    });
  };

  var NO_SVG = false;
  this.update = function() {
    var ixs = dm.map(function(_, ix) {
      return ix;
    });
    var ss = ixs.map(function(ix) {
      return -jkjs.stat.stddev(dm[ix][DM_D]);
    });
    ixs.sort(function(aix, bix) {
      var da = dm[aix];
      var db = dm[bix];
      var cmp0 = d3.descending(da[DM_CIXS].length, db[DM_CIXS].length);
      if(cmp0) return cmp0;
      var cmp1 = d3.descending(da[DM_IXS].length, db[DM_IXS].length);
      if(cmp1) return cmp1;
      return d3.ascending(ss[aix], ss[bix]);
    });

    var csel = div.selectAll("div").data(ixs, function(ix) {
      return ix;
    });
    csel.exit().remove();
    var cselE = csel.enter().append("div");
    cselE.append("canvas");
    if(!NO_SVG) {
      cselE.append("svg");
    }

    var maxFontSize = fontSize*1.5;
    var fontGap = fontSize*1.1;
    csel.order();
    csel.selectAll("canvas").style({
      "width": w + "px",
      "height": (h + maxFontSize) + "px",
    }).attr({
      "width": w * superSample,
      "height": (h + maxFontSize) * superSample,
    });
    if(!NO_SVG) {
      csel.selectAll("svg").style({
        "width": 0,
        "height": (h + maxFontSize) + "px",
        "margin-left": 5 + "px",
      });
    }

    function draw(ix, c) {
      var md = fixD ? maxD : dm[ix][DM_MD];
      var dd = dm[ix][DM_D];
      var cixs = {};
      dm[ix][DM_CIXS].forEach(function(cix) {
        cixs[cix] = true;
      });
      var maxCDist = 0;
      var elems = jkjs.util.rndFrom(dm[ix][DM_IXS], function(rix, eix) {
        var isClose = rix in cixs;
        if(isClose && dd[eix] > maxCDist) {
          maxCDist = dd[eix];
        }
        return {
          "color": getColor(points[rix]["score"]),
          "score": points[rix]["score"],
          "d": dd[eix],
          "is_close": isClose,
        };
      });

      // Canvas

      var ctx = c.select("canvas").node().getContext("2d");
      ctx.save();
      ctx.scale(superSample, superSample);
      ctx.clearRect(0, 0, w, h + maxFontSize);

      var margin = 2*size;
      function getX(x) {
        return x / md * (w - 2*margin) + margin;
      } // getX

      function getY(y) {
        return (1.0 - y) * (h - 2*margin) + margin;
      }

      ctx.strokeStyle = "black";
      ctx.lineWidth = 0.5;
      ctx.strokeRect(0, 0, w, h);

      ctx.beginPath();
      ctx.moveTo(getX(maxCDist), 0);
      ctx.lineTo(getX(maxCDist), h);
      ctx.stroke();

      ctx.save();
      elems.each(function(e) {
        var x = getX(e["d"]);
        var y = getY(e["score"]);
        ctx.fillStyle = e["color"];
        ctx.strokeStyle = e["is_close"] ? "crimson" : "black";
        ctx.lineWidth = 0.2;
        ctx.beginPath();
        ctx.arc(x, y, size, 0, 2 * Math.PI, false);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
      });
      ctx.restore();

      ctx.save();
      ctx.translate(margin, h + fontGap);
      ctx.font = fontSize + "px sans-serif";
      ctx.fillStyle = "black";
      ctx.textAlign = "left";
      ctx.fillText(fmt(0), 0, 0);
      ctx.translate(w - 2*margin, 0);
      ctx.textAlign = "right";
      ctx.fillText(fmt(md), 0, 0);
      ctx.restore();
      ctx.restore();

      // SVG

      if(!NO_SVG) {
        var svg = c.select("svg");
        var bw = 30;
        getVecs(ix, dm[ix][DM_CIXS], function(vals) {
          svg.style({
            "width": vals.length > 20 ? "calc(" + width + " - " + (w + 10) + "px)" : (bw * vals.length) + "px",
          });
          var g = svg.selectAll("g").data(vals, function(v) {
            return v[0];
          });
          g.exit().remove();
          var gE = g.enter().append("g");
          gE.append("rect");
          gE.append("text");
          g.order();
          g.attr({
            "transform": function(v, pos) {
              return "translate(" + [ pos * bw, 0 ] + ")";
            },
          });
          g.selectAll("rect").attr({
            "x": 0,
            "y": function(v) {
              return (1.0 - v[1]) * h;
            },
            "width": bw,
            "height": function(v) {
              return v[1] * h;
            },
            "fill": "lightgray",
            "stroke": "darkgray",
          });
          g.selectAll("text").attr({
            "text-anchor": "start",
            "alignment-baseline": "middle",
            "transform": "translate(" + [ bw * 0.5, h + maxFontSize ] + ") rotate(-90)",
            "font": fontSize + "px sans-serif",
          }).text(function(v) {
            return v[0];
          });
        });
      }
    } // draw

    function drawIfVisible() {
      toDraw = toDraw.filter(function(e) {
        var ix = e[0];
        var c = e[1];
        if(!jkjs.util.inViewport(c)) {
          return true;
        }
        draw(ix, c);
        return false;
      });
      if(toDraw.length) {
        sel.on("scroll", function() {
          drawIfVisible();
        });
      } else {
        sel.on("scroll", null);
      }
    } // drawIfVisible

    var toDraw = [];
    csel.each(function(ix) {
      var c = d3.select(this);
      toDraw.push([ ix, c ]);
    });

    drawIfVisible();
  };
} // Neighbograms
