/**
 * Created by krause on 2017-01-09.
 */

function Expl(sel, size, selectColor, fmt) {
  var that = this;
  var svg = sel.append("svg");

  var features = [];
  this.features = function(_) {
    if(!arguments.length) return features;
    features = _;
    if(features.some(function(e, ix) {
      return e["ix"] !== ix;
    })) {
      throw {
        "err": "incorrect indexing",
        "features": features,
      };
    }
  };

  this.selectableFeatures = function() {
    return features.filter(function(e) {
      return isSelectable(e);
    });
  };

  function isSelectable(e) {
    return !e["pinned"];
  }

  var forceWidth = true;
  this.forceWidth = function(_) {
    if(!arguments.length) return forceWidth;
    forceWidth = _;
  };

  var columns = [];
  this.columns = function(_) {
    if(!arguments.length) return columns;
    columns = _;
  };

  this.columnKeys = function() {
    return columns.filter(function(c) {
      return c[0];
    });
  };

  var sortBy = "ix";
  this.sortBy = function(_) {
    if(!arguments.length) return sortBy;
    sortBy = _;
  };

  var asc = true;
  this.asc = function(_) {
    if(!arguments.length) return asc;
    asc = !!_;
  };

  var selectIx = -1;
  this.selectIx = function(_) {
    if(!arguments.length) return selectIx;
    selectIx = _;
  };

  this.toggleCurrent = function() {
    if(selectIx < 0) return;
    if(!isSelectable(features[selectIx])) {
      selectIx = -1;
      return;
    }
    if(selectIx in multiSelect) {
      var s = {};
      Object.keys(multiSelect).forEach(function(ix) {
        if(+ix === selectIx) return;
        s[ix] = true;
      });
      multiSelect = s;
    } else {
      multiSelect[selectIx] = true;
    }
    selectIx = -1;
  };

  var multiSelect = {};
  this.selectMulti = function(_) {
    if(!arguments.length) return Object.keys(multiSelect).map(function(ix) {
      return +ix;
    });
    var s = {};
    _.forEach(function(ix) {
      if(isSelectable(features[ix])) {
        s[ix] = true;
      }
    });
    multiSelect = s;
  };

  function isSelected(ix) {
    return ix === selectIx || ix in multiSelect;
  } // isSelected

  var onClick = function(key, ix) {};
  this.onClick = function(_) {
    if(!arguments.length) return onClick;
    onClick = _;
  };

  function click(key, ix) {
    onClick(key, ix);
  }; // click

  var lastColumns = [];
  this.update = function() {
    var ixs = features.map(function(_, ix) {
      return ix;
    });
    ixs.sort(function(aix, bix) {
      var pa = features[aix]["pinned"];
      var pb = features[bix]["pinned"];
      if(pa || pb) {
        if(pa && pb) {
          return d3.ascending(features[aix]["ix"], features[bix]["ix"]);
        }
        return d3.descending(+pa, +pb);
      }
      var a = features[aix][sortBy];
      var b = features[bix][sortBy];
      var cmp = asc ? d3.ascending(a, b) : d3.descending(a, b);
      if(cmp !== 0) {
        return cmp;
      }
      return d3.ascending(features[aix]["ix"], features[bix]["ix"]);
    });
    ixs.unshift(-1);

    var wsize = size * 2;
    var hsize = size;
    var fontSize = size * 0.6;
    var fNameSize = size * 6;
    var rh = hsize * ixs.length + 2;
    if(forceWidth) {
      svg.attr({
        "width": fNameSize + wsize * columns.length + 2,
        "height": rh,
      });
    } else {
      svg.style({
        "width": "100%",
        "height": rh + "px",
      }).attr({
        "height": rh + 2,
      });
      fNameSize = Math.floor(jkjs.util.svgWidth(svg) - wsize * columns.length - 2);
    }
    if(columns.length !== lastColumns.length || columns.some(function(c, ix) {
      return lastColumns[ix][0] !== c[0];
    })) {
      svg.selectAll("*").remove();
    }
    lastColumns = columns;

    var maxs = features.reduce(function(p, _, ix) {
      return p.map(function(v, cix) {
        var key = columns[cix][0];
        if(key === "") {
          return 1;
        }
        if(!features[ix]["numeric"]) {
          return v;
        }
        return Math.max(v, features[ix][key]);
      });
    }, columns.map(function(_) {
      return 1;
    }));
    var gSel = svg.selectAll("g.row").data(ixs, function(ix) {
      return ix;
    });
    gSel.exit().remove();
    var gSelE = gSel.enter().append("g").classed("row", true);

    gSelE.append("text").classed("fname", true).attr({
      "font-size": fontSize,
      "font-family": "courier",
      "font-weight": "lighter",
      "stroke": "none",
      "text-anchor": "end",
      "alignment-baseline": "central",
    }).style({
      "cursor": "pointer",
      "user-select": "none",
    }).on("click", function(ix) {
      click("feature", ix);
    });

    columns.forEach(function(c, cix) {
      var key = c[0];
      if(key === "") {
        return;
      }
      gSelE.append("rect").classed(key, true).attr({
        "stroke": "black",
        "stroke-width": 1,
        "width": wsize - 1,
        "height": hsize - 1,
        "x": wsize * cix + 0.5,
        "y": 0.5,
        "fill": "white",
      }).style({
        "cursor": "pointer",
      }).on("click", function(ix) {
        click(key, ix);
      }).append("title");
      gSelE.append("text").classed(key, true).attr({
        "x": wsize * (cix + 0.5) + 0.5,
        "y": hsize * 0.5 + 0.5,
        "font-size": fontSize,
        "font-family": "courier",
        "font-weight": "lighter",
        "stroke": "none",
        "text-anchor": "middle",
        "alignment-baseline": "central",
      }).style({
        "cursor": "pointer",
        "user-select": "none",
      }).on("click", function(ix) {
        click(key, ix);
      });
    });

    gSel.order();
    gSel.attr({
      "transform": function(_, pos) {
        return "translate(" + [ 1 + fNameSize, hsize * pos + 1 ] + ")";
      },
    });

    var done = {};
    function drawAll() {
      var success = false;
      gSel.each(function(ix, pos) {
        if(ix in done) {
          return;
        }
        success = true;
        var rowSel = d3.select(this);
        if(!jkjs.util.inViewport(rowSel)) {
          return;
        }
        draw(rowSel);
        done[ix] = true;
      });
      return success;
    } // drawAll

    function draw(rowSel) {
      rowSel.selectAll("text.fname").each(function(ix) {
        var textSel = d3.select(this);
        var name = ix < 0 ? (columns.length > 0 ? "features" : "") : features[ix]["feature"];
        jkjs.text.display(textSel, name, {
          "x": -fNameSize,
          "y": 0.5,
          "width": fNameSize - 2,
          "height": hsize - 1,
        }, true, jkjs.text.align.right, jkjs.text.position.center, true, true);
      });
      rowSel.selectAll("title").text(function(ix) {
        if(ix < 0) {
          return "";
        }
        return features[ix]["feature"];
      });

      columns.forEach(function(c, cix) {
        var key = c[0];
        var text = c[1];
        var color = c.length > 2 ? c[2] : "white";
        if(key === "") {
          return;
        }
        var scale = d3.scale.linear().domain([ 0, maxs[cix] ]);
        rowSel.selectAll("rect." + key).attr({
          "stroke": function(ix) {
            if(ix < 0) {
              return "white";
            }
            return features[ix]["in_expl"] ? "red" : "black";
          },
          "fill": function(ix) {
            if(ix < 0) {
              return color;
            }
            if(isSelected(ix)) {
              return selectColor;
            }
            if(!features[ix]["numeric"]) {
              return features[ix][key] !== "" ? features[ix]["color"] : "white";
            }
            scale.range([ "white", features[ix]["color"] ]);
            return scale(features[ix][key]);
          },
        });
        rowSel.selectAll("text." + key).attr({
          "fill": function(ix) {
            if(ix < 0) {
              return jkjs.util.getFontColor(color);
            }
            if(isSelected(ix)) {
              return jkjs.util.getFontColor(selectColor);
            }
            if(!features[ix]["numeric"]) {
              return jkjs.util.getFontColor(features[ix][key] !== "" ? features[ix]["color"] : "white");
            }
            scale.range([ "white", features[ix]["color"] ]);
            return jkjs.util.getFontColor(scale(features[ix][key]));
          },
        }).text(function(ix) {
          if(ix < 0) {
            return text;
          }
          if(!features[ix]["numeric"]) {
            return features[ix][key];
          }
          return fmt(features[ix][key]);
        });
      });
    } // draw

    drawAll();
    sel.on("scroll", function() {
      if(!drawAll()) {
        sel.on("scroll", null);
      }
    });
  };
} // Expl
