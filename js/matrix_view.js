/**
 * Created by krause on 2016-02-02.
 */
function MatrixView(sel, fmt, colors, highColor) {
  var that = this;
  var right = sel.append("div").style({
    "overflow-x": "auto",
    "overflow-y": "hidden",
    "margin": "0 5px",
    "display": "block",
    "float": "left",
    "vertical-align": "top",
  });
  var headSvgParent = right.append("div").style({
    "margin": 0,
    "padding": 0,
    "display": "block",
    "vertical-align": "bottom",
  });
  var headSvg = headSvgParent.append("svg").style({
    "margin": 0,
    "padding": 0,
  });

  var left = sel.append("div").style({
    "margin": "0 5px",
    "display": "inline-block",
    "float": "right",
    "vertical-align": "top",
  });
  var headDiv = left.append("div").style({
    "margin": 0,
    "padding": 0,
    "display": "block",
    "vertical-align": "bottom",
  });
  var mainLeft = left.append("div").style({
    "margin": 0,
    "padding": 0,
    "display": "block",
    "vertical-align": "top",
  });
  var headSvgBars = headSvg.append("g");
  var headSvgText = headSvg.append("g");
  var mainRight = right.append("div").style({
    "margin": 0,
    "padding": 0,
    "display": "block",
  });

  var catColors = {
    "pos": colors[0],
    "mid": colors[1],
    "neg": colors[2],
  };
  function Cluster(name, countPos, countTotal, featureLength, complexity, fidelity) {
    var cthis = this;
    var cat = name.length >= 3 ? name.slice(0, 3) : "";

    var curFix = 0;
    var featureMap = {};
    function getFix(name) {
      if(!(name in featureMap)) {
        if(curFix >= featureLength) {
          throw {
            "err": "too many features!",
            "curFix": curFix,
            "total": featureLength,
          };
        }
        featureMap[name] = curFix;
        curFix += 1;
      }
      return featureMap[name];
    } // getFix

    var valsPos = {};
    var valsNeg = {};
    var featureWeights = {};

    var ixs = new Uint32Array(0);
    this.setIxs = function(newIxs) {
      ixs = new Uint32Array(newIxs.length);
      newIxs.forEach(function(ix, pos) {
        ixs[pos] = +ix;
      });
    };
    this.getIxs = function() {
      return ixs;
    };
    this.getVals = function() {
      var res = Object.keys(valsPos);
      res.sort(d3.ascending);
      return res;
    };

    this.setVals = function(features) {
      features.forEach(function(f) {
        var fix = getFix(f["name"]);
        f["values"].forEach(function(v) {
          var vv = v["value"];
          var s = v["stats"];
          if(!(vv in valsPos)) {
            valsPos[vv] = new Uint32Array(featureLength);
          }
          if(!(vv in valsNeg)) {
            valsNeg[vv] = new Uint32Array(featureLength);
          }
          valsPos[vv][fix] = getPos(s);
          valsNeg[vv][fix] = getNeg(s);
        });
      });
    };
    this.setWeights = function(features) {
      features.forEach(function(f) {
        var fix = getFix(f["name"]);
        Object.keys(f).forEach(function(k) {
          if(k === "name" || k === "values") return;
          if(!(k in featureWeights)) {
            featureWeights[k] = new Float64Array(featureLength);
          }
          featureWeights[k][fix] = +f[k];
        });
      });
      maxWeights = {};
    };
    this.getWeight = function(fname, wname) {
      return featureWeights[wname][getFix(fname)];
    };
    var maxWeights = {};
    this.getMaxWeight = function(wname) {
      if(!(wname in maxWeights)) {
        maxWeights[wname] = jkjs.stat.max(featureWeights[wname]);
      }
      return maxWeights[wname];
    };
    this.isInPath = function(f, vix) {
      var fix = getFix(f);
      if(arguments.length < 2) {
        return featureWeights["paths"][fix] > 0;
      }
      var vv = vix + 1.0;
      return featureWeights["paths"][fix] === vv;
    };
    this.complexity = function() {
      return complexity;
    };
    this.fidelity = function() {
      return fidelity;
    };
    this.getPosNeg = function(f, v) {
      if(arguments.length < 1) {
        if(!features.length) {
          return [ 0, 0 ];
        }
        return cthis.getPosNeg(features[0]);
      }
      if(arguments.length < 2) {
        return Object.keys(valsPos).reduce(function(p, v) {
          var res = cthis.getPosNeg(f, v);
          return [ res[0] + p[0], res[1] + p[1] ];
        }, [ 0, 0 ]);
      }
      var fix = getFix(f);
      return [ valsPos[v][fix], valsNeg[v][fix] ];
    };
    this.getTotalValue = function(f, v) {
      if(arguments.length < 2) {
        return Object.keys(valsPos).reduce(function(p, v) {
          return p + cthis.getTotalValue(f, v);
        }, 0);
      }
      var fix = getFix(f);
      return valsPos[v][fix] + valsNeg[v][fix];
    }
    this.getTotal = function() {
      return countTotal;
    };
    this.getCatColor = function() {
      return catColors[cat];
    };
  } // Cluster

  // tp, tn, fp, fn, tign, fign
  function getPos(s) {
    return +s[0] + +s[3] + +s[4]; // tp + fn + tign
  } // getPos
  function getNeg(s) {
    return +s[1] + +s[2] + +s[5]; // tn + fp + fign
  } // getNeg

  var clusters = {};
  var clusterOrder = [];
  var summaryCluster = null;
  var features = [];
  var vals = [];
  this.initClusters = function(data) {
    clusters = {};
    clusterOrder = [];
    var labelPos = getPos(data["stats"]);
    features = data["features"].map(function(f) {
      return f["name"];
    });
    summaryCluster = new Cluster("", labelPos, labelPos + getNeg(data["stats"]), data["features"].length, 1, 1);
    data["clusters"].forEach(function(c) {
      var n = c["name"];
      // points
      clusters[n] = new Cluster(n, +c["label_pos"], +c["total"], c["features"].length, +c["complexity"], +c["fidelity"]);
      clusters[n].setIxs(c["ixs"]);
      clusterOrder.push(n);
    });
  }; // initClusters
  this.finishClusters = function() {
    clusterOrder.sort(function(a, b) {
      var ca = clusters[a];
      var cb = clusters[b];
      var pa = ca.getPosNeg()[1] / ca.getTotal();
      var pb = cb.getPosNeg()[1] / cb.getTotal();
      var cmp = d3.ascending(pa, pb);
      if(cmp !== 0) return cmp;
      return d3.descending(ca.getTotal(), cb.getTotal());
    });
  };
  this.setVals = function(data) {
    summaryCluster.setVals(data["features"]);
    summaryCluster.setWeights(data["features"]);
    vals = summaryCluster.getVals();
    data["clusters"].forEach(function(c) {
      var n = c["name"];
      clusters[n].setVals(c["features"]);
    });
  }; // setVals
  this.setWeights = function(data) {
    clusterOrder.forEach(function(name, cix) {
      clusters[name].setWeights(data[cix]);
    });
  }; // setWeights

  this.getClusterIxs = function() {
    return clusterOrder.map(function(name) {
      return clusters[name].getIxs();
    });
  }; // getClusterIxs
  var zerosFeatures = {};
  this.sortFeatures = function(weight) {
    var fws = {};
    features.forEach(function(f) {
      fws[f] = clusterOrder.reduce(function(p, c) {
        return p + clusters[c].getWeight(f, weight);
      }, 0);
    });
    zerosFeatures = {};
    var vvs = {};
    features.forEach(function(f) {
      vvs[f] = clusterOrder.reduce(function(p, c) {
        return p + clusters[c].getTotalValue(f, "1");
      }, 0);
      var inAnyPath = clusterOrder.some(function(c) {
        return clusters[c].isInPath(f);
      });
      if(!inAnyPath) {
        zerosFeatures[f] = true;
      }
    });

    features.sort(function(fa, fb) {
      var cmp = d3.descending(fws[fa], fws[fb]);
      if(cmp !== 0) return cmp;
      return d3.descending(vvs[fa], vvs[fb]);
    });
  }; // sortFeatures

  var highlightF = null;
  var highlightC = null;
  this.highlight = function(c, f) {
    highlightC = c;
    highlightF = f;
    that.quickUpdate();
  }; // highlight

  function getC(b) {
    return clusters[b[0]];
  } // getC

  function getF(b) {
    return b[1];
  } // getF

  this.quickUpdate = function() {
    headSvgText.selectAll("text.name").attr({
      "font-weight": function(f) {
        return f === highlightF ? "bold" : null;
      },
    });
    headSvgBars.selectAll("rect.bars").attr({
      "fill": function(f) {
        return f === highlightF ? highColor : "lightgray";
      },
    });
    mainRight.selectAll("div.row").selectAll("rect.v_s").attr({
      "opacity": function(b) {
        return highlightF === getF(b) ? 0.3 : 0;
      },
    });
  };

  this.update = function() {
    var vFs = features.filter(function(f) {
      return !(zerosFeatures[f]);
    });
    var fontSize = 17;
    var margin = 3;
    var itemW = fontSize + margin;
    var itemH = function(c) {
      return itemW;
    };
    var maxTextLength = 120;
    var width = (vFs.length + 1) * itemW + maxTextLength;
    var height = function(c) {
      return itemH(c);
    };
    headSvgParent.style({
      "width": width + "px",
      "height": maxTextLength + "px",
    });
    headSvg.style({
      "width": width + "px",
      "height": maxTextLength + "px",
    }).attr({
      "viewBox": "0 0 " + width + " " + maxTextLength,
    });

    var ht = headSvgText.selectAll("text.name").data(vFs, function(f) {
      return f;
    });
    ht.exit().remove();
    ht.enter().append("text").classed("name", true);
    ht.order();
    ht.attr({
      "transform": function(f, ix) {
        var x = (ix + 1) * itemW;
        return "translate("+x+" "+maxTextLength+") rotate(-45) translate(5 -5)";
      },
      "font-family": "courier",
      "font-size": fontSize,
      "text-anchor": "start",
    }).text(function(f) {
      return f;
    }).on("mouseenter", function(f) {
      that.highlight(null, f);
    });

    var hb = headSvgBars.selectAll("rect.bars").data(vFs, function(f) {
      return f;
    });
    hb.exit().remove();
    hb.enter().append("rect").classed("bars", true);
    hb.order();
    hb.attr({
      "transform": function(f, ix) {
        var x = (ix + 1) * itemW;
        return "translate("+x+" "+maxTextLength+") rotate(-45) translate(5 -5) translate(0 "+(-fontSize*0.5)+")";
      },
      "width": function(f) {
        return (summaryCluster.getWeight(f, "importance") / summaryCluster.getMaxWeight("importance")) * maxTextLength;
      },
      "height": fontSize*0.5,
      "stroke": "gray",
    });

    var sigsD = mainRight.selectAll("div.row").data(clusterOrder, function(n) {
      return n;
    });
    sigsD.exit().remove();
    var sigsDE = sigsD.enter().append("div").classed("row", true);
    var sigsE = sigsDE.append("svg");
    sigsE.append("g").classed("layer_v", true);
    sigsE.append("g").classed("layer_p", true);
    sigsE.append("g").classed("layer_s", true);
    sigsE.append("g").classed("layer_g", true);
    sigsD.order();
    sigsD.style({
      "width": width + "px",
      "height": function(cix) {
        return height(clusters[cix]) + "px";
      },
      "margin": 0,
      "padding": 0,
    });
    var sigs = sigsD.selectAll("svg").attr({
      "viewBox": function(cix) {
        return "0 0 " + width + " " + height(clusters[cix]);
      },
    });

    var yOrder = {
      "0": 1,
      "1": 0,
      "p": 0,
      "s": 0,
    };
    var yRev = {
      "0": false,
      "1": true,
      "p": false,
      "s": false,
    };

    function fsVals(sel, val, valName, strokeColor, getRatio, getColor) {
      var fs = sel.selectAll("rect." + valName).data(function(r) {
        return vFs.map(function(f) {
          return [ r, f ];
        });
      }, function(b) {
        return b;
      });
      fs.exit().remove();
      fs.enter().append("rect").classed(valName, true);
      fs.order();

      fs.attr({
        "width": itemW,
        "height": function(b) {
          var c = getC(b);
          return itemH(c) * getRatio(c, getF(b), val);
        },
        "x": function(b, ix) {
          return (ix + 0.5) * itemW;
        },
        "y": function(b) {
          var c = getC(b);
          if(yRev[val]) {
            return (yOrder[val] + 1) * itemH(c) - itemH(c) * getRatio(c, getF(b), val);
          }
          return yOrder[val] * itemH(c);
        },
        "stroke-width": 1,
        "stroke": strokeColor,
        "fill": function(b) {
          return getColor(getC(b), getF(b), val);
        },
      });
    } // fsVals

    var colorDist = d3.scale.linear().domain([ 0, 0.5, 1 ]).range([ colors[0], colors[1], colors[2] ]);
    var colorGray = d3.scale.linear().domain([ 0, 1 ]).range([ "white", "black" ]);
    var oneIx = -1;
    var zeroIx = -1;
    vals.forEach(function(v, vix) {
      if(v !== "1") {
        zeroIx = vix;
        return;
      }
      oneIx = vix;
      fsVals(sigs.selectAll("g.layer_v"), v, "v_" + v, "lightgray", function(c, f, val) { // getRatio
        return 1.0;
        // var total = c.getTotal();
        // var tv = c.getTotalValue(f, val);
        // return tv / total;
      }, function(c, f, val) { // getColor
        var posNeg = c.getPosNeg(f);
        var tv = c.getTotalValue(f);
        return colorDist(tv > 0 ? (((posNeg[1] / tv) - 0.5) * 0.5 + 0.5) : 0.5);
        // var posNeg = c.getPosNeg(f, val);
        // var tv = c.getTotalValue(f, val);
        // return colorDist(tv > 0 ? posNeg[1] / tv : 0.5);
        // var total = c.getTotal();
        // var tv = c.getTotalValue(f, val);
        // return colorGray(0);//colorGray(tv / total);
      });
    });
    // fsVals(sigs.selectAll("g.layer_p"),"p", "v_p", "none", function(c, f, val) { // getRatio
    //   var total = c.getTotal();
    //   var tv = c.getTotalValue(f, "1");
    //   return 1.0 - tv / total;
    // }, function(c, f, val) { // getColor
    //   return c.isInPath(f, oneIx) ? "black" : c.isInPath(f, zeroIx) ? "gray" : "none";
    // });
    fsVals(sigs.selectAll("g.layer_s"),"s", "v_s", "none", function(c, f, val) { // getRatio
      return height(c) / itemH(c);
    }, function(c, f, val) { // getColor
      return highColor;
    });
    sigs.selectAll("rect.v_s").on("mouseenter", function(b) {
      that.highlight(getC(b), getF(b));
    });

    var glyph = sigs.selectAll("g.layer_g").selectAll("path.explain").data(function(r) {
      return vFs.map(function(f) {
        return [ r, f ];
      });
    }, function(b) {
      return b;
    });
    glyph.exit().remove();
    glyph.enter().append("path").classed("explain", true);
    glyph.order();
    glyph.attr({
      "transform": function(b, ix) {
        var c = getC(b);
        return "translate("+((ix + 1) * itemW)+" "+(itemH(c) * 0.5)+")";
      },
      "stroke-width": 1,
      "stroke": "black",
      "fill": function(b) {
        return getC(b).isInPath(getF(b), oneIx) ? "black" : getC(b).isInPath(getF(b), zeroIx) ? "black" : "none";
      },
      "d": function(b) {
        var radius = getC(b).isInPath(getF(b), oneIx) ? 3 : 5;
        return getC(b).isInPath(getF(b), oneIx)
        ? "M -X,0 a X,X 0 1,0 Y,0 a -X,-X 0 1,0 -Y,0".replace(/X/g, radius).replace(/Y/g, radius*2)
        : getC(b).isInPath(getF(b), zeroIx)
        ? "M-X -X lY Y M-X X lY -Y".replace(/X/g, radius).replace(/Y/g, radius*2)
        : "M0 0";
      }
    });

    var maxCmplx = 0;
    var maxSize = 0;
    clusterOrder.forEach(function(cix) {
      var c = clusters[cix];
      maxCmplx = Math.max(maxCmplx, c.complexity());
      maxSize = Math.max(maxSize, c.getTotal());
    });

    var infoW = 50;
    var infos = {
      "cmplx": function(c) {
        return fmt(c.complexity());
      },
      "size": function(c) {
        return fmt(c.getTotal());
      },
      "pos %": function(c) {
        return fmt(c.getPosNeg()[0] / c.getTotal() * 100.0) + "%";
      },
      "fidelity": function(c) {
        return fmt(c.fidelity());
      },
      "label": function(c) {
        return " ";
      },
    };
    var infoColor = {
      "cmplx": function(c) {
        return colorGray(c.complexity() / maxCmplx);
      },
      "size": function(c) {
        return colorGray(c.getTotal() / maxSize);
      },
      "pos %": function(c) {
        return colorDist(1.0 - c.getPosNeg()[0] / c.getTotal());
      },
      "fidelity": function(c) {
        return colorGray(c.fidelity());
      },
      "label": function(c) {
        return c.getCatColor();
      },
    };
    var infoOrder = [
      "label",
      "pos %",
      "fidelity",
      "size",
      "cmplx",
    ];
    headDiv.style({
      "width": (infoOrder.length * infoW) + "px",
      "height": maxTextLength + "px",
    });
    var hinf = headDiv.selectAll("div").data(infoOrder);
    hinf.exit().remove();
    hinf.enter().append("div");
    hinf.style({
      "width": infoW + "px",
      "height": maxTextLength + "px",
      "display": "table-cell",
      "vertical-align": "bottom",
      "text-align": "right",
    }).text(function(t) {
      return t;
    }).order();

    var allowedWidth = "80vw";
    var lw = infoOrder.length * infoW;
    left.style({
      "width": lw + "px",
    });
    right.style({
      "width": "calc(" + allowedWidth + " - " + lw + "px)",
    });

    var isel = mainLeft.selectAll("div.info").data(clusterOrder, function(n) {
      return n;
    });
    isel.exit().remove();
    isel.enter().append("div").classed("info", true);
    isel.style({
      "width": lw + "px",
      "height": function(c) {
        return height(c) + "px";
      },
    });

    var infs = isel.selectAll("div.infs").data(function(c) {
      return infoOrder.map(function(n) {
        return [ c, n ];
      });
    }, function(v) {
      return v[0] + "_" + v[1];
    });
    infs.exit().remove();
    infs.enter().append("div").classed("infs", true);
    infs.style({
      "width": infoW + "px",
      "display": "table-cell",
      "text-align": "right",
      "background": function(v) {
        return infoColor[v[1]](clusters[v[0]]);
      },
      "color": function(v) {
        return jkjs.util.getFontColor(infoColor[v[1]](clusters[v[0]]));
      },
    }).text(function(v) {
      return infos[v[1]](clusters[v[0]]);
    }).order();

    that.quickUpdate();
  }; // update
} // MatrixView
