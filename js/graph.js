/**
 * Created by krause on 2016-11-09.
 */
function Graph(sel, width, height, scale) {
  var that = this;
  var svg = sel.append("svg").style({
    "width": width,
    "height": height,
  });
  var zoom = d3.behavior.zoom().scaleExtent([ 0.1, 10 ]);
  var g = svg.append("g");
  zoom.on("zoom", function() {
    g.attr("transform", "translate(" + d3.event.translate + ") scale(" + d3.event.scale + ")");
  });
  svg.call(zoom);

  var linkSel = g.append("g");
  var nodeSel = g.append("g");

  var towardsPositive = false;
  this.towardsPositive = function(_) {
    if(!arguments.length) return towardsPositive;
    towardsPositive = !!_;
  };

  var scoreRatios = {};
  var points = [];
  this.points = function(ps) {
    var sr = {};
    points = ps.map(function(p, ix) {
      if(+p["ix"] !== ix) {
        throw {
          "err": "index mismatch",
          "p": p,
          "ix": ix,
        };
      }
      var score = +p["score"];
      if(!(score in sr)) {
        sr[score] = [ 0, 0 ];
      }
      var label = p["label"];
      sr[score][label ? 1 : 0] += 1;
      return {
        "id": ix,
        "score": score,
        "label": label,
        "meta": p["meta"],
      };
    });
    scoreRatios = sr;
  };

  var links = [];
  var linkInfo = {};
  this.neighbors = function(ns) {
    li = {};
    links = ns.map(function(n, ix) {
      li[+n["from"]] = ix;
      return {
        "source": +n["from"],
        "min_ix": +n["min_ix"],
        "max_ix": +n["max_ix"],
        "size": +n["size"],
        "chg_min": n["chg_min"],
        "chg_max": n["chg_max"],
      };
    });
    linkInfo = li;
  };

  function getColor(score) {
    var r = scoreRatios[score];
    if(!r) return "black";
    return scale(r[1] / (r[0] + r[1]));
  }

  var vecs = [];
  this.vecs = function(_) {
    if(!arguments.length) return vecs;
    vecs = _;
  };

  var force = d3.layout.force().charge(-50).nodes([]).links([]);
  this.update = function() {
    var w = jkjs.util.svgWidth(svg);
    var h = jkjs.util.svgHeight(svg);
    force.stop();

    function getVec(ix) {
      return vecs[ix].join("\n");
    } // getVec

    function getTitle(l) {
      var sep = "\n";
      var chg = l["chg"];
      var cs = [];
      if("0" in chg) {
        cs.push(chg["0"]["1"].map(function(c) {
          return "+" + c;
        }).join(sep));
      }
      if("1" in chg) {
        cs.push(chg["1"]["0"].map(function(c) {
          return "-" + c;
        }).join(sep));
      }
      return cs.join(sep);
    } // getTitle

    function getExplanationLength(chg) {
      return Object.keys(chg).reduce(function(p, v) {
        return Object.keys(chg[v]).reduce(function(pp, vv) {
          return pp + chg[v][vv].length;
        }, p);
      }, 0);
    } // getExplanationLength

    var actualLinks = links.map(function(l) {
      var res = {
        "source": l["source"],
        "target": towardsPositive ? l["max_ix"] : l["min_ix"],
        "size": +l["size"],
        "chg": towardsPositive ? l["chg_max"] : l["chg_min"],
      };
      res["expl_length"] = getExplanationLength(res);
      return res;
    });
    force.size([ w, h ]).nodes(points).links(actualLinks);
    // force.linkDistance(function(l) {
    //   return l["expl_length"] * 40.0;
    // });

    var link = linkSel.selectAll("line").data(actualLinks);
    link.exit().remove();
    link.enter().append("line").append("title");
    link.attr({
      "stroke-width": 0.5,
      "stroke": "black",
    });
    link.selectAll("title").text(function(l) {
        return getTitle(l);
    });

    var node = nodeSel.selectAll("circle").data(points);
    node.exit().remove();
    node.enter().append("circle").append("title");
    node.attr({
      "r": function(n) {
        return 5;
        // return Math.log(actualLinks[linkInfo[n["id"]]]["size"]) * 4 + 5;
      },
      "stroke": "black",
      "stroke-width": 0.1,
      "fill": function(n) {
        return getColor(n["score"]);
      },
    }).sort(function(a, b) {
      return d3.descending(actualLinks[linkInfo[a["id"]]]["size"], actualLinks[linkInfo[b["id"]]]["size"]);
    });
    node.selectAll("title").text(function(n) {
      var str = n["id"] + "\n";
      str += (n["label"] ? "T" : "F") + " (" + n["score"] + ")\n";
      str += n["meta"] + "\n";
      str += getVec(n["id"]) + "\n";
      return str + getTitle(actualLinks[linkInfo[n["id"]]]);
    });

    force.on("tick", function() {
      link.attr({
        "x1": function(d) {
          return d["source"]["x"];
        },
        "y1": function(d) {
          return d["source"]["y"];
        },
        "x2": function(d) {
          return d["target"]["x"];
        },
        "y2": function(d) {
          return d["target"]["y"];
        },
      });
      node.attr({
        "cx": function(d) {
          return d["x"];
        },
        "cy": function(d) {
          return d["y"];
        },
      });
    }).start();
  };
} // Graph
