#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import csv
import sys
import json
import random

import numpy as np
from sklearn.metrics import roc_auc_score
from sklearn.ensemble import RandomForestClassifier

class Rule(object):
    def __init__(self, obj, all_features_set):
        self._name = obj["name"]
        if self._name == "label" or self._name == "pred" or self._name == "meta":
            raise ValueError("invalid name: {0}".format(self._name))
        self._chance = int(obj["chance"])
        self._flip = float(obj["flip"])
        self._label = float(obj["label"])
        self._features = {}
        for (name, prob) in obj["features"]:
            self._features[name] = float(prob)
            all_features_set.add(name)

    def __str__(self):
        features = ""
        for (name, prob) in self._features.items():
            if features:
                features += " ∧ "
            features += "{0} {1}".format(prob, name)
        chance = " ({0}x)".format(self._chance) if self._chance > 1 else ""
        return "{0}{1}: {2}% ({3}) => {4} T".format(self._name, chance, (1.0 - self._flip) * 100.0, features, self._label)

    def _get_random(self, threshold):
        return random.random() < threshold

    def produce_row(self, all_features):
        label = self._get_random(self._label)
        features = [
            self._get_random(self._features[f] if f in self._features else False) != self._get_random(self._flip)
            for f in all_features
        ]
        return features, label

    def get_chance(self):
        return self._chance

    def get_name(self):
        return self._name

class Rules(object):
    def __init__(self, train):
        self._rules = []
        self._all_features = set()
        self._feature_order = None
        self._model = None
        self._train = train

    def _train_model(self):
        if self.is_trained():
            raise ValueError("already trained!")
        model = RandomForestClassifier(n_jobs=-1, random_state=0)
        _, X, y = self.produce_matrix(self._train)
        print("training model with {0} rows".format(self._train), file=sys.stdout)
        model.fit(X, y)
        print("done", file=sys.stdout)
        self._model = model

    def finish_build(self):
        if self.is_finished():
            raise ValueError("already finished building!")
        self._feature_order = list(self._all_features)
        self._train_model()

    def is_trained(self):
        return self._model is not None

    def is_finished(self):
        return self._feature_order is not None

    def add_rule(self, rule_obj):
        if self.is_finished():
            raise ValueError("already finished building!")
        rule = Rule(rule_obj, self._all_features)
        print(str(rule), file=sys.stdout)
        for _ in xrange(rule.get_chance()):
            self._rules.append(rule)

    def add_all_rules(self, rule_list):
        for r in rule_list:
            self.add_rule(r)

    def produce_row(self):
        if not self.is_finished():
            raise ValueError("not yet finished building!")
        rule = random.choice(self._rules)
        fs, l = rule.produce_row(self._feature_order)
        return rule.get_name(), fs, l

    def produce_pred_rows(self, count):
        if not self.is_trained():
            raise ValueError("not yet trained!")
        names, X, y = self.produce_matrix(count)
        p = self._model.predict_proba(X)
        p = p[:, list(self._model.classes_).index(True)]
        return names, X, y, p

    def get_features(self):
        return self._feature_order[:]

    def produce_matrix(self, count):
        X = np.zeros((count, len(self._feature_order)))
        y = np.zeros(count)
        names = []
        for ix in xrange(count):
            name, fs, l = self.produce_row()
            names.append(name)
            X[ix, :] = fs
            y[ix] = l
        return names, X, y

def usage():
    print("""
usage: {0} <input> <output>
<input>: the input rule JSON file
<output>: the output csv file
""".strip().format(sys.argv[0]), file=sys.stderr)
    exit(1)

if __name__ == '__main__':
    argv = sys.argv[:]
    if len(argv) != 3:
        usage()
    input = argv[1]
    output = argv[2]
    with open(input, 'r') as f_in:
        in_obj = json.load(f_in)
    train = int(in_obj["train"])
    test = int(in_obj["test"])
    rule_list = in_obj["rules"]
    rules = Rules(train)
    rules.add_all_rules(rule_list)
    rules.finish_build()
    names, X, y, p = rules.produce_pred_rows(test)
    feature_names = rules.get_features()
    feature_map = {}
    for (ix, f) in enumerate(feature_names):
        feature_map[f] = ix
    feature_names = sorted(feature_names)
    feature_names.append("label")
    feature_names.append("pred")
    feature_names.append("meta")

    def convert_output(out):
        return "1" if out else "0"

    def process_field(row_ix, k):
        if k == "label":
            return convert_output(y[row_ix])
        if k == "pred":
            return p[row_ix]
        if k == "meta":
            return names[row_ix]
        return convert_output(X[row_ix, feature_map[k]])

    print("writing {0} rows".format(len(names)), file=sys.stdout)
    with open(output, 'w') as f_out:
        out = csv.writer(f_out)
        out.writerow(feature_names)
        for row_ix in sorted(range(len(names)), key=lambda row_ix: -p[row_ix]):
            out.writerow([ process_field(row_ix, k) for k in feature_names ])
    print("done", file=sys.stdout)
    print("total true: {0}".format(np.sum(y)), file=sys.stdout)
    print("total features: {0}".format(len(feature_names) - 3), file=sys.stdout)
    print("AUC: {0}".format(roc_auc_score(y, p)), file=sys.stdout)
