# -*- coding: utf-8 -*-
"""
Provides a rule extractor that has

* guaranteed pure leafs
* no ambiguity
* not generalizable
* bias towards given value

Author: Josua Krause
Date: 2016-09-19
"""
from __future__ import print_function
from __future__ import division

import sys
import numpy as np

from progress_bar import progress_list

class RuleExtractor(object):
    def __init__(self, max_length, by):
        self._max_length = max_length
        self._by = by
        self._rules = None
        self._rule_labels = None
        self._stats = None

    def _get_ixs(self, base_ixs, x, fixs, by):
        # base_ixs must cover all rows that might match the rule
        ixs = np.array(base_ixs)
        prev_ixs = ixs
        for fix in fixs:
            prev_ixs = ixs
            ixs = ixs[np.nonzero(x[ixs, fix] == by)]
        return ixs, prev_ixs

    def _get_label(self, y, ixs):
        # return label, is_pure
        first = True
        label = None
        for ix in ixs:
            if first:
                label = y[ix]
                first = False
            else:
                if y[ix] != label:
                    return None, False
        if first:
            raise ValueError("empty rule")
        return label, True

    def _add_rule_if_ready(self, base_ixs, x, y, fixs, by, rule_res, covered_ixs):
        # return prune
        ixs, prev_ixs = self._get_ixs(base_ixs, x, fixs, by)
        if ixs.shape[0] == 0: # no rows match
            self._stats[0] += 1
            return True
        # if ixs.shape[0] == prev_ixs.shape[0]: # the new feature didn't improve the rule
        #     return True
        label, is_pure = self._get_label(y, ixs)
        if not is_pure: # the rule is not pure -- we need to improve
            self._stats[1] += 1
            return False
        ixs = set(ixs)
        covered_ixs.update(ixs)
        rule_res.append((fixs, label, ixs))
        return True

    def _rule_loop(self, base_ixs, x, y, by):
        end = [ False ]
        rule_candidates = [ [ fix ] for fix in xrange(x.shape[1]) ]
        next_candidates = [[]]
        rules = []
        covered_ixs = set()
        while len(rule_candidates) and not end[0]:
            print("inspecting {0} rules".format(len(rule_candidates)))
            print("rule length: {0} total rules: {1}".format(len(rule_candidates[0]), len(rules)))
            print("coverage so far: {0:5.2f}% ({1} / {2})".format(float(len(covered_ixs)) * 100.00 / float(len(base_ixs)), len(covered_ixs), len(base_ixs)))

            def inspect_rule(fixs):
                if end[0]:
                    return
                prune = self._add_rule_if_ready(base_ixs, x, y, fixs, by, rules, covered_ixs)
                if len(covered_ixs) == len(base_ixs):
                    end[0] = True
                    return
                if not prune:
                    for nix in xrange(fixs[-1] + 1, x.shape[1]):
                        nixs = fixs[:]
                        nixs.append(nix)
                        next_candidates[0].append(nixs)

            progress_list(rule_candidates, inspect_rule, out=sys.stderr, prefix="inspect rules")
            rule_candidates = next_candidates[0]
            next_candidates[0] = []

        print("rules left: {0} total rules: {1}".format(len(rule_candidates), len(rules)))
        print("coverage so far: {0:5.2f}% ({1} / {2})".format(float(len(covered_ixs)) * 100.00 / float(len(base_ixs)), len(covered_ixs), len(base_ixs)))
        return rules

    def _prune_rules(self, base_ixs, x, y, by, rules):
        # rules: [ fixs, label, ixs ]
        new_rules = []
        covered_ixs = set()

        def readd_rule(r):
            ixs = r[2]
            if not ixs.difference(covered_ixs):
                self._stats[2] += 1
                return
            covered_ixs.update(ixs)
            new_rules.append(r)

        progress_list(sorted(rules, key=lambda r: len(r[2]), reverse=True), readd_rule, out=sys.stderr, prefix="prune rules")
        print("rules before: {0} rules after: {1}".format(len(rules), len(new_rules)))
        print("coverage: {0:5.2f}% ({1} / {2})".format(float(len(covered_ixs)) * 100.00 / float(len(base_ixs)), len(covered_ixs), len(base_ixs)))
        return new_rules

    def _get_fallback_rule(self, x, y, by):
        base_ixs = []
        fb_ixs = []
        fb_label = None
        for ix in xrange(x.shape[0]):
            if np.count_nonzero(x[ix, :] == by) == 0:
                if len(fb_ixs) == 0:
                    fb_label = y[ix]
                elif fb_label != y[ix]:
                    raise ValueError("inconsistent labels")
                fb_ixs.append(ix)
            else:
                base_ixs.append(ix)
        return base_ixs, fb_ixs, fb_label

    def fit(self, x, y):
        self._stats = [0, 0, 0]
        x = np.asarray(x)
        by = self._by
        base_ixs, fb_ixs, fb_label = self._get_fallback_rule(x, y, by)
        rules = self._rule_loop(base_ixs, x, y, by)
        rules = self._prune_rules(base_ixs, x, y, by, rules)
        rules.append(([], fb_label, fb_ixs))
        # rules: fixs, label, ixs
        self._rules = [ tuple([ (fix, True) for fix in r[0] ]) if len(r[0]) else tuple([ (fix, False) for fix in xrange(x.shape[1]) ]) for r in rules ]
        self._rule_labels = [ r[1] for r in rules ]
        print("no match rules: {0} non pure rules: {1} already covered rules: {2}".format(self._stats[0], self._stats[1], self._stats[2]))

    def get_rules(self):
        if self._rules is None:
            raise ValueError("need to train first")
        return [ set(r) for r in self._rules ]

    def get_rule_labels(self):
        if self._rules is None:
            raise ValueError("need to train first")
        return self._rule_labels[:]

    def _check_rule(self, x, ix, rule):
        by = self._by
        for (fix, val) in rule:
            if (x[ix, fix] != by) != val:
                return 0.0
        return 1.0

    def predict_rules(self, x):
        if self._rules is None:
            raise ValueError("need to train first")
        rows = x.shape[0]
        res = np.zeros((rows, len(self._rules)))
        for ix in xrange(rows):
            for (rix, rule) in enumerate(self._rules):
                res[ix, rix] = self._check_rule(x, ix, rule)
        return res

    def get_rule_fidelity(self):
        if self._rules is None:
            raise ValueError("need to train first")
        return [ 1.0 for _ in self._rules ]
