# -*- coding: utf-8 -*-
"""
Provides a rule extractor via decision tree.

Author: Josua Krause
Date: 2016-09-20
"""
from __future__ import print_function
from __future__ import division

import sys
import numpy as np

from sklearn.tree import DecisionTreeClassifier

from progress_bar import progress_list, histogram

class DTRuleExtractor(object):
    def __init__(self):
        self._rules = None
        self._rule_labels = None

    def _get_ixs(self, x, tups):
        ixs = np.arange(x.shape[0])
        for (fix, value) in tups:
            ixs = ixs[np.nonzero(x[ixs, fix] if value else x[ixs, fix] == 0)]
        return ixs

    def _get_label(self, y, ixs):
        # return label, is_pure
        first = True
        label = None
        for ix in ixs:
            if first:
                label = y[ix]
                first = False
            else:
                if y[ix] != label:
                    return None, False
        if first:
            raise ValueError("empty rule")
        return label, True

    def _get_removed_rules(self, tups, min_fix):
        return [ (tuple(tups[:ix] + tups[ix + 1:]), tups[ix][0]) for ix in xrange(len(tups)) if tups[ix][0] > min_fix ] #and not tups[ix][1] ] # only remove negative features

    MAX_RULE_CANDIDATES = None # set to None for all rules
    def _simplify_rules(self, x, y, rules):
        # rules: [tups, label, ixs]
        result = []

        def work_rule(tups, old_label, old_ixs, min_fix):
            candidates = []
            for (sr, fix) in self._get_removed_rules(tups, min_fix):
                ixs = self._get_ixs(x, sr)
                if ixs.shape[0] < len(old_ixs):
                    continue
                label, is_pure = self._get_label(y, ixs)
                if not is_pure:
                    continue
                if label != old_label:
                    continue
                candidates.append((sr, old_label, set(ixs), fix))
            if len(candidates):
                candidates = self._prune_rules(x, candidates, do_print=False)
                for c in candidates[:DTRuleExtractor.MAX_RULE_CANDIDATES]:
                    r = (c[0], c[1], c[2])
                    simplify_rule(r, max(min_fix, c[3]))
                return False
            return True

        def simplify_rule(rule, min_fix=-1):
            if work_rule(rule[0], rule[1], rule[2], min_fix):
                result.append(rule)

        progress_list(rules, simplify_rule, out=sys.stderr, prefix="simplify rules")
        return result

    def _get_rules(self, x, y, paths, preds):
        # paths: [((fix, value), ...)]
        # preds: [label]
        rules = []
        for (ix, path) in enumerate(paths):
            ixs = self._get_ixs(x, path)
            if ixs.shape[0] == 0:
                continue
            label, is_pure = self._get_label(y, ixs)
            if not is_pure:
                raise ValueError("path is not pure! {0} {1}".format(path, preds[ix]))
            if label != preds[ix]:
                raise ValueError("label mismatch! {0} {1} != {2}".format(path, preds[ix], label))
            rules.append((path, label, set(ixs)))
        return rules

    def _prune_rules(self, x, rules, do_print=True):
        # rules: [ tups, label, ixs ]
        new_rules = []
        covered_ixs = set()

        def readd_rule(r):
            ixs = r[2]
            if not ixs.difference(covered_ixs):
                return
            covered_ixs.update(ixs)
            new_rules.append(r)

        traverse_list = sorted(rules, key=lambda r: len(r[2]), reverse=True)
        if do_print:
            progress_list(traverse_list, readd_rule, out=sys.stderr, prefix="prune rules")
            print("rules before: {0} rules after: {1}".format(len(rules), len(new_rules)))
            print("coverage: {0:5.2f}% ({1} / {2})".format(float(len(covered_ixs)) * 100.00 / float(x.shape[0]), len(covered_ixs), x.shape[0]))
        else:
            for tl in traverse_list:
                readd_rule(tl)
        return new_rules

    # sklearn==0.17.1 ea042f1485d5fe45bcf2475c3070cab4e5ac3381
    _TREE_LEAF = -1 # sklearn _tree.pyx:61
    _TREE_UNDEFINED = -2 # sklearn _tree.pyx:62
    def _extract_tree_paths(self, tree, classes, paths, preds, cur_path=[], tix=0):
        # sklearn _tree.pyx:780..790
        feature = tree.feature[tix]
        if tree.children_left[tix] == DTRuleExtractor._TREE_LEAF:
            # and tree.children_right[tix] == DTRuleExtractor._TREE_LEAF
            pred = classes[np.argmax(tree.value[tix])]
            paths.append(self._canonic_path(cur_path))
            preds.append(pred)
        elif feature == DTRuleExtractor._TREE_UNDEFINED:
            raise ValueError("invalid path {0}".format(cur_path))
        else:
            if tree.threshold[tix] < 0 or tree.threshold[tix] >= 1:
                raise ValueError("weird decision tree: threshold is {0}".format(tree.threshold[tix]))
            new_path = cur_path[:]
            new_path.append((feature, False)) # left path
            self._extract_tree_paths(tree, classes, paths, preds, new_path, tree.children_left[tix])
            new_path = cur_path[:]
            new_path.append((feature, True)) # right path
            self._extract_tree_paths(tree, classes, paths, preds, new_path, tree.children_right[tix])

    def _canonic_path(self, path):

        def cc(seg_a, seg_b):
            f_a, ia_a = seg_a
            f_b, ia_b = seg_b
            ia_c = cmp(not ia_a, not ia_b)
            if ia_c != 0:
                return ia_c
            return cmp(f_a, f_b)

        return tuple(sorted(path, cmp=cc))

    def fit(self, x, y):
        x = np.asarray(x)
        clf = DecisionTreeClassifier(criterion='entropy', random_state=0, max_features=None, max_leaf_nodes=None)
        clf.fit(x, y)
        paths = []
        preds = []
        self._extract_tree_paths(clf.tree_, clf.classes_, paths, preds)
        rules = self._prune_rules(x, self._get_rules(x, y, paths, preds))
        rules = self._simplify_rules(x, y, rules)
        rules = self._prune_rules(x, rules)
        # rules: tups, label, ixs
        self._rules = [ r[0] for r in rules ]
        self._rule_labels = [ r[1] for r in rules ]
        print("statistics:")
        bin_counts = {}
        for ix in xrange(x.shape[0]):
            s = 0
            for r in self._rules:
                if self._check_rule(x, ix, r):
                    s += 1
            nn = "in {0} rule{1}".format(s, " " if s == 1 else "s")
            if nn not in bin_counts:
                bin_counts[nn] = 0
            bin_counts[nn] += 1
        histogram(sorted(bin_counts.items(), key=lambda x: x[0]))

    def get_rules(self):
        if self._rules is None:
            raise ValueError("need to train first")
        return [ set(r) for r in self._rules ]

    def get_rule_labels(self):
        if self._rules is None:
            raise ValueError("need to train first")
        return self._rule_labels[:]

    def _check_rule(self, x, ix, rule):
        for (fix, has) in rule:
            if bool(x[ix, fix]) != has:
                return 0.0
        return 1.0

    def predict_rules(self, x):
        if self._rules is None:
            raise ValueError("need to train first")
        rows = x.shape[0]
        res = np.zeros((rows, len(self._rules)))
        for ix in xrange(rows):
            for (rix, rule) in enumerate(self._rules):
                res[ix, rix] = self._check_rule(x, ix, rule)
        return res

    def get_rule_fidelity(self):
        if self._rules is None:
            raise ValueError("need to train first")
        return [ 1.0 for _ in self._rules ]
