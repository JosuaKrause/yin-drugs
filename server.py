#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import sys
# fix for old server
__cur_folder = sys.path[0]
if '/usr/local/lib64/python2.6/site-packages' in sys.path:
    sys.path.remove('/usr/local/lib64/python2.6/site-packages')
sys.path.sort(key=lambda p: 0 if ("site-packages" in p and "Frameworks" not in p) or p == __cur_folder else 1)

import os
import csv
import math
import argparse
import threading

def show_version():
    print("python is {0}".format(sys.version), file=sys.stdout)
    import numpy as np
    print("numpy is {0}".format(np.__version__), file=sys.stdout)
    import sklearn
    print("sklearn is {0}".format(sklearn.__version__), file=sys.stdout)
    import scipy
    print("scipy is {0}".format(scipy.__version__), file=sys.stdout)

try:
    from cs_db import CSDB, DISTANCE_DIFF, DISTANCE_DEFAULT

    from quick_server import create_server, msg, setup_restart
    from quick_cache import QuickCache

    import slicer
except ImportError:
    show_version()
    raise

def get_server(addr, port, csdb, slicer_obj, cache):
    server = create_server((addr, port))

    if heroku:
        server.bind_path('/', '.')
        prefix = '/'
    else:
        server.bind_path('/', '..')
        prefix = '/' + os.path.basename(os.path.normpath(server.base_path))

    server.directory_listing = True
    server.add_default_white_list()
    server.link_empty_favicon_fallback()

    server.suppress_noise = True
    server.report_slow_requests = True

    server.link_worker_js(prefix + '/js/worker.js')
    server.cache = cache

    def canonic_ixss(ixss):
        return [ canonic_ixs(ixs) for ixs in ixss ]

    def canonic_ixs(ixs):
        return sorted(frozenset(convert_ixs(ixs)))

    def convert_ixs(ixs):
        if ixs is None:
            return []
        return [ int(ix) for ix in ixs ]

    def get_thresholds(args):
        return (float(args["l"]), float(args["r"]))

    def optional(key, args, default=None):
        return args[key] if key in args else default

    def optional_float(key, args, default=None):
        res = args[key] if key in args else default
        if res is None:
            return default
        return float(res)

    def optional_int(key, args, default=None):
        res = args[key] if key in args else default
        if res is None:
            return default
        return int(res)

    def optional_bool(key, args, default):
        res = args[key] if key in args else default
        if res is None:
            return default
        return bool(res)

    ### CSDB ###

    if csdb is not None:
        @server.json_worker(prefix + '/raw_clusters', cache_id=lambda args: {
                "function": "json_raw_clusters",
                "thresholds": list(get_thresholds(args)),
                "d": optional("d", args),
                "c": optional("c", args),
                "ns": convert_ixs(optional("ns", args)),
                "used": optional("used", args),
                "weights": optional("weights", args),
            } if "clusters" not in args else None)
        def json_raw_clusters(args):
            thresholds = get_thresholds(args)
            d = optional("d", args)
            c = optional("c", args)

            if "clusters" in args:
                cluster_names = args["cluster_names"]
                cluster_ixs = canonic_ixss(args["clusters"])
                cluster_task = args["task"]
                cluster_split = optional("split", args)
                used_features = optional("used", args)
                feature_weights = optional("weights", args)
                clusters, cluster_order, weights = csdb.manipulate_clusters(cluster_names, cluster_ixs, cluster_task, cluster_split, d, c, used_features, feature_weights)
            else:
                ns = convert_ixs(args["ns"])
                clusters, cluster_order, weights = csdb.get_raw_clusters(thresholds, ns, d, c)
            return csdb.enrich_clusters(clusters, cluster_order, thresholds, c, weights)

        @server.json_worker(prefix + '/info', cache_id=lambda args: {
                "function": "json_info",
                "thresholds": list(get_thresholds(args)),
                "ixs": canonic_ixs(args["ixs"]),
                "fs": canonic_ixs(args["fs"]),
                "vs": args["vs"],
            })
        def json_info(args):
            thresholds = get_thresholds(args)
            ixs = canonic_ixs(args["ixs"])
            fs = canonic_ixs(args["fs"])
            vs = args["vs"]
            return csdb.get_info(ixs, fs, vs, thresholds)

        @server.json_worker(prefix + '/weights', cache_id=lambda args: {
                "function": "json_weights",
                "clusters": canonic_ixss(args["clusters"]),
            })
        def json_weights(args):
            clusters = canonic_ixss(args["clusters"])
            return csdb.get_weights(clusters)

        @server.json_worker(prefix + '/project', cache_id=lambda args: {
                "function": "json_project",
                "thresholds": list(get_thresholds(args)),
                "d": args["d"],
                "p": args["p"],
            })
        def json_project(args):
            thresholds = get_thresholds(args)
            d = args["d"]
            p = args["p"]
            return csdb.project_points(p, d, thresholds)

        @server.json_worker(prefix + '/scatter', cache_id=lambda args: {
                "function": "json_scatter",
                "thresholds": list(get_thresholds(args)),
                "p": args["p"],
                "is_rule": optional_bool("is_rule", args, False),
            })
        def json_scatter(args):
            thresholds = get_thresholds(args)
            p = args["p"]
            is_rule = optional_bool("is_rule", args, False)
            return csdb.get_cluster_scatterplot(p, thresholds, rules=is_rule)

        @server.json_get(prefix + '/roc_curve', 0)
        def get_roc_curve(req, args):
            return csdb.get_roc_curve()

        @server.json_get(prefix + '/params', 0)
        def json_params(req, args):
            return csdb.get_params()

        @server.json_get(prefix + '/neighbor', 0)
        def json_neighbor(req, args):
            q = args["query"]
            from_ix = int(q["ix"])
            max_dist = int(q["max_dist"])
            n_ixs = csdb.get_neighbors(from_ix, DISTANCE_DIFF, max_dist)
            min_ix, max_ix = csdb.get_min_max_pred(n_ixs)
            return {
                "size": len(n_ixs),
                "from": csdb.get_point(from_ix),
                "min_ix": csdb.get_point(min_ix),
                "max_ix": csdb.get_point(max_ix),
                "max_dist": max_dist,
                "chg_min": csdb.get_diff(from_ix, min_ix),
                "chg_max": csdb.get_diff(from_ix, max_ix),
            }

        @server.json_worker(prefix + '/neighbors', cache_id=lambda args: {
                "function": "neighbors",
                "max_dist": int(args["max_dist"]),
            })
        def json_neighbors(args):
            max_dist = int(args["max_dist"])

            def get_node(ix):
                min_ix = ix
                max_ix = ix
                for md in range(csdb.get_feature_count() if max_dist == 0 else max_dist):
                    n_ixs = csdb.get_neighbors(ix, DISTANCE_DIFF, md)
                    mini, maxi = csdb.get_min_max_pred(n_ixs)
                    if min_ix == ix and mini != ix:
                        min_ix = mini
                    if max_ix == ix and maxi != ix:
                        max_ix = maxi
                    if min_ix != ix and max_ix != ix:
                        break
                # if not csdb.get_diff(ix, min_ix).keys():
                #     msg("{0} {1} {2} {3} {4} {5}", ix, min_ix, csdb.get_diff(ix, min_ix), csdb._x[ix] != csdb._x[min_ix], csdb._pred[ix], csdb._pred[min_ix])
                # if not csdb.get_diff(ix, max_ix).keys():
                #     msg("{0} {1} {2} {3} {4} {5}", ix, max_ix, csdb.get_diff(ix, max_ix), csdb._x[ix] != csdb._x[max_ix], csdb._pred[ix], csdb._pred[max_ix])
                return {
                    "size": len(n_ixs),
                    "from": ix,
                    "min_ix": min_ix,
                    "max_ix": max_ix,
                    "chg_min": csdb.get_diff(ix, min_ix),
                    "chg_max": csdb.get_diff(ix, max_ix),
                }

            return {
                "max_dist": max_dist,
                "vecs": [ csdb.get_vec(ix, "1") for ix in csdb.get_all_ixs() ],
                "points": [ csdb.get_point(ix) for ix in csdb.get_all_ixs() ],
                "neighbors": [ get_node(ix) for ix in csdb.get_all_ixs() ],
            }

        @server.json_worker(prefix + '/distances', cache_id=lambda args: {
                "function": "distances",
                "d": optional("d", args, DISTANCE_DIFF),
                "k": optional_int("k", args),
                "maxd": optional_float("maxd", args),
                "eps": optional_float("eps", args),
            })
        def json_distances(args):
            ixs = csdb.get_all_ixs()
            d = optional("d", args, DISTANCE_DIFF)
            k = optional_int("k", args)
            maxd = optional_float("maxd", args)
            eps = optional_float("eps", args)
            if eps is not None:
                cixs, ccixs = csdb.get_close_representatives(d, eps)
            else:
                cixs, ccixs = ixs, [ [ ix ] for ix in ixs ]

            def get_maxd(pos, ix):
                if len(ccixs[pos]) < 2:
                    return maxd
                altd = csdb.max_cluster_distance(d, [ ix ], ccixs[pos])
                if maxd is None or altd > maxd:
                    return altd
                return maxd

            return {
                "maxd": max([ get_maxd(pos, ix) for (pos, ix) in enumerate(cixs) ]),
                "vecs": [ csdb.get_vec(ix, "1") for ix in ixs ],
                "points": [ csdb.get_point(ix) for ix in ixs ],
                "distances": [ {
                    "ix": ix,
                    "d": csdb.get_knn(ix, ixs, d, k, get_maxd(pos, ix)),
                    "cixs": ccixs[pos],
                } for (pos, ix) in enumerate(cixs) ],
            }

        @server.json_post(prefix + '/items')
        def json_items(req, args):
            args = args["post"]
            ixs = canonic_ixs(args["ixs"])
            vec, features = csdb.get_vecs(ixs, "1")
            return {
                "features": features,
                "vec": vec.tolist(),
            }

    ### SLICER ###

    if slicer_obj is not None:
        @server.json_get(prefix + '/slicer_roc_curve', 0)
        def json_slicer_roc_curve(req, args):
            args = args["query"]
            token = args["token"] if "token" in args and args["token"] else None
            pred_ixs = optional_bool("pred_ixs", args, False)
            res = slicer_obj.get_roc_curve()
            res["token"] = server.create_token() if token is None else token
            if pred_ixs:
                res["pred_ixs"] = slicer_obj.get_pred_ixs()
            return res

        def token_obj(args):
            token = args["token"]
            obj = server.get_token_obj(token)
            if "ixss" not in obj:
                obj["ixss"] = [ canonic_ixs(slicer_obj.get_all_ixs()) ]
            return obj

        token_lock = threading.Lock()
        def get_ixs(args):
            with token_lock:
                return token_obj(args)["ixss"][-1]

        @server.json_post(prefix + '/slicer_ixs_get', 0)
        def json_slicer_ixs_get(req, args):
            with token_lock:
                args = args["post"]
                obj = token_obj(args)
                return [ len(ixs) for ixs in obj["ixss"] ]

        @server.json_post(prefix + '/slicer_ixs_put', 0)
        def json_slicer_ixs_put(req, args):
            with token_lock:
                args = args["post"]
                if "ixs" in args:
                    new_ixs = canonic_ixs(args["ixs"])
                else:
                    new_ixs = None
                if "pos" in args:
                    new_pos = int(args["pos"])
                else:
                    new_pos = None
                obj = token_obj(args)
                if new_pos is not None:
                    obj["ixss"] = obj["ixss"][:(new_pos + 1)]
                if new_ixs is not None:
                    ixss = obj["ixss"]
                    found = None
                    if len(new_ixs) == 0:
                        found = len(ixss)
                    else:
                        for (pos, ixs) in enumerate(ixss):
                            if ixs == new_ixs:
                                found = pos + 1
                                break
                    if found is not None:
                        obj["ixss"] = obj["ixss"][:found]
                    else:
                        obj["ixss"].append(new_ixs)
                return [ len(ixs) for ixs in obj["ixss"] ]

        @server.json_post(prefix + '/slicer_page_get', 0)
        def json_slicer_page_get(req, args):
            with token_lock:
                args = args["post"]
                page = args["page"]
                obj = token_obj(args)
                if page not in obj:
                    obj[page] = {}
                return obj[page]

        @server.json_post(prefix + '/slicer_page_put', 0)
        def json_slicer_page_put(req, args):
            with token_lock:
                args = args["post"]
                page = args["page"]
                obj = token_obj(args)
                if page not in obj:
                    obj[page] = {}
                put = args["put"]
                for (k, v) in put.items():
                    obj[page][k] = v
                return obj[page]

        @server.json_post(prefix + '/slicer_page_clear', 0)
        def json_slicer_page_clear(req, args):
            with token_lock:
                args = args["post"]
                page = args["page"]
                obj = token_obj(args)
                obj[page] = {}
                return obj[page]

        @server.json_get(prefix + '/slicer_axes', 0)
        def json_slicer_axes(req, args):
            all_ixs = slicer_obj.get_all_ixs()
            return {
                "ixs": all_ixs,
                "axes": [
                    ("label", "Label"),
                    ("pred", "Prediction Score"),
                    ("pred_raw", "Predicted Label"),
                    ("meta", "Meta Info"),
                    ("expl", "Explanation"),
                    ("vecs", "Vector"),
                    ("expl_dup", "in Explanation (Dupl)"),
                    ("vecs_dup", "in Vector (Dupl)"),
                ],
                "label": slicer_obj.get_label(all_ixs)[0],
                "pred": slicer_obj.get_pred_raw(all_ixs),
            }

        @server.json_post(prefix + '/slicer_axis', 0)
        def json_slicer_axis(req, args):
            args = args["post"]
            ixs = get_ixs(args)
            score = get_thresholds(args)
            axis = args["axis"]
            page = int(args["page"])
            actual_res = None
            pages = None
            if axis == "label":
                res, groups = slicer_obj.get_label(ixs)
                page = 0
            elif axis == "pred":
                res, groups = slicer_obj.get_pred(ixs)
                page = 0
            elif axis == "pred_raw":
                groups = [ "T", "U", "F" ]
                res = slicer_obj.get_pred_raw(ixs)
                res = [ "T" if p >= score[0] else ("F" if p < score[1] else "U") for p in res ]
                page = 0
            elif axis == "meta":
                actual_res, groups, page, pages = slicer_obj.get_meta(ixs, page)
            elif axis == "expl":
                actual_res, groups, page, pages = slicer_obj.get_expl(ixs, score, page)
            elif axis == "vecs":
                actual_res, groups, page, pages = slicer_obj.get_vecs(ixs, page)
            elif axis == "expl_dup":
                actual_res, groups, page, pages = slicer_obj.get_in_expl(ixs, score)
            elif axis == "vecs_dup":
                actual_res, groups, page, pages = slicer_obj.get_in_vecs(ixs, page)
            else:
                raise ValueError("unknown axis {0}".format(axis))
            if actual_res is None:
                actual_res = [ [ e ] for e in res ]
            if pages is None:
                pages = len(groups)
            return {
                "ixs": ixs,
                "res": actual_res,
                "groups": groups,
                "page": page,
                "pages": pages,
            }

        @server.json_post(prefix + '/slicer_desc', 0)
        def json_slicer_desc(req, args):
            args = args["post"]
            ix = int(args["ix"])
            score = get_thresholds(args)
            return slicer_obj.get_desc(ix, score)

        @server.json_post(prefix + '/slicer_descs', 0)
        def json_slicer_descs(req, args):
            args = args["post"]
            ixs = get_ixs(args)
            score = get_thresholds(args)
            return [ slicer_obj.get_desc(ix, score) for ix in ixs ]

        @server.json_post(prefix + '/slicer_expls', 0)
        def json_slicer_expls(req, args):
            args = args["post"]
            ixs = get_ixs(args)
            score = get_thresholds(args)
            compact = optional_bool("compact", args, True)
            return slicer_obj.get_all_expl(ixs, score, compact)

        @server.json_post(prefix + '/slicer_expls_stats', 0)
        def json_slicer_expls_stats(req, args):
            args = args["post"]
            ixs = get_ixs(args)
            score = get_thresholds(args)
            return {
                "expls": slicer_obj.get_expl_stats(ixs, score),
                "totals": slicer_obj.get_stats(ixs, score),
                "all": slicer_obj.get_stats(slicer_obj.get_all_ixs(), score),
            }

        @server.json_post(prefix + '/slicer_counts', 0)
        def json_slicer_counts(req, args):
            args = args["post"]
            score = get_thresholds(args)
            ixs = get_ixs(args)
            expl = args["expl"]
            partial = optional_bool("partial", args, False)
            return slicer_obj.get_for_expl(ixs, score, expl, partial)

        @server.json_post(prefix + '/slicer_granular', 0)
        def json_slicer_granular(req, args):
            args = args["post"]
            score = get_thresholds(args)
            ixs = get_ixs(args)
            expl = args["expl"]
            partial = optional_bool("partial", args, False)
            compact = optional_bool("ignore_irrelevant_features", args, False)
            groups, features, tree, feature_importances \
                = slicer_obj.get_granular_expl(ixs, score, expl, partial, compact)
            return {
                "groups": groups,
                "features": features,
                "tree": tree,
                "importances": feature_importances,
            }

        @server.json_get(prefix + '/slicer_tree_get', 0)
        def json_slicer_granular_get(req, args):
            args["post"] = args["query"]
            args["post"]["expl"] = args["post"]["expl"].split(',')
            return json_slicer_granular_tree(req, args)

        @server.json_post(prefix + '/slicer_tree', 0)
        def json_slicer_granular_tree(req, args):
            args = args["post"]
            score = get_thresholds(args)
            ixs = get_ixs(args)
            expl = args["expl"]
            partial = optional_bool("partial", args, False)
            return slicer_obj.get_expl_tree(ixs, score, expl, partial)

    ### COMMANDS ###

    def complete_cache_clear(args, text):
        if args:
            return []
        return [ section for section in cache.list_sections() if section.startswith(text) ]

    @server.cmd(complete=complete_cache_clear)
    def cache_clear(args):
        if len(args) > 1:
          msg("too many extra arguments! expected one got {0}", ' '.join(args))
          return
        msg("clear {0}cache{1}{2}", "" if args else "all ", " " if args else "s", args[0] if args else "")
        cache.clean_cache(args[0] if args else None)

    return server

heroku = False
if __name__ == '__main__':
    if '--heroku' not in sys.argv[1:]:
        setup_restart()

    if '-v' in sys.argv[1:]:
        show_version()
        exit(0)
    parser = argparse.ArgumentParser(description='Class Signature Server')
    parser.add_argument('-v', action='store_true', help="print library versions and exit")
    parser.add_argument('--metafeatures', action='store_true', help="interprets meta info as features")
    parser.add_argument('--sample', type=float, default=1.0, help="samples data points for slicer")
    parser.add_argument('--heroku', action='store_true', help="start in heroku mode")
    parser.add_argument('--quota', default=10240, help="set cache quota")
    parser.add_argument('--ram-quota', default=2048, help="set RAM cache quota")
    parser.add_argument('--weights', default=None, help="feature weight file")
    parser.add_argument('--slicer', default=None, help="file with slicer info")
    parser.add_argument('-a', type=str, default="localhost", help="specifies the server address")
    parser.add_argument('-p', type=int, default=8080, help="specifies the server port")
    parser.add_argument('input', type=str, help="file containing a CSV table with categorical values")
    args = parser.parse_args()

    addr = args.a
    port = args.p
    heroku = args.heroku
    cache_quota = args.quota
    ram_quota = args.ram_quota
    slicer_file = args.slicer
    sample = args.sample

    weights = None
    if args.weights is not None:
        weights = {}
        with open(args.weights, 'r') as f:
            for row in csv.reader(f):
                weights[row[0]] = float(row[1])

    cache_temp = "tmp"
    if os.path.exists("cache_path.txt"):
        with open("cache_path.txt") as cp:
            cache_temp = cp.read().strip()

    if slicer_file is not None:
        cid = args.input + ":" + slicer_file + ":" + str(args.metafeatures)
        cache = QuickCache(None, cid, quota=cache_quota, ram_quota=ram_quota, temp=cache_temp, warnings=msg)
    else:
        cache = QuickCache(args.input, quota=cache_quota, ram_quota=ram_quota, temp=cache_temp, warnings=msg)

    if heroku:
        QuickServerRequestHandler.protocol_version = "HTTP/1.0"
    if slicer_file is not None:
        csdb = None
        slicer_obj = slicer.Slicer(slicer_file, args.input, args.metafeatures, sample, cache, msg)
    else:
        csdb = CSDB(args.input, weights, cache, msg)
        slicer_obj = None
    server = get_server(addr, port, csdb, slicer_obj, cache)
    msg("{0}", " ".join(sys.argv))
    msg("starting server at {0}:{1}", addr if addr else 'localhost', port)
    server.serve_forever()
    msg("shutting down..")
    msg("{0}", " ".join(sys.argv))
    server.server_close()
