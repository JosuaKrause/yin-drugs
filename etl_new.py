#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import os
import csv
import sys

def assign_label(p_cur, label_str):
    label = label_str == "1"
    if not label and label_str != "0":
        print("invalid label '{0}'".format(label_str), file=sys.stderr)
    if "label" in p_cur:
        if p_cur["label"] != label:
            print("inconsistent label for patient! {0} {1}".format(p_cur["label"], label_str), file=sys.stderr)
    else:
        p_cur["label"] = label

def assign_pred(p_cur, pred_str):
    p = float(pred_str)
    if "pred" in p_cur:
        if p != p_cur["pred"]:
            print("inconsistent prediction for patient! {0} {1}".format(p_cur["pred"], p), file=sys.stderr)
    else:
        p_cur["pred"] = p

def assign_meta(p_cur, meta_str):
    if "meta" in p_cur:
        if p_cur["meta"] != meta_str:
            print("inconsistent meta for patient! {0} {1}".format(p_cur["meta"], meta_str), file=sys.stderr)
    else:
        p_cur["meta"] = meta_str

total_p = 0
def flush_patient(p_cur, patients):
    global total_p
    if p_cur is not None:
        if "label" not in p_cur:
            print("missing label for patient!", file=sys.stderr)
        if "pred" not in p_cur:
            print("missing pred for patient!", file=sys.stderr)
        if "meta" not in p_cur or not p_cur["meta"]:
            p_cur["meta"] = "(no diagnosis)"
        patients.append(p_cur)
        total_p += 1
        if total_p % 1000 == 0:
            print("processed {0} patients".format(total_p), file=sys.stdout)
    return {
        "drugs": set()
    }

def add_drug(p_cur, drugs, med, line):
    if med:
        p_cur["drugs"].add(med)
        drugs.add(med)
    else:
        print("WARNING: no drug ({0}): '{1}'".format(line, drug), file=sys.stdout)

def create_medi(cui, med):
    return "{0} ({1})".format(med, cui)

def convert_output(out):
    return "1" if out else "0"

def usage():
    print("""
usage: {0} <input> <output>
<input>: the input csv file
<output>: the output csv file
""".strip().format(sys.argv[0]), file=sys.stderr)
    exit(1)

NO_ID = True
if __name__ == '__main__':
    argv = sys.argv[:]
    if len(argv) != 3:
        usage()
    input = argv[1]
    output = argv[2]
    patients = []
    patient_cur = None
    drugs = set()
    cur_patient = None
    all_patients = set()
    with open(input, 'rU') as f_in:
        line = 1
        for row in csv.DictReader(f_in):
            if patient_cur is None or cur_patient != row["ID"].strip():
                patient_cur = flush_patient(patient_cur, patients)
                cur_patient = row["ID"].strip()
                if cur_patient in all_patients:
                    print("WARNING! duplicate patient {0}".format(cur_patient))
                all_patients.add(cur_patient)
            assign_label(patient_cur, row["True Label"].strip())
            assign_pred(patient_cur, row["Predicted Score"].strip())
            meta = "{0}{1}".format(cur_patient + " " if not NO_ID else "", row["Diag Descreption"].strip())
            if row["Diag Code"].strip():
                meta += " ({0})".format(row["Diag Code"].strip())
            assign_meta(patient_cur, meta)
            medi = create_medi(row["CUI"].strip(), row["Medication"].strip())
            add_drug(patient_cur, drugs, medi, line)
            line += 1
        patient_cur = flush_patient(patient_cur, patients)
    feature_names = sorted(list(drugs))
    feature_names.append("label")
    feature_names.append("pred")
    feature_names.append("meta")
    truths = len([ 1 for p in patients if p["label"] ])
    total = len(patients)
    threshold = truths / total
    if total_p != total:
        raise ValueError("{0} != {1}".format(total_p, total))

    def get_stats(patients, threshold):
        tp = 0
        tn = 0
        fp = 0
        fn = 0
        for p in patients:
            if p["pred"] > threshold:
                if p["label"]:
                    tp += 1
                else:
                    fp += 1
            else:
                if p["label"]:
                    fn += 1
                else:
                    tn += 1
        return tp, tn, fp, fn

    print("total patients: {0}".format(total), file=sys.stdout)
    print("total true: {0}".format(truths), file=sys.stdout)
    print("total features: {0}".format(len(feature_names)), file=sys.stdout)
    tp, tn, fp, fn = get_stats(patients, threshold)
    print("precision: {0}".format(tp / (tp + fp)), file=sys.stdout)
    print("recall: {0}".format(tp / (tp + fn)), file=sys.stdout)
    print("accuracy: {0}".format((tp + tn) / (tp + tn + fp + fn)), file=sys.stdout)
    print("  | p     n", file=sys.stdout)
    print("--+------------", file=sys.stdout)
    print("t | {0:.2f}  {1:.2f}".format(tp / total, tn / total), file=sys.stdout)
    print("f | {0:.2f}  {1:.2f}".format(fp / total, fn / total), file=sys.stdout)

    def process_field(p, k):
        if k == "label":
            return convert_output(p["label"])
        if k == "pred":
            return p["pred"]
        if k == "meta":
            return p["meta"]
        return convert_output(k in p["drugs"])

    with open(output, 'w') as f_out:
        out = csv.writer(f_out)
        out.writerow(feature_names)
        for p in patients:
            out.writerow([ process_field(p, k) for k in feature_names ])
