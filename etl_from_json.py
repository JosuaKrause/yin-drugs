#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import os
import csv
import sys
import json
import math

import numpy as np
from sklearn.metrics import roc_auc_score
from sklearn.ensemble import RandomForestClassifier

def usage():
    print("""
usage: {0} <train> <input> <output> [<weights>] [<reviews>]
<train>: training ratio (0..1)
<input>: the input json line file
<output>: the output csv file
<weights>: optional weight file
<reviews>: optional review folder
""".strip().format(sys.argv[0]), file=sys.stderr)
    exit(1)

min_count = 5
subj = frozenset(["Noun", "Adjective", "Verb"])
if __name__ == '__main__':
    argv = sys.argv[:]
    if len(argv) != 4 and len(argv) != 5 and len(argv) != 6:
        usage()
    train = float(argv[1])
    input = argv[2]
    output = argv[3]
    weight_file = argv[4] if len(argv) > 4 else None
    reviews = argv[5] if len(argv) > 5 else None
    patients = []
    drugs = set()
    with open(input, 'rb') as f_in:
        for line in f_in.readlines():
            cur = json.loads(line, encoding='utf-8')
            rating = float(cur["review"]["rating"])
            if rating < 4 and rating > 2:
                continue
            cur_drugs = set()
            for term in cur["$nlp"]["review.text"]["tokens"]:
                if not any(s in term for s in subj):
                    continue
                word = term["Lemma"]
                if word == "label" or word == "meta" or word == "meds":
                    word = "_" + word
                cur_drugs.add(word)
                drugs.add(word)
            cat = " ".join(sorted(cur["business"]["category"]))
            # cur["business"]["rating"]
            p = {
                "label": rating > 3,
                "meta": "{0}".format(cat),
                "meds": cur_drugs,
                "text": cur["review"]["text"],
            }
            patients.append(p)
    drug_list = sorted(list(drugs))

    feature_map = {}
    for (ix, f) in enumerate(drug_list):
        feature_map[f] = ix

    train_num = int(math.floor(len(patients) * train))

    def get_matrix(ps):
        y = np.array([ p["label"] for p in ps ])
        X = np.zeros((len(ps), len(drug_list)))
        for (ix, p) in enumerate(ps):
            X[ix, :] = [ 1 if d in p["meds"] else 0 for d in drug_list ]
        return X, y

    model = None
    if train_num > 0:
        print("train {0} rows".format(train_num), file=sys.stdout)
        train_ps = patients[:train_num]
        X, y = get_matrix(train_ps)
        model = RandomForestClassifier(n_jobs=-1, random_state=0)
        model.fit(X, y)
        print("training done", file=sys.stdout)

    test_ps = patients[train_num:]
    X, y = get_matrix(test_ps)

    feature_list = [ k for k in drug_list if np.sum(X[:, feature_map[k]]) > min_count ]

    pred = None
    if model is not None:
        pred = model.predict_proba(X)
        pred = pred[:, list(model.classes_).index(True)]
        print("prediction done", file=sys.stdout)
        if weight_file is not None:
            with open(weight_file, 'w') as wout:
                out = csv.writer(wout)
                for (ix, d) in enumerate(drug_list):
                    out.writerow([ d, model.feature_importances_[ix] ])

    feature_list.append("label")
    feature_list.append("meta")
    if pred is not None:
        feature_list.append("pred")

    def get_review_text_file(base, ix):
        name = str(ix).rjust(6, '0') + '.txt'
        fn = os.path.join(base, name[0:2], name[2:4])
        if not os.path.exists(fn):
            os.makedirs(fn)
        return os.path.join(fn, name[4:])

    def convert_output(out):
        return "1" if out else "0"

    def process_field(row_ix, k):
        if k == "label":
            return convert_output(y[row_ix])
        if k == "pred":
            return pred[row_ix]
        if k == "meta":
            return test_ps[row_ix]["meta"]
        return convert_output(X[row_ix, feature_map[k]])

    print("total true: {0}".format(np.sum(y)), file=sys.stdout)
    print("total features: {0}".format(len(feature_list) - 3), file=sys.stdout)
    if pred is not None:
        print("AUC: {0}".format(roc_auc_score(y, pred)), file=sys.stdout)
    print("writing {0} rows".format(len(test_ps)), file=sys.stdout)
    with open(output, 'w') as f_out:
        out = csv.writer(f_out)
        out.writerow(feature_list)
        row_ixs = range(len(test_ps))
        if pred is not None:
            row_ixs.sort(key=lambda row_ix: -pred[row_ix])
        for row_ix in row_ixs:
            try:
                if reviews:
                    with open(get_review_text_file(reviews, row_ix), "wb") as r_out:
                        r_out.write(test_ps[row_ix]["text"].encode('utf-8'))
                        if not test_ps[row_ix]["text"].endswith("\n"):
                            r_out.write("\n")
                out.writerow([ process_field(row_ix, k) for k in feature_list ])
            except:
                print("error in", row_ix)
                raise
    print("done", file=sys.stdout)
