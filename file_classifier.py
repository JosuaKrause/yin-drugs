# -*- coding: utf-8 -*-
"""
Provides a rule extractor from reading a file.

Author: Josua Krause
Date: 2016-09-20
"""
from __future__ import print_function
from __future__ import division

import sys
import json
import numpy as np

from sklearn.tree import DecisionTreeClassifier

from progress_bar import progress_list, histogram

class FileRuleExtractor(object):
    def __init__(self, name_lookup):
        self._rules = None
        self._name_lookup = name_lookup

    def fit(self, x, y):
        with open('explanation_drugs.json', 'rb') as f:
            self._rules = json.load(f)["list_of_exp"]

    def get_rules(self):
        if self._rules is None:
            raise ValueError("need to train first")
        return [ set([ (self._name_lookup[e], True) for e in r["exp"] ]) for r in self._rules ]

    def get_rule_labels(self):
        if self._rules is None:
            raise ValueError("need to train first")
        return [ "pos" if sum([ float(t) for t in r["ground_truth"] ]) / float(len(r["ground_truth"])) > 0.5 else "neg" for r in self._rules ]

    def _check_rule(self, x, ix, rule):
        for r in rule["exp"]:
            fix = self._name_lookup[r]
            if not bool(x[ix, fix]):
                return 0.0
        return 1.0

    def predict_rules(self, x):
        if self._rules is None:
            raise ValueError("need to train first")
        rows = x.shape[0]
        res = np.zeros((rows, len(self._rules)))
        for ix in xrange(rows):
            for (rix, rule) in enumerate(self._rules):
                res[ix, rix] = self._check_rule(x, ix, rule)
        return res

    def get_rule_fidelity(self):
        if self._rules is None:
            raise ValueError("need to train first")
        return [ abs(sum([ float(t) for t in r["ground_truth"] ]) / float(len(r["ground_truth"])) - 0.5) for r in self._rules ]
