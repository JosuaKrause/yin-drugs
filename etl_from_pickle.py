#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import sys
# fix for old server
if '/usr/local/lib64/python2.6/site-packages' in sys.path:
    sys.path.remove('/usr/local/lib64/python2.6/site-packages')

import os
import csv
import json
import math
import pickle
import random

import numpy as np
from sklearn.metrics import roc_auc_score

def usage():
    print("""
usage: {0} <input> <output>
<input>: the input folder
<output>: the output csv file
""".strip().format(sys.argv[0]), file=sys.stderr)
    exit(1)

min_count = 5
count = 3000
if __name__ == '__main__':
    argv = sys.argv[:]
    if len(argv) != 3:
        usage()
    in_folder = argv[1]
    output = argv[2]

    def prepare_icd9(line):
        l = line.split(None, 1)
        if len(l) < 2:
            return line
        return "{0} ({1})".format(l[1], l[0].split('-', 1)[0])

    def prepare_meta(line):
        return line.strip()

    def load_pickle(name):
        with open(os.path.join(in_folder, name), 'rb') as p_in:
            return pickle.load(p_in)

    def load_headers(name, prepare_line):
        with open(os.path.join(in_folder, name), 'rb') as h_in:
            return [ prepare_line(line) for line in h_in.readlines() ]

    feature_names = load_headers("headers_icd9.txt", prepare_icd9)
    X = load_pickle("xtest_ICD9s_only.pkl")
    labels = load_pickle("ytest_gold.pkl")
    pred = load_pickle("ytest_predicted.pkl")

    meta_names = load_headers("other_feature_headers.txt", prepare_meta)
    meta = load_pickle("xtest_other_features.pkl")
    meta_map = {}
    for (ix, m) in enumerate(meta_names):
        meta_map[m] = ix

    pos_ixs = [ ix for (ix, l) in enumerate(labels) if l ]
    random.shuffle(pos_ixs)
    pos_ixs = pos_ixs[:count]
    neg_ixs = [ ix for (ix, l) in enumerate(labels) if not l ]
    random.shuffle(neg_ixs)
    neg_ixs = neg_ixs[:count]
    ixs = pos_ixs + neg_ixs
    ixs.sort(key=lambda ix: -pred[ix])
    feature_map = {}
    for (ix, f) in enumerate(feature_names):
        feature_map[f] = ix
    feature_names = sorted([ f for f in feature_names if X[ixs, feature_map[f]].sum() > min_count ])
    feature_names.append("label")
    feature_names.append("pred")
    feature_names.append("meta")

    def convert_output(out):
        return "1" if out else "0"

    def process_field(row_ix, f):
        if f == "label":
            return convert_output(labels[row_ix])
        if f == "pred":
            return pred[row_ix]
        if f == "meta":
            gender = "F" if meta[row_ix, meta_map["gender"]] > 0 else "M"
            low = "age 18-40" if meta[row_ix, meta_map["18=<age<40"]] > 0 else ""
            medium = "age 40-65" if meta[row_ix, meta_map["40=<age<65"]] > 0 else ""
            high = "age > 65" if meta[row_ix, meta_map["65=<age"]] > 0 else ""
            return "{0} {1}{2}{3}".format(gender, low, medium, high)
        return convert_output(X[row_ix, feature_map[f]])

    print("total true: {0}".format(sum(labels[ixs])), file=sys.stdout)
    print("total features: {0}".format(len(feature_names) - 3), file=sys.stdout)
    print("AUC: {0}".format(roc_auc_score(labels[ixs], pred[ixs])), file=sys.stdout)
    print("writing {0} rows".format(len(ixs)), file=sys.stdout)
    with open(output, 'w') as f_out:
        out = csv.writer(f_out)
        out.writerow(feature_names)
        for row_ix in ixs:
            out.writerow([ process_field(row_ix, f) for f in feature_names ])
    print("done", file=sys.stdout)
