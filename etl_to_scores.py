#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import csv
import sys

def usage():
    print("""
usage: {0} <input> <output>
<input>: the input csv file
<output>: the output csv file
""".strip().format(sys.argv[0]), file=sys.stderr)
    exit(1)

if __name__ == '__main__':
    argv = sys.argv[:]
    if len(argv) != 3:
        usage()
    input = argv[1]
    output = argv[2]

    def process_field(v, f):
        if f == "label":
            return int(v)
        elif f == "pred":
            return float(v)
        return None

    feature_names = None
    with open(input, 'rU') as f_in:
        with open(output, 'w') as f_out:
            out = csv.writer(f_out)
            for row in csv.DictReader(f_in):
                if feature_names is None:
                    feature_names = sorted([ f for f in row.keys() if process_field(row[f], f) is not None ])
                    out.writerow(feature_names)
                out.writerow([ process_field(row[f], f) for f in feature_names ])
