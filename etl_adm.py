#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import os
import csv
import sys
import json
import math
import random
import argparse

import numpy as np
from progress_bar import progress_list, progress_indef

def read_med_map(fmap):
    mmap = {}
    dups = set()
    with open(fmap, 'rb') as f_in:
        for row in csv.reader(f_in, delimiter='\t', quotechar='"'):
            if len(row) >= 4 and row[2] != "null":
                new = row[3]
                # key = row[1]
                # if row[2] != "null":
                key = "{0}".format(row[2])
                if key in mmap:
                    old = mmap[key]
                    if old != new and (old, new) not in dups and (new, old) not in dups:
                        print("mismatch ({0}): {1} != {2}".format(key, old, new), file=sys.stderr)
                        dups.add((old, new))
                        dups.add((new, old))
                        if len(old) > len(new):
                            print("using '{0}'".format(old), file=sys.stderr)
                            continue
                        print("using '{0}'".format(new), file=sys.stderr)
                mmap[key] = new
    return mmap

HIST_IGNORE = "ignore"
HIST_MERGE = "merge"
HIST_SEPARATE = "separate"
HIST = frozenset([ HIST_IGNORE, HIST_MERGE, HIST_SEPARATE, ])
def read_csv(fcsv, fmap, hist_handle):
    key_label = "admitted"
    key_diag = "primary_ed_diag"
    skip = frozenset([ "<NA>", "EncounterEpicCsn_deid", "MRN_deid" ])
    out_skip = frozenset([ "label", "pred", "meta", ])
    labels = []
    metas = []
    rows = []
    feature_list = []
    features = set()

    def get_feature(f):
        if f == key_label:
            return "label"
        if f == key_diag:
            return "meta"
        if f in skip:
            return None
        split = f.index("_")
        hist = f[split:] == "_his"
        f = f[:split]
        if f not in fmap.keys():
            raise ValueError("unknown feature {0}".format(f))
        new_f = fmap[f]
        if hist_handle == HIST_SEPARATE:
            new_f = "{0}_{1}".format(new_f, "his" if hist else "ED")
        elif hist_handle == HIST_IGNORE:
            if hist:
                return None
        if new_f in out_skip:
            res = "_{0}".format(new_f)
        else:
            res = new_f
        features.add(res)
        return res

    def read_row(row):
        if not feature_list:
            for f in row:
                feature_list.append(get_feature(f))
            return
        cur_row = []
        for (pos, v) in enumerate(row):
            feature = feature_list[pos]
            if feature == "label":
                labels.append(float(v) > 0)
            elif feature == "meta":
                metas.append(v)
            elif feature is not None:
                if float(v) > 0:
                    cur_row.append(feature)
        rows.append(cur_row)

    with open(fcsv, 'rb') as f_in:
        csv_in = csv.reader(f_in, delimiter='\t', quotechar='"')
        progress_indef(csv_in, read_row, prefix="reading")

    if len(rows) != len(labels) or len(rows) != len(metas):
        raise ValueError("inconsistent lengths {0} == {1} == {2}".format(len(rows), len(labels), len(metas)))
    features = sorted(features)
    f_lookup = dict([ (f, ix) for (ix, f) in enumerate(features) ])
    matrix = np.zeros((len(rows), len(features)), dtype=np.int8)
    for (rix, row) in enumerate(rows):
        matrix[rix, [ f_lookup[r] for r in row ] ] = 1
    labels = np.array(labels, dtype=np.bool)
    return labels, matrix, metas, features

def write_csv(fcsv, labels, matrix, metas, features):
    with open(fcsv, 'wb') as fo:
        wout = csv.writer(fo)
        fs = features + [ "label", "meta" ]

        def convert(pos, fix):
            if fs[fix] == "label":
                return "1" if labels[pos] else "0"
            if fs[fix] == "meta":
                return metas[pos]
            return "1" if matrix[pos, fix] != 0 else "0"

        def row(pos):
            r = [ convert(pos, fix) for fix in range(len(fs)) ]
            wout.writerow(r)

        wout.writerow(fs)
        progress_list(range(len(labels)), row, prefix="writing")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='ETL for admission data')
    parser.add_argument('--hist', type=str, default="merge", help="how to handle historical medication")
    parser.add_argument('mmap', type=str, help="medication map file")
    parser.add_argument('input', type=str, help="file containing the CSV table")
    parser.add_argument('output', type=str, help="output CSV file")
    args = parser.parse_args()

    mmap = args.mmap
    hist = args.hist
    if hist not in HIST:
        print("unknown historical handling: '{0}'\noptions are:".format(hist), file=sys.stderr)
        for h in HIST:
            print("{0}".format(h), file=sys.stderr)
        sys.exit(2)

    if mmap == args.output or args.input == args.output:
        print("cannot read and write to same file", file=sys.stderr)
        sys.exit(1)

    try:
        mmap = read_med_map(mmap)
        labels, matrix, metas, features = read_csv(args.input, mmap, hist)
        write_csv(args.output, labels, matrix, metas, features)
    except KeyboardInterrupt:
        print("interrupted by user..")
