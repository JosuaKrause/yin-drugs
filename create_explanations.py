#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import os
import csv
import sys
import json
import math
import random
import argparse

import numpy as np
from sklearn.metrics import roc_auc_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier

from progress_bar import progress, progress_list, progress_map

class CacheMap(object):
    def __init__(self, size_max=3000, size=2500):
        self._size_max = size_max
        self._size = size
        self._map = {}

    def __getitem__(self, key):
        count, value = self._map[key]
        self._map[key] = (count + 1, value)
        return value

    def __setitem__(self, key, value):
        if key not in self._map:
            its = self._map.items()
            if len(its) > self._size_max:
                its.sort(key=lambda it: it[1][0], reverse=True)
                its = its[:self._size-1]
                base = its[-1][1][0]
                self._map = dict([ (k, (v[0] - base, v[1])) for (k, v) in its ])
        self._map[key] = (0, value)

    def __contains__(self, key):
        return key in self._map

def read_csv(fcsv, metafeatures):
    print("read csv", file=sys.stdout)
    skip = frozenset([ "label", "pred", "meta", ])
    labels = []
    metas = []
    rows = []
    features = set()
    with open(fcsv, 'rb') as f_in:
        for row in csv.DictReader(f_in):
            labels.append(int(row["label"]) > 0)
            cur_row = []
            for (k, v) in row.items():
                if k not in skip:
                    features.add(k)
                    if int(v) > 0:
                        cur_row.append(k)
            if metafeatures:
                metas.append("")
                metaf = row["meta"]
                cur_row.append(metaf)
                features.add(metaf)
            else:
                metas.append(row["meta"])
            rows.append(cur_row)
    if len(rows) != len(labels) or len(rows) != len(metas):
        raise ValueError("inconsistent lengths {0} == {1} == {2}".format(len(rows), len(labels), len(metas)))
    features = sorted(features)
    f_lookup = dict([ (f, ix) for (ix, f) in enumerate(features) ])
    matrix = np.zeros((len(rows), len(features)), dtype=np.int8)
    for (rix, row) in enumerate(rows):
        matrix[rix, [ f_lookup[r] for r in row ] ] = 1
    labels = np.array(labels, dtype=np.bool)
    print("read csv..done", file=sys.stdout)
    return labels, matrix, metas, features

def create_ixs(matrix, train_ratio, rand):
    print("splitting rows", file=sys.stdout)
    ixs = list(range(matrix.shape[0]))
    train_num = int(math.floor(len(ixs) * train_ratio))
    if rand:
        random.seed(0)
        random.shuffle(ixs)
    ixs_train = ixs[:train_num]
    ixs_test = ixs[train_num:]
    print("splitting rows..done", file=sys.stdout)
    return ixs_train, ixs_test

MODELS = {
    "rf": lambda: RandomForestClassifier(n_jobs=-1, random_state=0),
    "rfl": lambda: RandomForestClassifier(n_jobs=-1, random_state=0, max_depth=11, n_estimators=100),
    "rfm": lambda: RandomForestClassifier(n_jobs=-1, random_state=0, n_estimators=100),
    "lr": lambda: LogisticRegression(),
    "nb": lambda: GaussianNB(),
    "nn": lambda: MLPClassifier(activation='relu', random_state=0, early_stopping=True,
        hidden_layer_sizes=tuple([ 500 for _ in range(5) ]), max_iter=1000,
        learning_rate='adaptive', shuffle=True, alpha=0.0001, beta_1=0.9999, beta_2=0.8),
    "nnm": lambda: MLPClassifier(activation='relu', random_state=0, early_stopping=True,
        hidden_layer_sizes=tuple([ 500 for _ in range(4) ]), max_iter=1000,
        learning_rate='adaptive', shuffle=True),
}
def train(algo, labels, matrix, ixs_train):
    print("training the model", file=sys.stdout)
    X = matrix[ixs_train, :]
    y = labels[ixs_train]
    model = MODELS[algo]()
    model.fit(X, y)
    preds = model.predict_proba(X)
    preds = preds[:, list(model.classes_).index(True)]
    auc = roc_auc_score(y, preds)
    print("training AUC: {0}".format(auc))
    print("training the model..done", file=sys.stdout)
    return model, preds.tolist(), auc

def stats(labels, matrix, model, ixs_test):
    print("computing statistics", file=sys.stdout)
    X = matrix[ixs_test, :]
    y = labels[ixs_test]
    preds = model.predict_proba(X)
    preds = preds[:, list(model.classes_).index(True)]
    total_true = np.sum(y)
    total_rows, total_features = X.shape
    auc = roc_auc_score(y, preds)
    print("computing statistics..done", file=sys.stdout)
    return total_true, total_rows, total_features, auc, preds

def get_text_file(base, ix):
    if base is None:
        return None
    name = str(ix).rjust(6, '0') + '.txt'
    fn = os.path.join(base, name[0:2], name[2:4])
    res = os.path.join(fn, name[4:])
    return res if os.path.exists(res) else None

def get_explanation(matrix, model, ix, real_pred, up, cache_expl, cache_shorten, cache_stats):
    best = 1.0 if up else 0.0
    p_col = list(model.classes_).index(True)
    cur_row = np.array(matrix[ix, :])

    def pick(cur_preds, count):
        orig_pred = cur_preds[-1]
        next = 0
        while next < count:
            # at this point the best we can hope is equal pred
            if cur_preds[next] == orig_pred:
                return next
            next += 1
        return None

    def compute_pred(cur_row, features, count, value):
        # last row is original vector
        X = np.repeat([ cur_row ], count + 1, axis=0)
        for (rix, cix) in enumerate(features):
            X[rix, cix] = value
        cur_preds = model.predict_proba(X)
        cur_preds = cur_preds[:, p_col]
        if cur_preds[-1] == best:
            next = None
        elif up:
            next = np.argmax(cur_preds)
        else:
            next = np.argmin(cur_preds)
        if next is not None and next >= count:
            next = pick(cur_preds, count)
        if next is not None:
            cur_row = np.array(X[next, :])
        return cur_preds, next, cur_row

    def build_up(cur_row, expl, rows):
        nz, = np.nonzero(cur_row)
        nzc = nz.shape[0]
        if nzc == 0:
            return expl, rows
        key = tuple(nz)
        if key in cache_expl:
            feature, pred = cache_expl[key]
            expl.append((feature, pred))
            cur_row = np.array(cur_row)
            cur_row[feature] = 0
            rows.append(cur_row)
            cache_stats["expl_hit"] += 1
            return build_up(cur_row, expl, rows)
        cur_preds, next, cur_row = compute_pred(cur_row, nz, nzc, 0)
        if next is None:
            return expl, rows
        e = (nz[next], cur_preds[next])
        expl.append(e)
        rows.append(cur_row)
        cache_expl[key] = e
        cache_stats["expl_miss"] += 1
        return build_up(cur_row, expl, rows)

    expl, rows = build_up(cur_row, [], [])

    def shorten_expl(add_features, cur_row, res_fs, pred):
        if not add_features:
            return (res_fs, pred)
        nz, = np.nonzero(cur_row)
        key = (tuple(nz), tuple(add_features))
        if key in cache_shorten:
            r, pred = cache_shorten[key]
            res_fs += r
            cache_stats["shorten_hit"] += 1
            return (res_fs, pred)
        cur_preds, next, cur_row = compute_pred(cur_row, add_features, len(add_features), 1)
        if next is None:
            return (res_fs, pred)
        feature = add_features[next]
        add_features = [ f for f in add_features if f != feature ]
        from_ix = len(res_fs)
        res_fs.append(feature)
        res_fs, pred = shorten_expl(add_features, cur_row, res_fs, cur_preds[next])
        cache_shorten[key] = (res_fs[from_ix:], pred)
        cache_stats["shorten_miss"] += 1
        return (res_fs, pred)

    shorten = []
    add_features = []
    first_mismatch = False
    prev_pred = real_pred
    for (eix, e) in enumerate(expl):
        fix, pred = e
        add_features.append(fix)
        if not first_mismatch and prev_pred != pred:
            shorten.append(([], pred))
        else:
            shorten.append(shorten_expl(add_features, rows[eix], [], pred))
            first_mismatch = True
        prev_pred = pred
    return expl, shorten

def get_object(f_out, obj, base, labels, metas, features, matrix, model, ixs, ixs_train, preds_train, train_auc):
    total_true, total_rows, total_features, auc, preds = stats(labels, matrix, model, ixs)
    print("total true: {0}".format(total_true), file=sys.stdout)
    print("total rows: {0}".format(total_rows), file=sys.stdout)
    print("total features: {0}".format(total_features), file=sys.stdout)
    print("AUC: {0}".format(auc), file=sys.stdout)
    cache_expl_up = CacheMap()
    cache_expl_down = CacheMap()
    cache_shorten_up = CacheMap()
    cache_shorten_down = CacheMap()
    cache_stats_up = {
        "expl_hit": 0,
        "expl_miss": 0,
        "shorten_hit": 0,
        "shorten_miss": 0,
    }
    cache_stats_down = {
        "expl_hit": 0,
        "expl_miss": 0,
        "shorten_hit": 0,
        "shorten_miss": 0,
    }

    def get_expl(posix):
        pos, ix = posix
        if pos < len(old_expls):
            return old_expls[pos]
        up_expl, up_shorten = get_explanation(matrix, model, ix, preds[pos],
                    True, cache_expl_up, cache_shorten_up, cache_stats_up)
        down_expl, down_shorten = get_explanation(matrix, model, ix, preds[pos],
                    False, cache_expl_down, cache_shorten_down, cache_stats_down)
        res = {
            "ix": ix,
            "pred": preds[pos],
            "label": 1 if labels[ix] else 0,
            "up": [ (up_expl[fix][0], s[1], s[0]) for (fix, s) in enumerate(up_shorten) ],
            "down": [ (down_expl[fix][0], s[1], s[0]) for (fix, s) in enumerate(down_shorten) ],
            "file": get_text_file(base, ix),
            "meta": metas[ix],
        }
        tmp_expls.append(res)
        if pos % 100 == 0:
            obj["expls"] = tmp_expls
            write_file(f_out, obj)
        return res

    old_expls = obj.get("expls", [])
    if ixs != obj.get("ixs", []) or \
            total_true != obj.get("total_true", 0) or \
            total_rows != obj.get("total_rows", 0) or \
            total_features != obj.get("total_features", 0) or \
            features != obj.get("features", []) or \
            auc != obj.get("auc", 0.0) or \
            ixs_train != obj.get("train_ixs", []) or \
            preds_train != obj.get("train_preds", []) or \
            train_auc != obj.get("train_auc", 0.0):
        if os.path.exists(f_out):
            print("mismatching preexisting output", file=sys.stdout)
        old_expls = []
    tmp_expls = old_expls
    obj = {
        "ixs": ixs,
        "total_true": total_true,
        "total_rows": total_rows,
        "total_features": total_features,
        "features": features,
        "auc": auc,
        "train_ixs": ixs_train,
        "train_preds": preds_train,
        "train_auc": train_auc,
        "expls": [],
    }
    obj["expls"] = progress_map(list(enumerate(ixs)), get_expl, prefix='explanations')

    def stat(cs, pre):
        if cs[pre + "_hit"] + cs[pre + "_miss"] == 0:
            return 0
        return float(cs[pre + "_hit"]) / float(cs[pre + "_hit"] + cs[pre + "_miss"]) * 100.0

    print("cache up: {0:6.2f}% expl {1:6.2f}% shorten".format(
        stat(cache_stats_up, "expl"),
        stat(cache_stats_up, "shorten")), file=sys.stdout)
    print("cache down: {0:6.2f}% expl {1:6.2f}% shorten".format(
        stat(cache_stats_down, "expl"),
        stat(cache_stats_down, "shorten")), file=sys.stdout)
    return obj

def write_file(f_out, obj):
    with open(f_out, 'wb') as w_out:
        json.dump(obj, w_out, sort_keys=True, indent=2, separators=(',', ': '))

def infer_output(f_in, model, metafeatures):
    ext = f_in.rfind(".")
    if f_in[ext:] != ".csv":
        print("WARNING: expected file extention \".csv\" got \"{0}\"".format(f_in[ext+1:]), file=sys.stderr)
    pre = f_in[:ext]
    return "{0}.expl.{1}{2}.json".format(pre, model, ".mf" if metafeatures else "")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Explanation generator')
    parser.add_argument('--metafeatures', action='store_true', help="interprets meta info as features")
    parser.add_argument('--det-split', action='store_true', help="use deterministic split instead of random")
    parser.add_argument('--base', type=str, default="-", help="associated text file directory")
    parser.add_argument('--model', type=str, default="rf", help="the ML model")
    parser.add_argument('--force', action='store_true', help="overwrite existing files")
    parser.add_argument('-r', type=float, default=0.2, help="ratio of training to test")
    parser.add_argument('input', type=str, help="file containing the CSV table")
    parser.add_argument('output', type=str, help="JSON output file or '-' to infer")
    args = parser.parse_args()

    if args.input == args.output:
        print("input and output file cannot be the same", file=sys.stderr)
        sys.exit(2)

    model = args.model
    if model not in MODELS.keys():
        print("unknown model: {0}".format(model), file=sys.stdout)
        print("available models:\n{0}".format("\n".join(MODELS.keys())), file=sys.stdout)
        sys.exit(3)
    base = args.base if args.base != "-" else None
    metafeatures = args.metafeatures
    rand = not args.det_split
    train_ratio = args.r
    f_in = args.input
    f_out = args.output if args.output != "-" else infer_output(f_in, model, metafeatures)
    print("output file: {0}".format(f_out), file=sys.stdout)
    labels, matrix, metas, features = read_csv(f_in, metafeatures)
    ixs_train, ixs_test = create_ixs(matrix, train_ratio, rand)
    model, preds_train, train_auc = train(model, labels, matrix, ixs_train)
    print("found text files" if get_text_file(base, 0) is not None else "no text files", file=sys.stdout)
    obj = {}
    if os.path.exists(f_out) and not args.force:
        print("amending output file", file=sys.stdout)
        with open(f_out, 'rb') as r_out:
            obj = json.load(r_out)
    try:
        obj = get_object(f_out, obj, base, labels, metas, features, matrix, model, ixs_test, ixs_train, preds_train, train_auc)

        print("writing output", file=sys.stdout)
        write_file(f_out, obj)
        print("writing output..done", file=sys.stdout)
    except KeyboardInterrupt:
        print("interrupted by user..")
        sys.exit(1)
