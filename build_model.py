#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import os
import csv
import sys
import json
import math
import cPickle as pickle
import argparse

import numpy as np
from sklearn.metrics import roc_auc_score
from sklearn.ensemble import RandomForestClassifier

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train Model')
    parser.add_argument('--ratio', type=float, default=0.1, help="test train ratio between 0..1")
    parser.add_argument('input', type=str, help="input file")
    parser.add_argument('output', type=str, help="output file")
    args = parser.parse_args()

    features = None
    rows = []
    metas = []
    labels = []
    with open(args.input, 'rb') as f_in:
        cin = csv.DictReader(f_in)
        for row in cin:
            if features is None:
                features = [ k for k in sorted(row.keys()) if k != 'label' and k != 'meta' ]
            metas.append(row['meta'])
            labels.append(bool(int(row['label'])))
            rows.append([ int(row[f]) for f in features ])

    data = np.matrix(rows, dtype=np.int8)
    labels = np.array(labels, dtype=np.bool)

    np.random.seed(0)
    num = data.shape[0]
    train = int(math.floor(num * args.ratio))
    all_ixs = np.arange(num)
    np.random.shuffle(all_ixs)

    train_ixs = all_ixs[:train]
    test_ixs = all_ixs[train:]

    print("test data: {0}".format(test_ixs.shape[0]), file=sys.stdout)
    print("test true: {0}".format(np.count_nonzero(labels[test_ixs])), file=sys.stdout)
    print("features: {0}".format(len(features)), file=sys.stdout)
    if train > 0:
        model = RandomForestClassifier(n_jobs=-1, random_state=0)
        model.fit(data[train_ixs, :], labels[train_ixs])

        pred = model.predict_proba(data)
        pred = pred[:, list(model.classes_).index(True)]
        print("prediction done", file=sys.stdout)
        print("AUC: {0}".format(roc_auc_score(labels[test_ixs], pred[test_ixs])), file=sys.stdout)
    else:
        pred = None
        model = None

    with open(args.output, 'w') as f_out:
        pickle.dump((data, features, labels, pred, metas, train_ixs, test_ixs, model), f_out, -1)

