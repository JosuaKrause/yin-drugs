#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import os
import csv
import sys
import json

def usage():
    print("""
usage: {0} <input> <output>
<input>: the input csv file
<output>: the output json file
""".strip().format(sys.argv[0]), file=sys.stderr)
    exit(1)

if __name__ == '__main__':
    argv = sys.argv[:]
    if len(argv) != 3:
        usage()
    input = argv[1]
    output = argv[2]
    patients = []
    skip = frozenset(["label", "pred", "meta"])
    with open(input, 'rU') as f_in:
        for row in csv.DictReader(f_in):
            p = {
                "label": int(row["label"]) > 0,
                "pred": float(row["pred"]),
                "meta": row["meta"],
                "medication": [ k for (k, v) in row.items() if k not in skip and int(v) > 0 ],
            }
            patients.append(p)

    with open(output, 'w') as f_out:
        json.dump(patients, f_out, sort_keys=True)
