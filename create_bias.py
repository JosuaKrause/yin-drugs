#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import os
import csv
import sys
import json
import math
import random
import argparse

import numpy as np

from progress_bar import progress, progress_list, progress_map

def read_csv(fcsv, remove_empty):
    skip = frozenset([ "label", "pred", "meta", ])
    labels = []
    metas = []
    rows = []
    features = set()
    with open(fcsv, 'rb') as f_in:
        for row in csv.DictReader(f_in):
            cur_row = []
            for (k, v) in row.items():
                if k not in skip:
                    features.add(k)
                    if int(v) > 0:
                        cur_row.append(k)
            if remove_empty and not len(cur_row):
                continue
            metas.append(row["meta"])
            labels.append(int(row["label"]) > 0)
            rows.append(cur_row)
    if len(rows) != len(labels) or len(rows) != len(metas):
        raise ValueError("inconsistent lengths {0} == {1} == {2}".format(len(rows), len(labels), len(metas)))
    features = sorted(features)
    f_lookup = dict([ (f, ix) for (ix, f) in enumerate(features) ])
    matrix = np.zeros((len(rows), len(features)), dtype=np.int8)
    for (rix, row) in enumerate(rows):
        matrix[rix, [ f_lookup[r] for r in row ] ] = 1
    labels = np.array(labels, dtype=np.bool)
    return labels, matrix, metas, features

def get_split(matrix, train_ratio):
    random.seed(0)
    ixs = list(range(matrix.shape[0]))
    train_num = int(math.floor(len(ixs) * train_ratio))
    random.shuffle(ixs)
    ixs_train = ixs[:train_num]
    ixs_test = ixs[train_num:]
    return ixs_train, ixs_test

def get_meta_counts(metas, labels):
    ms = {}
    mp = {}
    for (pos, m) in enumerate(metas):
        if m not in ms:
            ms[m] = 0
            mp[m] = 0
        ms[m] += 1
        if labels[pos]:
            mp[m] += 1
    return [
        (m[0], m[1], mp[m[0]])
        for m in
        sorted(ms.items(), key=lambda m: (m[1], m[0]), reverse=True)
    ]

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Bias generator')
    parser.add_argument('--forge', type=str, help="pairs of meta and label separated by ';'. meta=label")
    parser.add_argument('--ignore', type=str, help="list of metas to ignore separated by ';'")
    parser.add_argument('--list', action='store_true', help="list metas with count")
    parser.add_argument('--remove-empty', action='store_true', help="removes empty rows")
    parser.add_argument('-r', type=float, default=0.2, help="ratio of training to test")
    parser.add_argument('--only-train', action='store_true', help="apply only on train data")
    parser.add_argument('--only-test', action='store_true', help="apply only on test data")
    parser.add_argument('input', type=str, help="file containing the CSV table")
    parser.add_argument('output', type=str, help="CSV output file or '-' for ignoring")
    args = parser.parse_args()

    if args.input == args.output:
        print("input and output file cannot be the same", file=sys.stderr)
        sys.exit(3)

    do_train = not args.only_test
    do_test = not args.only_train
    train_ratio = args.r
    if not do_train and not do_test:
        print("cannot do only both", file=sys.stderr)
        sys.exit(2)

    remove_empty = args.remove_empty

    forge = dict([ fg.split('=') for fg in args.forge.split(';') ]) if args.forge else {}
    ignore = set(args.ignore.split(';')) if args.ignore else set()
    f_in = args.input
    f_out = args.output if args.output != "-" else os.devnull
    if f_out != os.devnull:
        print("output file: {0}".format(f_out), file=sys.stdout)
    labels, matrix, metas, features = read_csv(f_in, remove_empty)
    if not do_train or not do_test:
        ixs_train, ixs_test = get_split(matrix, train_ratio)
    else:
        ixs_train = list(range(matrix.shape[0]))
        ixs_test = []

    try:
        if args.list:
            for m in get_meta_counts(metas, labels):
                print("'{0}' ({1}P / {2})".format(m[0], m[2], m[1]), file=sys.stdout)
            sys.exit(0)

        with open(f_out, 'wb') as fo:
            wout = csv.writer(fo)
            fs = features + [ "label", "meta" ]
            obj = {}

            def normal_convert(pos, fix):
                if fs[fix] == "label":
                    return "1" if labels[pos] else "0"
                if fs[fix] == "meta":
                    return metas[pos]
                return "1" if matrix[pos, fix] != 0 else "0"

            def convert(pos, fix):
                if fs[fix] == "label":
                    l = labels[pos] if metas[pos] not in forge else int(forge[metas[pos]])
                    return "1" if l else "0"
                if fs[fix] == "meta":
                    return metas[pos]
                return "1" if matrix[pos, fix] != 0 else "0"

            def rows(dom):
                conv = convert if dom else normal_convert
                ign = ignore if dom else set()

                def row(pos):
                    if metas[pos] in ign:
                        return
                    obj["rows"] += 1
                    r = [ conv(pos, fix) for fix in range(len(fs)) ]
                    wout.writerow(r)

                return row

            wout.writerow(fs)
            obj["rows"] = 0
            progress_list(ixs_train, rows(do_train), prefix=("writing train" if ixs_test else "writing"))
            train_count = obj["rows"]
            if ixs_test:
                obj["rows"] = 0
                progress_list(ixs_test, rows(do_test), prefix="writing test")
                test_count = obj["rows"]
                print("split at: {0}".format(float(train_count) / (train_count + test_count)), file=sys.stdout)

    except KeyboardInterrupt:
        print("interrupted by user..")
        sys.exit(1)
