#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import os
import csv
import sys

def usage():
    print("""
usage: {0} <prefix> <input> <output>
<prefix>: the column prefix
<input>: the input csv file
<output>: the output csv file
""".strip().format(sys.argv[0]), file=sys.stderr)
    exit(1)

if __name__ == '__main__':
    argv = sys.argv[:]
    if len(argv) != 4:
        usage()
    prefix = argv[1]
    if prefix == '-':
        prefix = None
    input = argv[2]
    output = argv[3]

    feature_names = None
    rows = []
    fixed = ["label", "pred"]
    with open(input, 'r') as f_in:
        for row in csv.DictReader(f_in):
            if feature_names is None:
                feature_names = sorted(filter(lambda f: prefix is None or f.startswith(prefix), row.keys()))
                for f in fixed:
                    feature_names.append(f)
            rows.append([ ("true" if int(row[f]) == 1 else "false") if f != "pred" else row[f] for f in feature_names ])

    fixed = set(fixed)
    with open(output, 'w') as f_out:
        out = csv.writer(f_out, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        out.writerow([ f[len(prefix):] if prefix is not None and f not in fixed else f for f in feature_names ])
        for r in rows:
            out.writerow(r)
