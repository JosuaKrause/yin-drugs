#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import os
import csv
import sys
import argparse

import numpy as np

skip = frozenset([ "label", "pred", "meta", ])
skip_out = frozenset([ "label", "meta", ])
words = frozenset([
    "AEPB",
    "CHEW",
    "CREA",
    "DLGL",
    "GRAM",
    "IM",
    "IN",
    "INHL",
    "INJ",
    "IV",
    "IVPB",
    "HCL",
    "LIQD",
    "MG",
    "ML",
    "ML",
    "OPHT",
    "ORAL",
    "ORDERABLE",
    "OTIC",
    "RECT",
    "SOLN",
    "SOLR",
    "SUSP",
    "SYRG",
    "SYRINGE",
    "TAB",
    "TBEC",
    "TBSR",
    "TOP",
    "UNIT",
])
def read_csv(fcsv):
    print("read csv", file=sys.stdout)
    labels = []
    metas = []
    rows = []
    features = set()
    with open(fcsv, 'rb') as f_in:
        for row in csv.DictReader(f_in):
            labels.append(int(row["label"]) > 0)
            metas.append(row["meta"])
            cur_row = []
            for (k, v) in row.items():
                if k not in skip and int(v) > 0:
                    cur_row.append(k)
                    features.add(k)
            rows.append(cur_row)
    if len(rows) != len(labels) or len(rows) != len(metas):
        raise ValueError("inconsistent lengths {0} == {1} == {2}".format(len(rows), len(labels), len(metas)))
    features = sorted(features)
    f_lookup = dict([ (f, ix) for (ix, f) in enumerate(features) ])
    matrix = np.zeros((len(rows), len(features)), dtype=np.int8)
    for (rix, row) in enumerate(rows):
        matrix[rix, [ f_lookup[r] for r in row ] ] = 1
    labels = np.array(labels, dtype=np.bool)
    print("read csv..done", file=sys.stdout)
    return labels, matrix, metas, features

def is_proper(s):
    if not s:
        return True
    if any([ str(n) == s or str(n) == str(s[0]) for n in range(0, 10) ]):
        return False
    if "%" in s:
        return False
    if "(" == s[0]:
        return False
    return s not in words

def canonic_feature(f):
    res = []
    for fs in f.split(" "):
        fss = fs.split(";")
        if len(fss) > 1:
            if is_proper(fss[0]):
                res.append(fss[0])
            break
        fs = fss[0].strip()
        if is_proper(fs):
            res.append(fs)
        else:
            break
    res = " ".join(res)
    if not res:
        res = f
    if res in skip_out:
        res = "_" + res
    return res

def write_csv(fcsv, labels, matrix, metas, features):
    print("write csv", file=sys.stdout)
    with open(fcsv, 'wb') as f_out:
        w = csv.writer(f_out)
        new_features = sorted(set([ canonic_feature(f) for f in features ]))
        f_lookup = dict([ (f, ix) for (ix, f) in enumerate(new_features) ])
        cf_lookup = dict([ (f, f_lookup[canonic_feature(f)]) for f in features ])
        all_features = new_features + sorted(skip_out)
        w.writerow(all_features)
        for row in range(matrix.shape[0]):
            vec = np.zeros((len(new_features), ))
            ixs, = np.nonzero(matrix[row, :])
            for ix in ixs:
                pos = cf_lookup[features[ix]]
                vec[pos] = 1

            def convert(k):
                if k == "label":
                    return "1" if labels[row] else "0"
                elif k == "meta":
                    return metas[row]
                else:
                    return "1" if vec[f_lookup[k]] != 0 else "0"

            w.writerow([ convert(k) for k in all_features ])
    print("write csv..done", file=sys.stdout)
    print("features", file=sys.stdout)
    for f in new_features:
        print(str(f), file=sys.stdout)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Simplify Data')
    parser.add_argument('input', type=str, help="file containing the CSV table")
    parser.add_argument('output', type=str, help="output CSV file")
    args = parser.parse_args()

    if args.input == args.output:
        print("input and output file cannot be the same", file=sys.stderr)
        sys.exit(1)

    labels, matrix, metas, features = read_csv(args.input)
    write_csv(args.output, labels, matrix, metas, features)

