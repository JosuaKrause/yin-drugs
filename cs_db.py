#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import os
import sys
import csv
import math
import time
import argparse

import numpy as np
from sklearn.metrics import roc_auc_score
from sklearn.cluster import AgglomerativeClustering

from sklearn.cluster import KMeans
from sklearn.tree import DecisionTreeClassifier
from scipy.stats import chisquare

from sklearn import manifold

from progress_bar import progress, progress_list, progress_map
import rule_classifier
import dt_classifier
import file_classifier

CLUSTER_KMEDOID = "k-medoid"
CLUSTER_AGGLOMERATIVE = "agglomerative"
CLUSTER_SPLIT = "data split"
CLUSTER_TREE = "decision tree"
CLUSTER_RULES = "rules"
CLUSTER_NO_MODEL = "no model"
CLUSTER_META = "meta"
CLUSTER_DEFAULT = CLUSTER_KMEDOID
CLUSTER_PARAMS = frozenset([
    CLUSTER_KMEDOID,
    CLUSTER_AGGLOMERATIVE,
    CLUSTER_SPLIT,
    # CLUSTER_TREE,
    CLUSTER_RULES,
    CLUSTER_NO_MODEL,
    CLUSTER_META,
])

PROJECT_MDS = "MDS"
PROJECT_TSNE = "t-SNE"
PROJECT_DEFAULT = PROJECT_TSNE
PROJECT_PARAMS = frozenset([
    PROJECT_MDS,
    PROJECT_TSNE,
])

# DISTANCE_NEW_TANIMOTO = "new tanimoto"
# DISTANCE_COSINE = "cosine"
DISTANCE_TANIMOTO = "tanimoto"
DISTANCE_SIMILARITY = "similarity"
DISTANCE_DIFF = "diff"
DISTANCE_PDIFF = "pdiff"
DISTANCE_DEFAULT = DISTANCE_TANIMOTO + " 1"
DISTANCE_PARAMS = frozenset([
    DISTANCE_TANIMOTO,
    # DISTANCE_NEW_TANIMOTO,
    # DISTANCE_COSINE,
    # DISTANCE_SIMILARITY,
    DISTANCE_DIFF,
    DISTANCE_PDIFF,
])

def _dist_tanimoto(orig_vec_a, orig_vec_b, value_lookup, weight_list, p_a, p_b):
    names = []
    vs = []
    for (vix, v) in enumerate(value_lookup):
        names.append(" " + str(v))
        vec_a = np.equal(orig_vec_a, vix).astype(np.float64) * weight_list
        vec_b = np.equal(orig_vec_b, vix).astype(np.float64) * weight_list
        conj = np.dot(vec_a, vec_b)
        disj = np.dot(vec_a, vec_a) + np.dot(vec_b, vec_b) - conj
        if disj == 0:
            vs.append(np.float64(0))
        elif conj == 0:
            vs.append(-np.log2(np.float64(0.5) / np.float64(disj)))
        else:
            vs.append(-np.log2(np.float64(conj) / np.float64(disj)))
    return vs, names

def _dist_sim(vec_a, vec_b, value_lookup, weight_list, p_a, p_b):
    neqs = np.not_equal(vec_a, vec_b)
    ws = weight_list[np.nonzero(neqs)]
    ms = 1.0 / ws[np.nonzero(ws)]
    return [ np.float64(np.sum(ms)) / np.float64(np.count_nonzero(weight_list)) ], [ "" ]

def _dist_diff(vec_a, vec_b, value_lookup, weight_list, p_a, p_b):
    neqs = np.not_equal(vec_a, vec_b)
    return [ np.count_nonzero(neqs) ], [ "" ]

def _dist_pdiff(vec_a, vec_b, value_lookup, weight_list, p_a, p_b):
    neqs = np.not_equal(vec_a, vec_b)
    dp = (p_a - p_b) * (p_a - p_b) + 1
    return [ np.log2(np.count_nonzero(neqs) + 1) * dp ], [ "" ]

DISTANCES = {}
DISTANCES[DISTANCE_PDIFF] = _dist_pdiff
DISTANCES[DISTANCE_DIFF] = _dist_diff
DISTANCES[DISTANCE_SIMILARITY] = _dist_sim
DISTANCES[DISTANCE_TANIMOTO] = _dist_tanimoto

SKIP_COLS = frozenset([ "label", "pred", "meta" ])

class CSDB(object):
    def __init__(self, csvfile, weights, cache, msg):
        self._msg = msg
        self._cache = cache
        cache.verbose = True
        with self._cache.get_hnd({ "id": "data", "weights": weights, }, section="data", method="pickle") as hnd:
            if hnd.has():
                label, pred, meta, meta_lookup, feature_list, value_lookup, dists, x = hnd.read()
            else:
                label, pred, meta, meta_lookup, feature_list, value_lookup, dists, x = hnd.write(self._do_load(csvfile, weights))
        self._label = label
        self._pred = pred
        self._meta = meta
        self._meta_lookup = meta_lookup
        self._feature_list = feature_list
        self._feature_lookup = dict([ (f, fix) for (fix, f) in enumerate(feature_list) ])
        self._value_lookup = value_lookup
        self._dists = dists
        self._x = x
        self._msg("{0} rows {1} metas {2} features {3} values", self._label.shape[0], len(self._meta_lookup), len(self._feature_list), len(self._value_lookup))

        value_ixs = {}
        for (vix, v) in enumerate(self._value_lookup):
            value_ixs[v] = vix
        self._value_ixs = value_ixs

    def _do_load(self, csvfile, weights):
        load_time = time.clock()
        self._msg("load data")
        label = []
        pred = []
        meta_ixs = {}
        meta_lookup = []
        meta = []
        weight_list = []
        feature_list = []
        features = []
        value_ixs = {}
        value_lookup = []

        with open(csvfile, 'r') as f:
            for row in csv.DictReader(f):
                label.append(int(row["label"]) != 0)
                pred.append(float(row["pred"]))
                m = row["meta"]
                if m not in meta_ixs:
                    meta_ixs[m] = len(meta_lookup)
                    meta_lookup.append(m)
                meta.append(meta_ixs[m])

                def get_f(f):
                    return f[1:] if len(f) > 1 and f[0] == "_" and f[1:] in SKIP_COLS else f

                def get_col(f):
                    return "_{0}".format(f) if f in SKIP_COLS else f

                if not feature_list:
                    feature_list = sorted([ get_f(f) for f in row.keys() if f not in SKIP_COLS ])
                    if weights is not None and sorted(weights.keys()) != feature_list:
                        raise ValueError("weights and features are not the same")
                    weight_list = [ weights[f] if weights is not None else 1.0 for f in feature_list ]
                    features = [ [] for _ in feature_list ]
                for (ix, f) in enumerate(feature_list):
                    v = row[get_col(f)]
                    if v not in value_ixs:
                        value_ixs[v] = len(value_lookup)
                        value_lookup.append(v)
                    features[ix].append(value_ixs[v])

        def get_int_type(arr):
            for dt in [ np.int8, np.int16, np.int32, np.int64 ]:
                if len(arr) <= np.iinfo(dt).max:
                    return dt
            raise ValueError("array too large: {0}".format(len(arr)))

        weight_list = np.array(weight_list, dtype=np.float64)
        no_weight_list = np.ones(weight_list.shape, dtype=np.float64)
        pred = np.array(pred, dtype=np.float64)
        label = np.array(label, dtype=np.bool)
        meta = np.array(meta, dtype=get_int_type(meta_lookup))
        x = np.zeros((len(label), len(feature_list)), dtype=get_int_type(value_lookup))
        for (c, col) in enumerate(features):
            for (r, v) in enumerate(col):
                x[r, c] = v
        self._msg("loading took {0}s", time.clock() - load_time)
        dist_time = time.clock()
        self._msg("compute distances")
        dists = {}
        for d in DISTANCE_PARAMS:
            dists.update(self._get_dists(x, value_lookup, DISTANCES[d], d, no_weight_list, pred))
            if not np.array_equal(weight_list, no_weight_list):
                dists.update(self._get_dists(x, value_lookup, DISTANCES[d], d + " (weighted)", weight_list, pred))
        self._msg("distances took {0}s", time.clock() - dist_time)
        return label, pred, meta, meta_lookup, feature_list, value_lookup, dists, x

    def _get_dists(self, x, value_lookup, metric, metric_name, weight_list, pred):
        dists = {}

        def process_row(row, size):
            r = x[row, :]
            pr = pred[row]
            for col in xrange(row):
                vs, names = metric(r, x[col, :], value_lookup, weight_list, pr, pred[col])
                for (ix, v) in enumerate(vs):
                    n = names[ix]
                    mn = metric_name + n
                    if mn not in dists:
                        dists[mn] = np.zeros((size, size), dtype=np.float64)
                    dd = dists[mn]
                    dd[row, col] = np.clip(v, 0, 1e6)
                    dd[col, row] = dd[row, col]

        progress(0, x.shape[0], process_row, prefix=metric_name)
        return dists

    def get_cache(self):
        return self._cache

    def get_knn(self, from_ix, to_ixs, metric_name, k=None, max_dist=None):
        d = self._dists[metric_name][from_ix, :]
        ixs = list(to_ixs)
        if max_dist is not None:
            md_ixs, = np.nonzero(d[ixs] <= max_dist)
            ixs = (np.array(ixs)[md_ixs]).tolist()
        ixs.sort(key=lambda ix: d[ix])
        real_k = k if k is not None else len(ixs) - 1
        if real_k < len(ixs):
            ref = d[ixs[real_k]]
            while real_k + 1 < len(ixs) and d[ixs[real_k + 1]] <= ref:
                real_k += 1
        return [ [ ix, d[ix], ] for ix in ixs[:real_k + 1] ]

    def get_neighbors(self, ix, metric_name, max_dist):
        dists = self._dists[metric_name][ix, :]
        res_ixs, = np.nonzero(dists <= max_dist)
        return res_ixs

    def get_distance_pairs(self, metric_name, max_dist):
        res = []
        dists = self._dists[metric_name]
        for x in range(dists.shape[0]):
            for y in range(x):
                d = dists[x, y]
                if d < max_dist and x != y:
                    res.append((y, x, d)) # y < x
        res.sort(key=lambda r: r[2])
        return res

    def max_cluster_distance(self, metric_name, aixs, bixs):
        return np.max(self._dists[metric_name][aixs, :][:, bixs])

    def get_centroid(self, metric_name, ixs):
        return ixs[np.argmin(np.mean(self._dists[metric_name][ixs, :][:, ixs], axis=1))]

    def get_close_representatives(self, metric_name, max_dist):
        cluster_lookup = {}
        cluster_ixs = {}

        def get_cluster(ix):
            if ix in cluster_lookup:
                res = cluster_lookup[ix]
                if res != ix:
                    res = get_cluster(res)
                    cluster_lookup[ix] = res
                    cluster_ixs[ix] = None
                return res
            cluster_lookup[ix] = ix
            cluster_ixs[ix] = [ ix ]
            return ix

        def merge_cluster(aix, bix):
            if aix > bix:
                merge_cluster(bix, aix)
                return
            aix = get_cluster(aix)
            bix = get_cluster(bix)
            if aix == bix:
                return
            cluster_ixs[aix] = get_cluster_ixs(aix) + get_cluster_ixs(bix)
            cluster_ixs[aix].sort()
            cluster_lookup[bix] = aix
            if get_cluster(bix) != aix:
                raise ValueError("cluster mismatch {0} != {1} for {2}".format(get_cluster(bix), aix, bix))

        def get_cluster_ixs(ix):
            res = cluster_ixs[get_cluster(ix)]
            if res is None:
                raise ValueError("should not happen! {0} {1} {2}".format(ix, get_cluster(ix), res))
            return res

        def process_pairs(e):
            from_ix, to_ix, d = e
            from_ix = get_cluster(from_ix)
            to_ix = get_cluster(to_ix)
            if from_ix == to_ix:
                return
            # fixs = get_cluster_ixs(from_ix)
            # tixs = get_cluster_ixs(to_ix)
            # if self.max_cluster_distance(metric_name, fixs, tixs) < max_dist:
            #     merge_cluster(from_ix, to_ix)
            merge_cluster(from_ix, to_ix)

        progress_list(self.get_distance_pairs(metric_name, max_dist), process_pairs, prefix='pairs')
        clusters = set()
        progress(0, self._dists[metric_name].shape[0], lambda ix, _: clusters.add(get_cluster(ix)), prefix='clusters')
        print("found", len(clusters), "clusters")
        ccixs = [ get_cluster_ixs(cix) for cix in list(clusters) ]
        cixs = progress_map(ccixs, lambda cixs: self.get_centroid(metric_name, cixs), prefix='centroids')
        return cixs, ccixs

    def get_min_max_pred(self, ixs):
        ixs = sorted(ixs)
        pred = self._pred[ixs]
        min_ix = np.argmin(pred)
        max_ix = np.argmax(pred)
        return ixs[min_ix], ixs[max_ix]

    def get_features(self):
        return self._feature_list

    def get_vec(self, ix, v):
        vec = self._x[ix, :]
        return [ self._feature_list[pos] for pos in range(self._x.shape[1]) if self._value_lookup[vec[pos]] == v ]

    def get_vecs(self, ixs, v):
        vec = np.mean(np.float64(self._x[ixs, :] == self._value_lookup.index(v)), axis=0)
        return vec, self._feature_list

    def get_diff(self, from_ix, to_ix):
        x = self._x
        from_vec = x[from_ix, :]
        to_vec = x[to_ix, :]
        diff_fixs, = np.nonzero(np.not_equal(from_vec, to_vec))
        chgs = {}
        for fix in diff_fixs:
            fv = self._value_lookup[from_vec[fix]]
            if fv not in chgs:
                chgs[fv] = {}
            fo = chgs[fv]
            tv = self._value_lookup[to_vec[fix]]
            if tv not in fo:
                fo[tv] = []
            fo[tv].append(self._feature_list[fix])
        return chgs

    def get_all_ixs(self):
        return range(self._x.shape[0])

    def get_feature_count(self):
        return len(self._feature_list)

    def get_dists(self, ixs, metric_name):
        return self._dists[metric_name][ixs, :][:, ixs]

    def get_labels(self, ixs, thresholds):
        truths = self._label[ixs]
        if thresholds is None:
            return truths, np.logical_not(truths), truths
        (l, r) = thresholds
        return self._pred[ixs] >= l, self._pred[ixs] < r, truths

    def get_ixs(self, thresholds):
        f_pos, f_neg, _truth = self.get_labels(slice(None), thresholds)
        pos, = np.nonzero(f_pos)
        neg, = np.nonzero(f_neg)
        mid, = np.nonzero(np.logical_not(np.logical_or(f_pos, f_neg)))
        return pos, mid, neg

    def get_stats(self, ixs, thresholds):
        f_pos, f_neg, f_truth = self.get_labels(ixs, thresholds)
        all_p = np.count_nonzero(f_pos)
        tp = np.count_nonzero(np.logical_and(f_pos, f_truth))
        fp = all_p - tp
        all_n = np.count_nonzero(f_neg)
        tn = np.count_nonzero(np.logical_and(f_neg, np.logical_not(f_truth)))
        fn = all_n - tn
        all_t = np.count_nonzero(f_truth)
        tign = all_t - tp - fn
        all_f = np.count_nonzero(np.logical_not(f_truth))
        fign = all_f - fp - tn
        return tp, tn, fp, fn, tign, fign

    def extract_clusters(self, clusters, prefix, ixs, n_clusters, metric_name, cluster_method):
        ixs = sorted(frozenset(ixs))
        metric_name = metric_name if cluster_method != CLUSTER_META else "meta"
        n_clusters = min(n_clusters, len(ixs))
        if n_clusters <= 1 and cluster_method != CLUSTER_META and cluster_method != CLUSTER_SPLIT:
            if n_clusters <= 0:
                return
            clabels = np.array([ 0 for _ in ixs ])
        else:
            self._msg("clustering n:{0} metric:{1} method:{2} ixs:{3}", n_clusters, metric_name, cluster_method, len(ixs))
            clabels = self.compute_cluster(ixs, n_clusters, metric_name, cluster_method)
        cmap = {}
        for (pos, ix) in enumerate(ixs):
            name = str(clabels[pos])
            if name not in cmap:
                cmap[name] = []
            cmap[name].append(ix)
        for (name, arr) in cmap.items():
            name = prefix + name
            if name not in clusters:
                clusters[name] = []
            clusters[name].extend(arr)

    def _compute_split(self, cur_ixs, rest_ixs, max_features):

        def get_split(ixs, features):
            view = self._x[ixs, :][:, [ f for (f, v) in features ]]
            looks = np.ones(view.shape[0], dtype=np.bool)
            for (ix, (f, v)) in enumerate(features):
                looks = np.logical_and(looks, view[:, ix] == v)
            return ixs[np.nonzero(looks)]

        def count_split(ixs, features):
            view = self._x[ixs, :][:, [ f for (f, v) in features ]]
            for (ix, (f, v)) in enumerate(features):
                tmps, = np.where((view[:, ix] == v).transpose())
                view = view[tmps, :]
            return view.shape[0], ixs.shape[0]

        def entropy(p):
            if p == 0:
                return 0
            return -p * np.log2(p)

        def get_quality(features):
            cur_is, cur_all = count_split(cur_ixs, features)
            rest_is, rest_all = count_split(rest_ixs, features)
            sum_is = cur_is + rest_is
            sum_all = cur_all + rest_all
            h_all = entropy(sum_is / sum_all) + entropy((sum_all - sum_is) / sum_all)
            h_cur = entropy(cur_is / cur_all) + entropy((cur_all - cur_is) / cur_all)
            return h_all - h_cur

        val_count = len(self._value_lookup)
        f_count = self._x.shape[1]
        features = []
        best = None
        old_qual = None
        while len(features) <= max_features:
            fs = frozenset([ f for (f, v) in features ])
            for f in xrange(f_count):
                if f in fs:
                    continue
                for v in xrange(val_count):
                    if self._value_lookup[v] != "1":
                        continue
                    cur_fs = features + [ (f, v) ]
                    qual = get_quality(cur_fs)
                    if old_qual is None or qual > old_qual:
                        best = cur_fs
                        old_qual = qual
            if len(best) <= len(features):
                break
            features = best
        return get_split(cur_ixs, best) if best is not None else np.array([])

    def compute_cluster(self, ixs, n_clusters, metric_name, cluster_method):
        np.random.seed(0)
        if cluster_method == CLUSTER_AGGLOMERATIVE:
            clus = AgglomerativeClustering(n_clusters=n_clusters, affinity="precomputed", linkage="complete")
            dists = self.get_dists(ixs, metric_name)
            res = clus.fit_predict(dists)
        elif cluster_method == CLUSTER_KMEDOID:
            clus = KMeans(n_clusters=n_clusters, precompute_distances=True)
            dists = self.get_dists(ixs, metric_name)
            res = clus.fit_predict(dists)
        elif cluster_method == CLUSTER_META:
            elem, counts = np.unique(self._meta[ixs], return_counts=True)
            drop = frozenset([ e for (e, c) in zip(elem, counts) if c < n_clusters ])
            res = np.array([ r if r not in drop else "rest_cluster" for r in res ])
        elif cluster_method == CLUSTER_SPLIT:
            res = np.array([ "rest_cluster" for _ in xrange(self._x.shape[0]) ])
            iteration = 0
            valid_cur = np.zeros(self._x.shape[0], dtype=np.int8)
            valid_cur[ixs] = 1
            rest_ixs, = np.nonzero(1 - valid_cur)
            MIN_SIZE = 0
            MAX_ITER = 10
            while np.count_nonzero(valid_cur) > MIN_SIZE:
                cur_ixs, = np.nonzero(valid_cur)
                split = self._compute_split(cur_ixs, rest_ixs, n_clusters)
                self._msg("{0} {1}", np.count_nonzero(valid_cur), iteration)
                if split.shape[0] == 0:
                    break
                res[split] = "cluster " + str(iteration)
                valid_cur[split] = 0
                iteration += 1
                if iteration >= MAX_ITER:
                    break
            res = res[ixs]
        else:
            raise ValueError("unknown clustering {0}".format(cluster_method))
        return res

    def project_points(self, projection, metric_name, thresholds):
        self._msg("projecting metric:{0} method:{1}", metric_name, projection)
        dists = self.get_dists(slice(None), metric_name)
        return self.do_project(projection, dists)

    def do_project(self, projection, dists):
        np.random.seed(0)
        if projection == PROJECT_MDS:
            mds = manifold.MDS(dissimilarity="precomputed")
            pos = mds.fit_transform(dists)
        elif projection == PROJECT_TSNE:
            tsne = manifold.TSNE(metric="precomputed")
            pos = tsne.fit_transform(dists)
        else:
            raise ValueError("invalid projection {0}".format(projection))
        return [{
            "ix": ix,
            "x": p[0],
            "y": p[1],
        } for (ix, p) in enumerate(pos) ]

    def empty_weight_function(self):
        return lambda name: {}

    def compute_features(self, cixs, thresholds, name, w_fun):
        cx = self._x[cixs, :]
        lixs = np.arange(self._x.shape[0])[cixs]
        weights = w_fun(name)

        def get_feature(fix, name):
            f = {
                "name": name,
                "values": [ {
                    "value": v,
                    "stats": self.get_stats(lixs[np.nonzero(cx[:, fix] == vix)], thresholds),
                } for (vix, v) in enumerate(self._value_lookup) ]
            }
            for (k, ws) in weights.items():
                f[k] = ws[fix]
            return f

        return [ get_feature(fix, name) for (fix, name) in enumerate(self._feature_list) ]

    def get_point(self, ix):
        return {
            "ix": ix,
            "score": self._pred[ix],
            "label": bool(self._label[ix]),
            "meta": self._meta_lookup[self._meta[ix]],
        }

    def get_points(self, ixs):
        return sorted([ self.get_point(ix) for ix in ixs ], key=lambda p: p["score"], reverse=True)

    def manipulate_clusters(self, c_names, ixss, task, split, metric_name, cluster_method, c_used_features, feature_weights):
        if split:
            cluster_split_cix = int(split[0])
            cluster_split_fix = int(split[1])
        else:
            cluster_split_cix = None
            cluster_split_fix = None
        all_ixs = sorted(frozenset([ ix for ixs in ixss for ix in ixs ]))
        clusters = {}
        cluster_order = []

        def add_cluster(name, ixs):
            if not ixs:
                return
            nix = 0
            cur_name = name
            while cur_name in clusters:
                cur_name = "{0}_{1}".format(name, nix)
                nix += 1
            cluster_order.append(cur_name)
            clusters[cur_name] = ixs

        def split_cluster(name, cixs, split_fix):
            vals = self._x[cixs, :][:, split_fix]
            lixs = np.arange(self._x.shape[0])[cixs]
            for (vix, _) in enumerate(self._value_lookup):
                add_cluster(name, lixs[np.nonzero(np.equal(vals, vix))].tolist())

        def recluster(name, cixs, num_childs):
            cs = {}
            self.extract_clusters(cs, name, cixs, num_childs, metric_name, cluster_method)
            cs_order = sorted(cs.keys(), key=lambda c: len(cs[c]), reverse=True)
            for k in cs_order:
                add_cluster(k, cs[k])

        if task == "set_clusters" and (c_used_features is not None or feature_weights is not None):
            used_features = {}
            if c_used_features is not None:
                for (cix, arrs) in enumerate(c_used_features):
                    name = c_names[cix]
                    uf = {}
                    for (v, fs) in enumerate(arrs):
                        for f in fs:
                            uf[f] = v + 1
                    used_features[name] = uf
            if feature_weights is None:
                feature_weights = np.zeros(len(self._feature_list))
            weights = lambda name: {
                "paths": [ used_features[name].get(f, 0) for f in self._feature_list ]
            } if name is not None else {
                "importance": [ feature_weights[fix] for fix in xrange(len(self._feature_list)) ],
            }
        else:
            weights = self.empty_weight_function()
        for (cix, cixs) in enumerate(ixss):
            c_name = c_names[cix]
            if task != "set_clusters" and cix == cluster_split_cix:
                if task == "split":
                    split_cluster(c_name, cixs, cluster_split_fix)
                elif task == "cluster":
                    recluster(c_name, cixs, cluster_split_fix)
                else:
                    raise ValueError("unknow task: {0}".format(task))
            else:
                add_cluster(c_name, cixs)
        return clusters, cluster_order, weights

    def get_raw_clusters(self, thresholds, ns, metric_name, cluster_method):
        classes = [ "pos", "mid", "neg" ]
        ixss = self.get_ixs(thresholds)
        clusters = {}
        if cluster_method == CLUSTER_RULES or cluster_method == CLUSTER_TREE:
            labels = np.array([ "unknown" for _ in xrange(self._x.shape[0]) ])
            for (pix, name) in enumerate(classes):
                labels[ixss[pix]] = name
            clf = rule_classifier.RuleExtractor(max_length=5, by=self._value_lookup.index("1")) if cluster_method == CLUSTER_RULES else file_classifier.FileRuleExtractor(self._feature_lookup) # dt_classifier.DTRuleExtractor()
            clf.fit(self._x, labels)
            importance = np.zeros(self._x.shape[1])

            def get_rule_weights(name):
                res = {
                    "importance": importance.tolist(),
                }
                if name is not None:
                    res["paths"] = np.array([ 2.0 if (fix, True) in rule_map[name] else (1.0 if (fix, False) in rule_map[name] else 0.0) for fix in xrange(len(self._feature_list)) ])
                    res["fidelity"] = fidelity_map[name]
                return res

            weights = get_rule_weights
            rules = clf.get_rules()
            rule_labels = clf.get_rule_labels()
            rule_fidelity = clf.get_rule_fidelity()
            rule_map = {}
            fidelity_map = {}
            name_map = []
            nums = {}
            for (rix, r) in enumerate(rules):
                l = rule_labels[rix]
                if l not in nums:
                    nums[l] = 0
                name = "{0}_{1}".format(l, nums[l])
                name_map.append(name)
                clusters[name] = []
                fidelity_map[name] = rule_fidelity[rix]
                rule_map[name] = r # is a set
                for (fix, _) in r:
                    importance[fix] += 1.0
                nums[l] += 1
            pr = clf.predict_rules(self._x)
            for ix in xrange(pr.shape[0]):
                for rix in pr[ix, :].nonzero()[0]:
                    name = name_map[rix]
                    if name not in clusters:
                        clusters[name] = []
                    clusters[name].append(ix)
        elif cluster_method == CLUSTER_NO_MODEL:
            weights = self.empty_weight_function()
            ixss = [ [], [], [], ]
            for (ix, l) in enumerate(self._label):
                if l:
                    ixss[0].append(ix)
                else:
                    ixss[2].append(ix)
            for (pix, name) in enumerate(classes):
                self.extract_clusters(clusters, name, ixss[pix], ns[pix], metric_name, CLUSTER_KMEDOID)
        else:
            weights = self.empty_weight_function()
            for (pix, name) in enumerate(classes):
                self.extract_clusters(clusters, name, ixss[pix].tolist(), ns[pix], metric_name, cluster_method)
        cluster_order = sorted(clusters.keys(), key=lambda c: len(clusters[c]), reverse=True)
        return clusters, cluster_order, weights

    def get_cluster_scatterplot(self, projection, thresholds, rules):
        ixss = self.get_ixs(thresholds)
        clusters, cluster_order, weights = self.get_raw_clusters(thresholds, [ ixs.shape[0] for ixs in ixss ], None, CLUSTER_TREE if not rules else CLUSTER_RULES)
        wss = [ weights(name) for name in cluster_order ]
        dists = np.zeros((len(cluster_order), len(cluster_order)), dtype=np.float64)

        def compute_path_distance(rix, size):
            rp = wss[rix]["paths"]
            rvs = [ np.equal(rp, float(v) + 1.0) for v in self._value_lookup ]
            rtotal = 0.0
            for ix in xrange(len(self._value_lookup)):
                np.dot(rvs[ix], rvs[ix])
            for cix in xrange(rix):
                cp = wss[cix]["paths"]
                cvs = [ np.equal(cp, float(v) + 1.0) for v in self._value_lookup ]
                conj = 0.0
                total = rtotal
                for ix in xrange(len(self._value_lookup)):
                    conj += np.dot(rvs[ix], cvs[ix])
                    total += np.dot(cvs[ix], cvs[ix])
                disj = total - conj
                if disj == 0 or conj == 0:
                    d = np.inf
                else:
                    d = -np.log2(np.float64(conj) / np.float64(disj))
                d = min(d, 1e6)
                dists[rix, cix] = d
                dists[cix, rix] = d

        progress(0, dists.shape[0], compute_path_distance, prefix="path_distances")
        pos = self.do_project(projection, dists)

        def convert_path(path):
            res = [ [] for _ in self._value_lookup ]
            for (fix, f) in enumerate(self._feature_list):
                v = int(path[fix] - 1)
                if v >= 0:
                    res[v].append(f)
            return res

        def get_vals(nix, name, cixs):
            ws = wss[nix]
            if pos[nix]["ix"] != nix:
                raise ValueError("{0} != {1}".format(pos[nix]["ix"], nix))
            return {
                "name": name,
                "total": len(cixs),
                "label_pos": np.count_nonzero(self._label[cixs]),
                "complexity": np.count_nonzero(ws["paths"]),
                "fidelity": ws["fidelity"],
                "importance": ws["importance"],
                "proj_x": pos[nix]["x"],
                "proj_y": pos[nix]["y"],
                "path": convert_path(ws["paths"]),
                "ixs": cixs,
            }

        return [ get_vals(nix, name, clusters[name]) for (nix, name) in enumerate(cluster_order) ]

    def enrich_clusters(self, clusters, cluster_order, thresholds, cluster_method, weights):
        fs_info = self.compute_features(slice(None), thresholds, None, weights)

        def raw_cluster(name, cixs):
            features = self.compute_features(cixs, thresholds, name, weights)
            res = {
                "name": name,
                "total": len(cixs),
                "label_pos": np.count_nonzero(self._label[cixs]),
                "features": features,
                "points": self.get_points(cixs),
                "ixs": cixs,
            }
            if cluster_method == CLUSTER_META:
                hmeta = None
                for p in res["points"]:
                    if hmeta is None:
                        hmeta = p["meta"]
                    elif hmeta != p["meta"]:
                        hmeta = "(remaining)"
                        break
                res["header_name"] = hmeta
            return res

        return {
            "stats": self.get_stats(slice(None), thresholds),
            "features": fs_info,
            "clusters": [ raw_cluster(name, clusters[name]) for name in cluster_order ],
        }

    def get_info(self, ixs, fs, vs, thresholds):
        if len(fs) != len(vs):
            raise ValueError("fs and vs don't have same lengths {0} != {1}".format(len(fs), len(vs)))
        vixs = [ self._value_ixs[v] for v in vs ]
        if not ixs and fs:
            ixs = slice(None)
        lixs = np.arange(self._x.shape[0])[ixs]
        if fs:
            eqs = self._x[ixs, :][:, fs] == np.repeat([ vixs ], lixs.shape[0], axis=0)
            rows = lixs[np.nonzero(np.all(eqs, axis=1))]
        else:
            rows = lixs
        return {
            "points": self.get_points(rows),
            "features": self.compute_features(rows, thresholds, None, self.empty_weight_function()),
            "full_total": self._x.shape[0],
        }

    def get_weights(self, cixss):
        self._msg("weights clusters: {0}", len(cixss))
        all_ixs = sorted(frozenset([ ix for ixs in cixss for ix in ixs ]))
        x = self._x[all_ixs, :]

        def feature_weights(cluster):
            clf = DecisionTreeClassifier(criterion='gini', random_state=0, max_features=None)
            clf.fit(x, [ ix in cluster for ix in all_ixs ])
            return [ {
                "name": name,
                "gini": clf.feature_importances_[fix],
            } for (fix, name) in enumerate(self._feature_list) ]

        return [ feature_weights(frozenset(cixs)) for cixs in cixss ]

    def get_roc_curve(self):
        ths = sorted(np.unique(self._pred))
        if ths[0] != 0:
            ths.insert(0, 0)
        if ths[-1] != 1:
            ths.append(1)
        ths.append(1.0 + 1e-12) # include all elements

        def get_point(t):
            tp, tn, fp, fn, tign, fign = self.get_stats(slice(None), (t, t))
            if tign != 0 or fign != 0:
                raise ValueError("should not happen! t: {0} f: {1}".format(tign, fign))
            return {
                "score": t,
                "tp": tp,
                "tn": tn,
                "fp": fp,
                "fn": fn,
            }

        return {
            "auc": roc_auc_score(self._label, self._pred),
            "roc": [ get_point(t) for t in ths ],
        }

    def get_params(self):
        return {
            "cluster": {
                "values": list(CLUSTER_PARAMS),
                "default": CLUSTER_DEFAULT,
            },
            "project": {
                "values": list(PROJECT_PARAMS),
                "default": PROJECT_DEFAULT,
            },
            "distance": {
                "values": sorted(self._dists.keys()),
                "default": DISTANCE_DEFAULT,
            },
            "features": self._feature_list,
        }
