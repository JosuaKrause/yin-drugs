#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import os
import csv
import sys

def assign_label(p_cur, label_str):
    if "label" in p_cur:
        print("duplicate label for patient!", file=sys.stderr)
    p_cur["label"] = label_str == "1"
    if not p_cur["label"] and label_str != "0":
        print("invalid label '{0}'".format(label_str), file=sys.stderr)

def assign_pred(p_cur, pred_str):
    if "pred" in p_cur:
        print("duplicate prediction for patient!", file=sys.stderr)
    p_cur["pred"] = float(pred_str)

total_p = 0
def flush_patient(p_cur, patients):
    global total_p
    if p_cur is not None:
        if "label" not in p_cur:
            print("missing label for patient!", file=sys.stderr)
        if "pred" not in p_cur:
            print("missing pred for patient!", file=sys.stderr)
        patients.append(p_cur)
        total_p += 1
        if total_p % 1000 == 0:
            print("processed {0} patients".format(total_p), file=sys.stdout)
    return {
        "drugs": set()
    }

def add_drug(med, p_cur, drugs, line):

    def do_add(med):
        p_cur["drugs"].add(med)
        drugs.add(med)

    head = med.split('(', 1)
    do_add('num_' + head[0])
    if len(head) == 1:
        return
    gran = head[1].split('||', 1)
    do_add('gran_' + gran[0].rstrip(')'))
    if len(gran) == 1:
        return
    if not gran[1].rstrip(')'):
        print("WARNING: no detail ({0}): '{1}'".format(line, med), file=sys.stdout)
    else:
        do_add('detail_' + gran[1].rstrip(')'))

def convert_output(out):
    return "1" if out else "0"

def usage():
    print("""
usage: {0} <input> <output>
<input>: the input csv file
<output>: the output csv file
""".strip().format(sys.argv[0]), file=sys.stderr)
    exit(1)

if __name__ == '__main__':
    argv = sys.argv[:]
    if len(argv) != 3:
        usage()
    input = argv[1]
    output = argv[2]
    patients = []
    patient_cur = None
    drugs = set()
    with open(input, 'rU') as f_in:
        line = 1
        for row in csv.DictReader(f_in):
            if len(row["True_Label"].strip()):
                patient_cur = flush_patient(patient_cur, patients)
                assign_label(patient_cur, row["True_Label"].strip())
            if len(row["Prediction"].strip()):
                assign_pred(patient_cur, row["Prediction"].strip())
            add_drug(row["Medication"].strip(), patient_cur, drugs, line)
            line += 1
        patient_cur = flush_patient(patient_cur, patients)
    feature_names = sorted(list(drugs))
    feature_names.append("label")
    feature_names.append("pred")
    truths = len([ 1 for p in patients if p["label"] ])
    total = len(patients)
    threshold = truths / total
    if total_p != total:
        raise ValueError("{0} != {1}".format(total_p, total))

    def get_stats(patients, threshold):
        tp = 0
        tn = 0
        fp = 0
        fn = 0
        for p in patients:
            if p["pred"] > threshold:
                if p["label"]:
                    tp += 1
                else:
                    fp += 1
            else:
                if p["label"]:
                    fn += 1
                else:
                    tn += 1
        return tp, tn, fp, fn

    print("total patients: {0}".format(total), file=sys.stdout)
    print("total true: {0}".format(truths), file=sys.stdout)
    print("total features: {0}".format(len(feature_names)), file=sys.stdout)
    print("total detail: {0}".format(len([ 1 for k in feature_names if k.startswith("detail_") ])), file=sys.stdout)
    print("total gran: {0}".format(len([ 1 for k in feature_names if k.startswith("gran_") ])), file=sys.stdout)
    print("total num: {0}".format(len([ 1 for k in feature_names if k.startswith("num_") ])), file=sys.stdout)
    tp, tn, fp, fn = get_stats(patients, threshold)
    print("precision: {0}".format(tp / (tp + fp)), file=sys.stdout)
    print("recall: {0}".format(tp / (tp + fn)), file=sys.stdout)
    print("accuracy: {0}".format((tp + tn) / (tp + tn + fp + fn)), file=sys.stdout)
    print("  | p     n", file=sys.stdout)
    print("--+------------", file=sys.stdout)
    print("t | {0:.2f}  {1:.2f}".format(tp / total, tn / total), file=sys.stdout)
    print("f | {0:.2f}  {1:.2f}".format(fp / total, fn / total), file=sys.stdout)

    def process_field(p, k):
        if k == "label":
            return convert_output(p["label"])
        if k == "pred":
            return p["pred"] # > threshold ## we want the actual value
        return convert_output(k in p["drugs"])

    with open(output, 'w') as f_out:
        out = csv.writer(f_out)
        out.writerow(feature_names)
        for p in patients:
            out.writerow([ process_field(p, k) for k in feature_names ])
